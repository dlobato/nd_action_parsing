/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PCL_TYPE_RGBATOHSV_H
#define PCL_TYPE_RGBATOHSV_H

namespace pcl
{

  /**
   * RGBA to HSV conversion
   * H [0,360]
   * S [0,1]
   * V [0,1]
   */
  inline void
  RGBAtoHSV(const uint32_t rgba, float* hsv){
    uint8_t r_ = static_cast <uint8_t> (rgba >> 16);
    uint8_t g_ = static_cast <uint8_t> (rgba >> 8);
    uint8_t b_ = static_cast <uint8_t> (rgba);
    float h_,s_,v_,min;

    v_ = std::max (r_, std::max (g_, b_));
    min = std::min (r_, std::min (g_, b_));

    if (v_ != 0)
      s_ = (v_ - min) / v_;
    else{
	s_ = 0;
	h_ = -1;
	return;
    }
    
    if (r_ == v_)
      h_ = static_cast<float> (g_ - b_) / (v_ - min);
    else if (g_ == v_)
      h_ = static_cast<float> (2 + (b_ - r_) / (v_ - min));
    else 
      h_ = static_cast<float> (4 + (r_ - g_) / (v_ - min));
    h_ *= 60;
    if (h_ < 0)
      h_ += 360;
    hsv[0] = h_;
    hsv[1] = s_;
    hsv[2] = v_/255.0;//in PCL this isn't adjusted to [0,1], instead they just return [0,255] Why??
  }

  /** \brief Convert a XYZRGBA point type to a XYZHSV
   * \param[in] in the input XYZRGBA point 
   * \param[out] out the output XYZHSV point
   */
  inline void 
  PointXYZRGBAtoXYZHSV (const PointXYZRGBA& in,
			PointXYZHSV& out)
  {
    float min;

    out.x = in.x; out.y = in.y; out.z = in.z;
    RGBAtoHSV(in.rgba,out.data_c);
  }

  /** \brief Convert a XYZRGBA point cloud to a XYZHSV
   * \param[in] in the input XYZRGBA point cloud
   * \param[out] out the output XYZHSV point cloud
   */
  inline void 
  PointCloudXYZRGBAtoXYZHSV (const PointCloud<PointXYZRGBA>& in,
			     PointCloud<PointXYZHSV>& out)
  {
    out.width   = in.width;
    out.height  = in.height;
    for (size_t i = 0; i < in.points.size (); i++)
      {
	PointXYZHSV p;
	PointXYZRGBAtoXYZHSV (in.points[i], p);
	out.points.push_back (p);
      }
  }
}
#endif //PCL_TYPE_RGBATOHSV_H
