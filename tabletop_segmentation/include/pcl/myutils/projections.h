/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
  
#ifndef PCL_PROJECTIONS_H
#define PCL_PROJECTIONS_H

#include <vector>
#include <pcl/point_cloud.h>

namespace pcl
{
  /*! \brief Makes an orthographic projection on XY plane from input_cloud[indices]
   *         Makes an orthographic projection on XY plane from input_cloud[indices] with 
   * given width and height. Resultant projection is split in two components, z value (or depth) 
   * and rgb data if available in PointT.
   * \param input_cloud cloud to be projected
   * \param indices input_cloud subset to be projected
   * \param projection_width resulting projection width (rows)
   * \param projection_height resulting projection height (height)
   * \param projection_z_data data buffer to save z value. Internally used in a very basic z-buffer. 
   *        Unless you intend that, this buffer should be initiallized to the minimum z value you expect in your input cloud.
   *        Make sure the buffer points to at least projection_width*projection_height*sizeof(float)
   * \param projection_rgb_data data buffer to save rgb data. If rgb or rgba fields not avalible in input_cloud
   *        or NULL, rgb data won't be returned. Unless you intend that, this buffer should be initialized to 0.
   *        Make sure the buffer points to at least projection_width*projection_height*sizeof(unsigned char)*3
   */ 
  template <typename PointT>
  void orthographicXYProjection(const PointCloud<PointT> &input_cloud,
				const std::vector<int> &indices,
				const int projection_width, const int projection_height,
				float* projection_z_data,
				unsigned char* projection_rgb_data=NULL);//1
  
  /*! \brief Makes an orthographic projection on XY plane from input_cloud
   *         Makes an orthographic projection on XY plane from input_cloud with 
   * given width and height. Resultant projection is split in two components, z value (or depth) 
   * and rgb data if available in PointT.
   * \param input_cloud cloud to be projected
   * \param projection_width resulting projection width (rows)
   * \param projection_height resulting projection height (height)
   * \param projection_z_data data buffer to save z value. Internally used in a very basic z-buffer. 
   *        Unless you intend that, this buffer should be initiallized to the minimum z value you expect in your input cloud.
   *        Make sure the buffer points to at least projection_width*projection_height*sizeof(float)
   * \param projection_rgb_data data buffer to save rgb data. If rgb or rgba fields not avalible in input_cloud
   *        or NULL, rgb data won't be returned. Unless you intend that, this buffer should be initialized to 0.
   *        Make sure the buffer points to at least projection_width*projection_height*sizeof(unsigned char)*3
   */ 
  template <typename PointT>
  void orthographicXYProjection(const PointCloud<PointT> &input_cloud,
				const int projection_width, const int projection_height,
				float* projection_z_data,
				unsigned char* projection_rgb_data=NULL);//2

  /*! \brief Makes an orthographic projection on XY plane from input_cloud[indices]
   *         Makes an orthographic projection on XY plane from input_cloud[indices] with 
   * given width and height. Resultant projection is split in two components, z value (or depth) 
   * and rgb data if available in PointT.
   * \param input_cloud cloud to be projected
   * \param indices input_cloud subset to be projected
   * \param projection_width resulting projection width (rows)
   * \param projection_height resulting projection height (height)
   * \param projection_z_data data buffer to save z value. Internally used in a very basic z-buffer. 
   *        Unless you intend that, this buffer should be initiallized to the minimum z value you expect in your input cloud.
   *        Make sure the buffer vector is at least projection_width*projection_height
   * \param projection_rgb_data data buffer to save rgb data. If rgb or rgba fields not avalible in input_cloud
   *        rgb data won't be returned. Unless you intend that, this buffer should be initialized to 0.
   *        Make sure the buffer vector is at least projection_width*projection_height*3
   */
  template <typename PointT>
  void orthographicXYProjection(const PointCloud<PointT> &input_cloud,
				const std::vector<int> &indices,
				const int projection_width, const int projection_height,
				std::vector<float> &projection_z_data,
				std::vector<unsigned char> &projection_rgb_data);//3

  /*! \brief Makes an orthographic projection on XY plane from input_cloud[indices]
   *         Makes an orthographic projection on XY plane from input_cloud[indices] with 
   * given width and height. Resultant projection is z value (or depth) 
   * \param input_cloud cloud to be projected
   * \param indices input_cloud subset to be projected
   * \param projection_width resulting projection width (rows)
   * \param projection_height resulting projection height (height)
   * \param projection_z_data data buffer to save z value. Internally used in a very basic z-buffer. 
   *        Unless you intend that, this buffer should be initiallized to the minimum z value you expect in your input cloud.
   *        Make sure the buffer vector is at least projection_width*projection_height
   */
  template <typename PointT>
  void orthographicXYProjection(const PointCloud<PointT> &input_cloud,
				const std::vector<int> &indices,
				const int projection_width, const int projection_height,
				std::vector<float> &projection_z_data);//4
  /*! \brief Makes an orthographic projection on XY plane from input_cloud
   *         Makes an orthographic projection on XY plane from input_cloud with 
   * given width and height. Resultant projection is split in two components, z value (or depth) 
   * and rgb data if available in PointT.
   * \param input_cloud cloud to be projected
   * \param projection_width resulting projection width (rows)
   * \param projection_height resulting projection height (height)
   * \param projection_z_data data buffer to save z value. Internally used in a very basic z-buffer. 
   *        Unless you intend that, this buffer should be initiallized to the minimum z value you expect in your input cloud.
   *        Make sure the buffer vector is at least projection_width*projection_height
   * \param projection_rgb_data data buffer to save rgb data. If rgb or rgba fields not avalible in input_cloud
   *        rgb data won't be returned. Unless you intend that, this buffer should be initialized to 0.
   *        Make sure the buffer vector is at least projection_width*projection_height*3
   */
  template <typename PointT>
  void orthographicXYProjection(const PointCloud<PointT> &input_cloud,
				const int projection_width, const int projection_height,
				std::vector<float> &projection_z_data,
				std::vector<unsigned char> &projection_rgb_data);//5

  /*! \brief Makes an orthographic projection on XY plane from input_cloud
   *         Makes an orthographic projection on XY plane from input_cloud with 
   * given width and height. Resultant projection is z value (or depth) 
   * \param input_cloud cloud to be projected
   * \param projection_width resulting projection width (rows)
   * \param projection_height resulting projection height (height)
   * \param projection_z_data data buffer to save z value. Internally used in a very basic z-buffer. 
   *        Unless you intend that, this buffer should be initiallized to the minimum z value you expect in your input cloud.
   *        Make sure the buffer vector is at least projection_width*projection_height
   */
  template <typename PointT>
  void orthographicXYProjection(const PointCloud<PointT> &input_cloud,
				const int projection_width, const int projection_height,
				std::vector<float> &projection_z_data);//6
}

#endif //PCL_PROJECTIONS_H
