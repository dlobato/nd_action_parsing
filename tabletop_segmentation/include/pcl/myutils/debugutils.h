/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pcl/common/time.h>
#include <iostream>

#if PROFILE
#define FPS_CALC_BEGIN                          \
  static double duration = 0;			\
  double start_time = pcl::getTime ();		\

#define FPS_CALC_END(_WHAT_)						\
  {									\
    double end_time = pcl::getTime ();					\
    static unsigned count = 0;						\
    if (++count == 10)							\
      {									\
	std::cerr << "Average framerate("<< _WHAT_ << "): " << double(count)/double(duration) << " Hz" <<  std::endl; \
	count = 0;							\
	duration = 0.0;							\
      }									\
    else								\
      {									\
	duration += end_time - start_time;				\
      }									\
  }
#else
#define FPS_CALC_BEGIN
#define FPS_CALC_END(_WHAT_)
#endif

#if DEBUG
#define DEBUG_MESSAGE(_WHAT_)  std::cerr << __PRETTY_FUNCTION__ << ":" << _WHAT_ << std::endl;
#else
#define DEBUG_MESSAGE(_WHAT_)
#endif
