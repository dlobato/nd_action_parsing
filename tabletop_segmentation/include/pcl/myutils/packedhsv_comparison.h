/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PCL_PACKEDHSV_COMPARISON_H_
#define PCL_PACKEDHSV_COMPARISON_H_

#include <pcl/filters/conditional_removal.h>
#include <pcl/myutils/point_types_rgbatohsv.h>

namespace pcl
{
  /** \brief A packed HSV specialization of the comparison object. */
  template<typename PointT>
  class PackedHSVComparison : public ComparisonBase<PointT>
  {
    using ComparisonBase<PointT>::capable_;
    using ComparisonBase<PointT>::op_;

  public:
    /** \brief Construct a PackedHSVComparison 
     * \param component_name either "h", "s" or "v"
     * \param op the operator to use when making the comparison
     * \param compare_val the constant value to compare the component value too
     */
    PackedHSVComparison (std::string component_name, ComparisonOps::CompareOp op, double compare_val);

    /** \brief Destructor. */
    virtual ~PackedHSVComparison () {}

    /** \brief Determine the result of this comparison.  
     * \param point the point to evaluate
     * \return the result of this comparison.
     */
    virtual bool
    evaluate (const PointT &point) const;

    typedef enum
      {
        H=0, // see point_types_rgbatohsv.h:RGBAtoHSV for ranges
        S=1, // 
        V=2  // 
      } ComponentId;

  protected:
    /** \brief The name of the component. */
    std::string component_name_;

    /** \brief The ID of the component. */
    ComponentId component_id_;

    /** \brief All types (that we care about) can be represented as a double. */
    double compare_val_;

    /** \brief The offset of the component */
    uint32_t rgb_offset_;

  private:
    PackedHSVComparison () :
      component_name_ (), component_id_ (), compare_val_ (), rgb_offset_ ()
    {
    } // not allowed
  };
}
#endif //PCL_PACKEDHSV_COMPARISON_H_
