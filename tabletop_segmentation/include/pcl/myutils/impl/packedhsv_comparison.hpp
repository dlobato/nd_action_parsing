/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PCL_PACKEDHSV_COMPARISON_IMPL_H_
#define PCL_PACKEDHSV_COMPARISON_IMPL_H_

#include <pcl/common/io.h>


template <typename PointT>
pcl::PackedHSVComparison<PointT>::PackedHSVComparison (
    std::string component_name, ComparisonOps::CompareOp op, double compare_val) : 
  component_name_ (component_name), component_id_ (), compare_val_ (compare_val), rgb_offset_ ()
{
  // Get all the fields
  std::vector<pcl::PCLPointField> point_fields; 
  // Use a dummy cloud to get the field types in a clever way
  PointCloud<PointT> dummyCloud;
  pcl::getFields (dummyCloud, point_fields);

  // Locate the "rgb" field
  size_t d;
  for (d = 0; d < point_fields.size (); ++d)
    if (point_fields[d].name == "rgb" || point_fields[d].name == "rgba") 
      break;
  if (d == point_fields.size ())
  {
    PCL_WARN ("[pcl::PackedHSVComparison::PackedHSVComparison] rgb field not found!\n");
    capable_ = false;
    return;
  }

  // Verify the datatype
  uint8_t datatype = point_fields[d].datatype;
  if (datatype != pcl::PCLPointField::FLOAT32 && 
      datatype != pcl::PCLPointField::UINT32 && 
      datatype != pcl::PCLPointField::INT32) 
  {
    PCL_WARN ("[pcl::PackedHSVComparison::PackedHSVComparison] has unusable type!\n");
    capable_ = false;
    return;
  }

  // verify the offset
  uint32_t offset = point_fields[d].offset;
  if (offset % 4 != 0)
  {
    PCL_WARN ("[pcl::PackedHSVComparison::PackedHSVComparison] rgb field is not 32 bit aligned!\n");
    capable_ = false;
    return;
  }
  rgb_offset_ = point_fields[d].offset;
  // verify the component name
  if (component_name == "h" ) 
  {
    component_id_ = H;
  } 
  else if (component_name == "s") 
  {
    component_id_ = S;
  } 
  else if (component_name == "v") 
  { 
    component_id_ = V;
  } 
  else 
  {
    PCL_WARN ("[pcl::PackedRGBComparison::PackedRGBComparison] unrecognized component name!\n");
    capable_ = false;
    return;
  }

  // Save the context
  capable_ = true;
  op_ = op;
}

template <typename PointT> bool
pcl::PackedHSVComparison<PointT>::evaluate (const PointT &point) const
{
  // Since this is a const function, we can't make these data members because we change them here
  static uint32_t rgb_val_ = 0;
  static uint8_t r_ = 0;
  static uint8_t g_ = 0;
  static uint8_t b_ = 0;
  static float hsv_[3] = {0.0,0.0,0.0};

  // We know that rgb data is 32 bit aligned (verified in the ctor) so...
  const uint8_t* pt_data = reinterpret_cast<const uint8_t*> (&point);
  const uint32_t* rgb_data = reinterpret_cast<const uint32_t*> (pt_data + rgb_offset_);
  uint32_t new_rgb_val = *rgb_data;

  if (rgb_val_ != new_rgb_val) 
  { // avoid having to redo this calc, if possible
    rgb_val_ = new_rgb_val;
    RGBAtoHSV(rgb_val_,hsv_);
  }

  if (component_id_ != H && component_id_ != S && component_id_ != V)
    assert (false);

  float my_val = hsv_[component_id_];

  // now do the comparison
  switch (this->op_) 
  {
    case pcl::ComparisonOps::GT :
      return (my_val > this->compare_val_);
    case pcl::ComparisonOps::GE :
      return (my_val >= this->compare_val_);
    case pcl::ComparisonOps::LT :
      return (my_val < this->compare_val_);
    case pcl::ComparisonOps::LE :
      return (my_val <= this->compare_val_);
    case pcl::ComparisonOps::EQ :
      return (my_val == this->compare_val_);
    default:
      PCL_WARN ("[pcl::PackedHSVComparison::evaluate] unrecognized op_!\n");
      return (false);
  }
}

#define PCL_INSTANTIATE_PackedHSVComparison(T) template class PCL_EXPORTS pcl::PackedHSVComparison<T>;

#endif //PCL_PACKEDHSV_COMPARISON_IMPL_H_
