/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PCL_PROJECTIONS_IMPL_H
#define PCL_PROJECTIONS_IMPL_H

#include <pcl/point_types.h>
#include <pcl/common/io.h>
#include <pcl/common/time.h>

#define PROFILE 0
#define DEBUG 0
#include <pcl/myutils/debugutils.h>


template <typename PointT>
void pcl::orthographicXYProjection(const PointCloud<PointT> &input_cloud,
				   const std::vector<int> &indices,
				   const int projection_width, const int projection_height,
				   float* projection_z_data,
				   unsigned char* projection_rgb_data){
  FPS_CALC_BEGIN;
  // if RGB data available
  std::vector<pcl::PCLPointField> fields;
  int rgba_index = -1;
  size_t rgba_offset = -1;
  rgba_index = pcl::getFieldIndex (input_cloud, "rgba", fields);
  if (rgba_index == -1)
    rgba_index = pcl::getFieldIndex (input_cloud, "rgb", fields);
  if (rgba_index >= 0)
    rgba_offset = fields[rgba_index].offset;

  int proj_x,proj_y;
  size_t data_offset;
  std::vector<bool> z_assigned(projection_width*projection_height, false);

  // If dense, no need to check for NaNs
  if (input_cloud.is_dense){
    for (std::vector<int>::const_iterator indices_it = indices.begin();
	 indices_it != indices.end();
	 indices_it++){
      //project point
      assert(*indices_it >= 0 && *indices_it < static_cast<int> (input_cloud.points.size()) && "Indices out of bounds");
      const PointT &point = (input_cloud)[*indices_it];
      proj_x = pcl_lrint(point.x);
      proj_y = pcl_lrint(point.y);
      if (proj_x >= 0 && proj_x < projection_width && proj_y >= 0 && proj_y < projection_height){//point inside limits
	data_offset = (proj_y*projection_width)+proj_x;
	if (!z_assigned[data_offset] || point.z > projection_z_data[data_offset]){//z buffering
	  z_assigned[data_offset] = true;
	  projection_z_data[data_offset] = point.z;//update z
	  if (projection_rgb_data && rgba_index >= 0){//rgb present
	    pcl::RGB rgb;
	    memcpy (&rgb, reinterpret_cast<const char*> (&point) + rgba_offset, sizeof (RGB));
	    projection_rgb_data[data_offset*3] = rgb.r;//update rgb
	    projection_rgb_data[data_offset*3+1] = rgb.g;
	    projection_rgb_data[data_offset*3+2] = rgb.b;
	  }
	}
      }
    }
  }else{//not dense
    for (std::vector<int>::const_iterator indices_it = indices.begin();
	 indices_it != indices.end();
	 indices_it++){
      //project point
      assert(*indices_it >= 0 && *indices_it < static_cast<int> (input_cloud.points.size()) && "Indices out of bounds");
      const PointT &point = (input_cloud)[*indices_it];
      // Check if the point is invalid
      if (!pcl_isfinite (point.x) || 
          !pcl_isfinite (point.y) || 
          !pcl_isfinite (point.z))
        continue;
      proj_x = pcl_lrint(point.x);
      proj_y = pcl_lrint(point.y);
      if (proj_x >= 0 && proj_x < projection_width && proj_y >= 0 && proj_y < projection_height){//point inside limits
	data_offset = (proj_y*projection_width)+proj_x;
	if (!z_assigned[data_offset] || point.z > projection_z_data[data_offset]){//z buffering
	  z_assigned[data_offset] = true;
	  projection_z_data[data_offset] = point.z;//update z
	  if (projection_rgb_data && rgba_index >= 0){//rgb present
	    pcl::RGB rgb;
	    memcpy (&rgb, reinterpret_cast<const char*> (&point) + rgba_offset, sizeof (RGB));
	    projection_rgb_data[data_offset*3] = rgb.r;//update rgb
	    projection_rgb_data[data_offset*3+1] = rgb.g;
	    projection_rgb_data[data_offset*3+2] = rgb.b;
	  }
	}
      }
    }
  }
  FPS_CALC_END(__PRETTY_FUNCTION__);
}

template <typename PointT>
void pcl::orthographicXYProjection(const PointCloud<PointT> &input_cloud,
				   const int projection_width, const int projection_height,
				   float* projection_z_data,
				   unsigned char* projection_rgb_data=NULL){
  //init indices to cover every point in input_cloud
  std::vector<int> indices(input_cloud.size());
  for (size_t i=0;i<indices.size();i++) indices[i] = i;
  orthographicXYProjection(input_cloud,indices,projection_width,projection_height,projection_z_data,projection_rgb_data);
}

template <typename PointT>
void pcl::orthographicXYProjection(const PointCloud<PointT> &input_cloud,
				   const std::vector<int> &indices,
				   const int projection_width, const int projection_height,
				   std::vector<float> &projection_z_data,
				   std::vector<unsigned char> &projection_rgb_data){
  assert(projection_z_data.size() >= projection_width*projection_height);
  assert(projection_rgb_data.size() >= projection_width*projection_height*3);
  orthographicXYProjection(input_cloud,indices,projection_width,projection_height,&(projection_z_data[0]),&(projection_rgb_data[0]));
}

template <typename PointT>
void pcl::orthographicXYProjection(const PointCloud<PointT> &input_cloud,
				   const std::vector<int> &indices,
				   const int projection_width, const int projection_height,
				   std::vector<float> &projection_z_data){
  assert(projection_z_data.size() >= projection_width*projection_height);
  orthographicXYProjection(input_cloud,indices,projection_width,projection_height,&(projection_z_data[0]));
}

template <typename PointT>
void pcl::orthographicXYProjection(const PointCloud<PointT> &input_cloud,
				   const int projection_width, const int projection_height,
				   std::vector<float> &projection_z_data,
				   std::vector<unsigned char> &projection_rgb_data){
  assert(projection_z_data.size() >= projection_width*projection_height);
  assert(projection_rgb_data.size() >= projection_width*projection_height*3);
  orthographicXYProjection(input_cloud,projection_width,projection_height,&(projection_z_data[0]),&(projection_rgb_data[0]));
}

template <typename PointT>
void pcl::orthographicXYProjection(const PointCloud<PointT> &input_cloud,
				   const int projection_width, const int projection_height,
				   std::vector<float> &projection_z_data){
  assert(projection_z_data.size() >= projection_width*projection_height);
  orthographicXYProjection(input_cloud,projection_width,projection_height,&(projection_z_data[0]));
}

#define PCL_INSTANTIATE_orthographicXYProjection1(T) template PCL_EXPORTS void pcl::orthographicXYProjection<T>(const PointCloud<T> &, const std::vector<int> &, const int, const int, float*, unsigned char*);
#define PCL_INSTANTIATE_orthographicXYProjection2(T) template PCL_EXPORTS void pcl::orthographicXYProjection<T>(const PointCloud<T> &, const int , const int , float* , unsigned char*);
#define PCL_INSTANTIATE_orthographicXYProjection3(T) template PCL_EXPORTS void pcl::orthographicXYProjection<T>(const PointCloud<T> &, const std::vector<int> &, const int , const int , std::vector<float> &, std::vector<unsigned char> &);
#define PCL_INSTANTIATE_orthographicXYProjection4(T) template PCL_EXPORTS void pcl::orthographicXYProjection<T>(const PointCloud<T> &, const std::vector<int> &, const int , const int , std::vector<float> &);
#define PCL_INSTANTIATE_orthographicXYProjection5(T) template PCL_EXPORTS void pcl::orthographicXYProjection<T>(const PointCloud<T> &, const int , const int , std::vector<float> &, std::vector<unsigned char> &);
#define PCL_INSTANTIATE_orthographicXYProjection6(T) template PCL_EXPORTS void pcl::orthographicXYProjection<T>(const PointCloud<T> &, const int , const int , std::vector<float> &);

#endif //PCL_PROJECTIONS_IMPL_H
