/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TABLETOP_SEGMENTATION_HSVUTIL_H
#define TABLETOP_SEGMENTATION_HSVUTIL_H

#include <vector>

namespace ActionParsing{
  /**
   * HSV ranges in pcl/myutils/point_types_rgbatohsv.h are:
   *   H [0,360]
   *   S [0,1]
   *   V [0,1]
   * whereas in OpenCV (CV_8U) are:
   *   H [0,180]
   *   S [0,255]
   *   V [0,255]
   */

  std::vector<float>
  hsvFromOpenCV(const std::vector<unsigned char> &HSV_in);
  
  std::vector<unsigned char>
  hsvToOpenCV(const std::vector<float> &HSV_in);
  
}//ActionParsing

#endif //TABLETOP_SEGMENTATION_HSVUTIL_H
