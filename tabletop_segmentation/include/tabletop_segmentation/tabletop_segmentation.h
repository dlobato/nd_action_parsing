/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TABLETOP_SEGMENTATION_H
#define TABLETOP_SEGMENTATION_H

#include <pcl/point_cloud.h>
#include <pcl/PointIndices.h>
#include <pcl/common/eigen.h>
#include <boost/scoped_ptr.hpp>


namespace pcl{
  class PointXYZRGBA;
}


namespace ActionParsing{
  /*! \ingroup tabletop
   *  \brief This class contains the functionality related to the segmentation
   *  step. It can segment the tabletop area from the objects.
   */
  class TableTopSegmentation{
  protected:
    typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloud;
    typedef typename PointCloud::Ptr PointCloudPtr;
    typedef typename PointCloud::ConstPtr PointCloudConstPtr;

  public:
    /*!
     * Constructor.
     * \param[in] plane_segmentation_threshold Distance threshold(meters) for plane segmentation.
     * \param[in] plane_segmentation_sac_max_iterations Maximum number of iterations for sac plane segmentation.
     * \param[in] plane_segmentation_find_main_cluster If true main(bigger) cluster from the segmented plane is extracted.
     * \param[in] plane_segmentation_cluster_tolerance Spatial cluster tolerance(meters) as a measure in the L2 Euclidean space.
     * \param[in] plane_segmentation_cluster_min_size Minimum number of points that a plane needs to contain in order to be considered valid.
     * \param[in] plane_segmentation_cluster_max_size Maximum number of points that a plane needs to contain in order to be considered valid.
     * \param[in] object_segmentation_min_height Minimum height(meters) above plane for object segmentation.
     * \param[in] object_segmentation_max_height Maximum height(meters) above plane for object segmentation.
     * \param[in] object_segmentation_cluster_tolerance Spatial cluster tolerance(meters) as a measure in the L2 Euclidean space.
     * \param[in] object_segmentation_cluster_min_size Minimum number of points that an object needs to contain in order to be considered valid.
     * \param[in] object_segmentation_cluster_max_size Maximum number of points that an object needs to contain in order to be considered valid.
     */
    TableTopSegmentation(const float plane_segmentation_threshold=0.02,//meters
			 const int plane_segmentation_sac_max_iterations=1000,
			 const bool plane_segmentation_find_main_cluster=true,
			 const float plane_segmentation_cluster_tolerance=0.02,//meters
			 const int plane_segmentation_cluster_min_size=1000,
			 const int plane_segmentation_cluster_max_size=250000,
			 const float object_segmentation_min_height=0.02,//meters
			 const float object_segmentation_max_height=1.0,//meters
			 const float object_segmentation_cluster_tolerance=0.03,//meters
			 const int object_segmentation_cluster_min_size=50,
			 const int object_segmentation_cluster_max_size=25000);
    ~TableTopSegmentation();

    /*!
     * Set the plane distance threshold
     * \param[in] threshold New plane distance threshold.
     */ 
    void setPlaneThreshold(const float threshold);
    

    /*!
     * Segment tabletop plane from input_cloud
     * \param[in] input_cloud input cloud from where the tabletop plane will be segmented.
     * \param[out] table_cloud resultant segmented plane. If it points to a null pointer a new pointcloud instance will be created.
     * \param[out] table_plane_normal resultant segmented plane normal.
     * \return resultant segmented plane.
     */
    PointCloudPtr
    segmentTableTop (const PointCloudConstPtr &input_cloud,
		     PointCloudPtr &table_cloud, 
		     Eigen::Vector4f &table_plane_normal);

    /* PointCloudPtr */
    /* segmentObjects (const PointCloudConstPtr &input_cloud,  */
    /* 		    const PointCloudConstPtr &table_cloud,   */
    /* 		    PointCloudPtr &objects_cloud ); */

    /*!
     * Segment objects from input_cloud above table_hull area.
     * \param[in] input_cloud input cloud from where objects will be segmented.
     * \param[in] table_hull table area used to segment objects.
     * \param[out] objects_cloud resultant segmented objects cloud. If it points to a null pointer a new pointcloud instance will be created.
     * \return resultant segmented objects cloud.
     */
    PointCloudPtr
    segmentObjects (const PointCloudConstPtr &input_cloud, 
		    const PointCloudConstPtr &table_hull,
		    PointCloudPtr &objects_cloud );

    /*!
     * Segment and cluster objects from input_cloud above table_hull area.
     * \param[in] input_cloud input cloud from where objects will be segmented.
     * \param[in] table_hull table area used to segment objects.
     * \param[out] objects_cloud resultant segmented objects cloud. If it points to a null pointer a new pointcloud instance will be created.
     * \param[out] objects_clusters resultant object clusters.
     * \return resultant segmented objects cloud.
     */
    PointCloudPtr
    segmentAndClusterObjects (const PointCloudConstPtr &input_cloud, 
			      const PointCloudConstPtr &table_hull,
			      PointCloudPtr &objects_cloud, 
			      std::vector<pcl::PointIndices> &object_clusters );

    /*!
     * Extract hull from tabletop cloud.
     * \param[in] table_cloud input cloud from where the hull will be extracted.
     * \param[out] table_hull resultant hull. If it points to a null pointer a new pointcloud instance will be created.
     * \return resultant hull.
     */
    PointCloudPtr
    calcTabletopHull(const PointCloudConstPtr &table_cloud,
		     PointCloudPtr &table_hull);

    /*!
     * Calculates the size (x,y,z) of the input cloud.
     * \param[in] table_cloud input cloud.
     * \return Eigen::Vector3f with cloud size (x,y,z).
     */
    Eigen::Vector3f calcTabletopSize(const PointCloud &table_cloud);

    /*!
     * Calculates the min/max points of the input cloud.
     * \param[in] table_cloud input cloud.
     * \param[out] min min point.
     * \param[out] max max point.
     */
    void calcTabletopMinMax3D(const PointCloud &table_cloud, Eigen::Vector3f &min, Eigen::Vector3f &max);
  private:
    struct PImpl;
    boost::scoped_ptr<PImpl> impl_;
    
    bool plane_segmentation_find_main_cluster_;
  };
}
#endif //TABLETOP_SEGMENTATION_H
