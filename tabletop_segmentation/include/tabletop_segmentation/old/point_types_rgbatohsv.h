#ifndef PCL_TYPE_RGBATOHSV_H
#define PCL_TYPE_RGBATOHSV_H

namespace pcl
{

  /** \brief Convert a XYZRGBA point type to a XYZHSV
   * \param[in] in the input XYZRGBA point 
   * \param[out] out the output XYZHSV point
   */
  inline void 
  PointXYZRGBAtoXYZHSV (const PointXYZRGBA& in,
			PointXYZHSV& out)
  {
    float min;

    out.x = in.x; out.y = in.y; out.z = in.z;

    out.v = std::max (in.r, std::max (in.g, in.b));
    min = std::min (in.r, std::min (in.g, in.b));

    if (out.v != 0)
      out.s = (out.v - min) / out.v;
    else
      {
	out.s = 0;
	out.h = -1;
	return;
      }

    if (in.r == out.v)
      out.h = static_cast<float> (in.g - in.b) / (out.v - min);
    else if (in.g == out.v)
      out.h = static_cast<float> (2 + (in.b - in.r) / (out.v - min));
    else 
      out.h = static_cast<float> (4 + (in.r - in.g) / (out.v - min));
    out.h *= 60;
    if (out.h < 0)
      out.h += 360;
  }

  /** \brief Convert a XYZRGBA point cloud to a XYZHSV
   * \param[in] in the input XYZRGBA point cloud
   * \param[out] out the output XYZHSV point cloud
   */
  inline void 
  PointCloudXYZRGBAtoXYZHSV (const PointCloud<PointXYZRGBA>& in,
			     PointCloud<PointXYZHSV>& out)
  {
    out.width   = in.width;
    out.height  = in.height;
    for (size_t i = 0; i < in.points.size (); i++)
      {
	PointXYZHSV p;
	PointXYZRGBAtoXYZHSV (in.points[i], p);
	out.points.push_back (p);
      }
  }
}
#endif //PCL_TYPE_RGBATOHSV_H
