/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TABLETOP_XYPROJECTION_H
#define TABLETOP_XYPROJECTION_H

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/eigen.h>
#include <boost/scoped_ptr.hpp>
#include <vector>
#include <cfloat>

namespace ActionParsing{
  class TableTopXYProjection{
  protected:
    typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloud;
    typedef typename PointCloud::Ptr PointCloudPtr;
    typedef typename PointCloud::ConstPtr PointCloudConstPtr;
  public:
    TableTopXYProjection(const float projection_width, const float projection_height,
			 const float cloud_width_worldf, const float cloud_height_worldf);
    ~TableTopXYProjection();
    
    void transformPointCloudToXYProjection(const PointCloud &cloud_in, std::vector<float> &depth_projection_data, std::vector<unsigned char> &rgb_projection_data,
					   const Eigen::Affine3f to_world_tf = Eigen::Affine3f::Identity(),
					   const bool fill=true, const float depth_fill=0.0, const unsigned char rgb_fill=0);

    void transformPointCloudToXYProjection(const PointCloud &cloud_in, float* depth_projection_data, unsigned char* rgb_projection_data,
					   const Eigen::Affine3f to_world_tf = Eigen::Affine3f::Identity(),
					   const bool fill=true, const float depth_fill=0.0, const unsigned char rgb_fill=0);

    inline const Eigen::Affine3f& getTransformationToXYProjection() const { return to_xyprojection_tf_; }
    inline const Eigen::Affine3f& getScalingToXYProjection() const { return to_xyprojection_scaling_; }
  private:
    void calcTransformationToXYProjection();
    
    
    struct PImpl;
    boost::scoped_ptr<PImpl> impl_;
    float projection_width_;
    float projection_height_;
    float cloud_width_;
    float cloud_height_;
    Eigen::Affine3f to_xyprojection_scaling_;
    Eigen::Affine3f to_xyprojection_tf_;
  };
}
#endif //TABLETOP_XYPROJECTION_H
