/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TABLETOP_HANDCLASSIFIER_H
#define TABLETOP_HANDCLASSIFIER_H

#include <tabletop_segmentation/tabletop_object.h>
#include <pcl/point_cloud.h>
#include <pcl/PointIndices.h>
#include <boost/scoped_ptr.hpp>


namespace pcl{
  class PointXYZRGBA;
}


namespace ActionParsing{
  class TableTopHandClassifier{
  protected:
    typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloud;
    typedef typename PointCloud::Ptr PointCloudPtr;
    typedef typename PointCloud::ConstPtr PointCloudConstPtr;

  public:
    TableTopHandClassifier(const std::vector<float> &hand_hsv_min,
			   const std::vector<float> &hand_hsv_max,
			   const unsigned int min_points=100);
    ~TableTopHandClassifier();

    void setHandHSVValues(const std::vector<float> &hand_hsv_min,
			  const std::vector<float> &hand_hsv_max);

    void setMinPoints(const unsigned int min_points);

    
    bool classify(TableTopObjectPtr &object) const;

    /* bool */
    /* classifyHand(const PointCloudConstPtr &object_cloud, */
    /* 		 PointCloud &hand_cloud); */
    /* bool */
    /* classifyHand(const PointCloudConstPtr &object_cloud, */
    /* 		 const pcl::PointIndices &object_indices, */
    /* 		 PointCloud &hand_cloud); */

    bool
    classifyHand(const PointCloudConstPtr &object_cloud,
		 PointCloud &hand_cloud) const;
    bool
    classifyHand(const PointCloudConstPtr &object_cloud,
		 const pcl::PointIndices &object_indices,
		 PointCloud &hand_cloud) const;
 
  private:
    struct PImpl;
    boost::scoped_ptr<PImpl> impl_;
    unsigned int min_points_;
  };
}
#endif //TABLETOP_HANDCLASSIFIER_H
