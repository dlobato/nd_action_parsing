/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TABLETOP_3DPOINTTRACKER_H
#define TABLETOP_3DPOINTTRACKER_H
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>

namespace ActionParsing{
  class TableTop3DPointTracker;
  typedef boost::shared_ptr<TableTop3DPointTracker> TableTop3DPointTrackerPtr;
  typedef boost::shared_ptr<const TableTop3DPointTracker> TableTop3DPointTrackerConstPtr;

  class TableTop3DPointTracker{
  public:
    TableTop3DPointTracker(const int max_tracked_points=20,
			   const float max_distance_observation_to_prediction=0.2,
			   const int max_not_tracked_hits=10);

    ~TableTop3DPointTracker();

    void track(const std::vector<cv::Point3f> &points_to_track, std::vector<cv::Point3f> &tracked_points_position, std::vector<cv::Point3f> &tracked_points_velocity);

  private:
    struct PImpl;
    boost::scoped_ptr<PImpl> impl_;
    const int max_tracked_points_;
    const float max_distance_observation_to_prediction_;
    const int max_not_tracked_hits_;
  };

  
}

#endif //TABLETOP_3DPOINTTRACKER_H
