/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TABLETOP_OBJECT_CLASSIFIER_H
#define TABLETOP_OBJECT_CLASSIFIER_H

#include <tabletop_segmentation/tabletop_object.h>
#include <boost/scoped_ptr.hpp>

namespace ActionParsing{
  class TableTopObjectClassifier{
  public:
    TableTopObjectClassifier(const std::string datasetdir,
			     const unsigned int num_classes, const unsigned int num_views,
			     const bool randomise=false,const std::string ext="png",
			     const unsigned int image_limit=1000,
			     const unsigned int knnSearchChecks = 100);
    ~TableTopObjectClassifier();

    bool classify(TableTopObjectPtr &object) const;
  private:
    struct PImpl;
    boost::scoped_ptr<PImpl> impl_;
    unsigned int knnSearchChecks_;
  };
}//namespace ActionParsing
#endif //TABLETOP_OBJECT_CLASSIFIER_H
