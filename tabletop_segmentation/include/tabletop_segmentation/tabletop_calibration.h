/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TABLETOP_CALIBRATION_H
#define TABLETOP_CALIBRATION_H

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/eigen.h>
#include <boost/scoped_ptr.hpp>


namespace ActionParsing{
  /*! \ingroup tabletop
   *  \brief This class contains the functionality related to the calibration
   *  step. It can calibrate the tabletop area using two color markers.
   */
  class TableTopCalibration{
  protected:
    typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloud;
    typedef typename PointCloud::Ptr PointCloudPtr;
    typedef typename PointCloud::ConstPtr PointCloudConstPtr;
  public:
    /*!
     * Constructor.
     * HSV ranges for marker are H=[0,360],S=[0,1],V=[0,1]. Check hsvFromOpenCV() to convert from openCV ranges.
     * \param[in] focal_length camera focal lenght.
     * \param[in] image_center_x camera x image center in pixels.
     * \param[in] image_center_y camera y image center in pixels.
     * \param[in] o_marker_hsv_min HSV min values for origin marker.
     * \param[in] o_marker_hsv_max HSV max values for origin marker.
     * \param[in] x_marker_hsv_min HSV min values for x marker.
     * \param[in] x_marker_hsv_max HSV max values for x marker.
     * \param[in] min_marker_points Minumum number or points a marker must have to be valid.
     * \param[in] marker_filter_radius Minimum distance to neighbours.
     */
    TableTopCalibration(const float focal_length,
			const float image_center_x,
			const float image_center_y,
			const std::vector<float> &o_marker_hsv_min,
			const std::vector<float> &o_marker_hsv_max,
			const std::vector<float> &x_marker_hsv_min,
			const std::vector<float> &x_marker_hsv_max,
			const int min_marker_points=20,
			const float marker_filter_radius=0.05);//m
    /*!
     * Destructor.
     */
    ~TableTopCalibration();

    /*!
     * Set new hsv values for color markers.
     * HSV ranges for marker are H=[0,360],S=[0,1],V=[0,1]. Check hsvFromOpenCV() to convert from openCV ranges.
     * \param[in] o_marker_hsv_min HSV min values for origin marker.
     * \param[in] o_marker_hsv_max HSV max values for origin marker.
     * \param[in] x_marker_hsv_min HSV min values for x marker.
     * \param[in] x_marker_hsv_max HSV max values for x marker.
     */
    void setMarkerHSVValues(const std::vector<float> &o_marker_hsv_min,
			    const std::vector<float> &o_marker_hsv_max,
			    const std::vector<float> &x_marker_hsv_min,
			    const std::vector<float> &x_marker_hsv_max);

    /*!
     * Segment color markers from input cloud.
     * \param[in] input_cloud input cloud from where markers will be segmented.
     * \param[out] o_marker_cloud origin marker cloud.
     * \param[out] x_marker_cloud x marker cloud.
     * \return not 0 if both markers were found.
     */
    int segmentMarkers(const PointCloudConstPtr &input_cloud,
		       PointCloud &o_marker_cloud,
		       PointCloud &x_marker_cloud);

    /*!
     * %Filter color markers to remove outliers.
     * \param[in] o_marker_cloud origin marker cloud.
     * \param[in] x_marker_cloud x marker cloud.
     * \param[out] o_marker_cloud_filtered origin marker filtered cloud.
     * \param[out] x_marker_cloud_filtered x marker filtered cloud.
     * \return not 0 if both markers stood after filtering.
     */
    int filterMarkers(const PointCloudConstPtr &o_marker_cloud,
		      const PointCloudConstPtr &x_marker_cloud,
		      PointCloud &o_marker_cloud_filtered,
		      PointCloud &x_marker_cloud_filtered);

    /*!
     * Find calibration with an input cloud containing markers plus plane normal.
     * \param[in] input_cloud input cloud containing markers.
     * \param[in] plane_normal normal to the plane where markers are.
     * \return not 0 if calibration was successful.
     */
    int calibrate(const PointCloudConstPtr &input_cloud,//cloud with markers in it
		  const Eigen::Vector4f &plane_normal);
    
    /*!
     * Find calibration with marker positions plus plane normal.
     * \param[in] o_marker_position origin marker position. Vector4f[0:2] -> 3D position.
     * \param[in] x_marker_position x marker position.
     * \param[in] plane_normal normal to the plane where markers are.
     * \return not 0 if calibration was successful.
     */
    int calibrate(const Eigen::Vector4f &o_marker_position,//origin
		  const Eigen::Vector4f &x_marker_position,//x
		  const Eigen::Vector4f &plane_normal);

    /*!
     * Calculate marker positions from clouds.
     * \param[in] o_marker_cloud origin marker cloud.
     * \param[in] x_marker_cloud x marker cloud.
     * \param[out] o_marker_position origin marker position. Vector4f[0:2] -> 3D position.
     * \param[out] x_marker_position x marker position.
     */
    void calcMarkersCoordinates(const PointCloud &o_marker_cloud,
				const PointCloud &x_marker_cloud,
				Eigen::Vector4f &o_marker_position,
				Eigen::Vector4f &x_marker_position) const;

    /*!
     * Transform a point (3D) from device coordinates to world coordinates.
     * \param[in] p_devicef point in device coordinates.
     * return a point in world coordinates.
     */
    inline Eigen::Vector3f transformPointToWorldCoordinates(const Eigen::Vector3f &p_devicef) const { return to_world_tf_ * p_devicef; }

    /*!
     * Transform a point (3D) from world coordinates to device coordinates.
     * \param[in] p_worldf point in world coordinates.
     * return a point in device coordinates.
     */
    inline Eigen::Vector3f transformPointToDeviceCoordinates(const Eigen::Vector3f &p_worldf) const { return to_device_tf_ * p_worldf; }

    /*!
     * Transform a point (3D->2D) from device coordinates to image coordinates
     * \param[in] p_devicef point in device coordinates.
     * return a 2D point in image coordinates.
     */
    inline Eigen::Vector2f transformPointToImageCoordinates(const Eigen::Vector3f &p_devicef) const { return Eigen::Vector2f( ((focal_length_*p_devicef[0])/p_devicef[2]) + image_center_x_, 
															      ((focal_length_*p_devicef[1])/p_devicef[2]) + image_center_y_ ); }

    /*!
     * Transform a point cloud from device coordinates to world coordinates.
     * \param[in] cloud_in_devicef cloud in device coordinates.
     * \param[out] cloud_out_worldf cloud in world coordinates.
     */
    void transformPointCloudToWorldCoordinates(const PointCloud &cloud_in_devicef, PointCloud &cloud_out_worldf) const;

    /*!
     * Transform a point cloud from world coordinates to device coordinates.
     * \param[in] cloud_in_worldf cloud in world coordinates.
     * \param[out] cloud_out_devicef cloud in device coordinates.
     */
    void transformPointCloudToDeviceCoordinates(const PointCloud &cloud_in_worldf, PointCloud &cloud_out_devicef) const;
    
    /*!
     * Get transformation from device coordinates to world coordinates.
     * \return affine transformation from device coordinates to world coordinates.
     */
    inline const Eigen::Affine3f& getTransformationToWorldCoordinates() const { return to_world_tf_; }

    /*!
     * Get transformation from world coordinates to device coordinates.
     * \return affine transformation from world coordinates to device coordinates.
     */
    inline const Eigen::Affine3f& getTransformationToDeviceCoordinates() const { return to_device_tf_; }


    // const Eigen::Affine3f& getToXYProjectionTransformation(const float x_scaling,
    // 							   const float y_scaling,
    // 							   const float x_translation,
    // 							   const float y_translation) const;

  private:
    // Eigen::Affine3f&
    // calcTransformationToWorldCoordinates(const Eigen::Vector4f &o_marker_position,
    // 					 const Eigen::Vector4f &x_marker_position,
    // 					 const Eigen::Vector4f &plane_normal,
    // 					 Eigen::Affine3f &transformation,
    // 					 Eigen::Vector3f &o_position,
    // 					 Eigen::Vector3f &x_direction,
    // 					 Eigen::Vector3f &y_direction,
    // 					 Eigen::Vector3f &z_direction);

    /*!
     * Calculate calibration with marker positions plus plane normal.
     * \param[in] o_marker_position origin marker position. Vector4f[0:2] -> 3D position.
     * \param[in] x_marker_position x marker position.
     * \param[in] plane_normal normal to the plane where markers are.
     * \param[out] affine transformation from device coordinates to world coordinates.
     */
    void
    calcTransformationToWorldCoordinates(const Eigen::Vector4f &o_marker_position,
					 const Eigen::Vector4f &x_marker_position,
					 const Eigen::Vector4f &plane_normal,
					 Eigen::Affine3f &to_world_tf) const;

    // Eigen::Affine3f&
    // calcTransformationToXYProjection(const Eigen::Vector3f tabletop_dimensions,
    // 				     const float xyprojection_width,
    // 				     const float xyprojection_height,
    // 				     Eigen::Affine3f &to_xyprojection_tf) const;//FIXME: either change name or make more general(signs)
    
    struct PImpl;
    boost::scoped_ptr<PImpl> impl_;
    float focal_length_;
    float image_center_x_;
    float image_center_y_;
    int min_marker_points_;
    float marker_filter_radius_;
    Eigen::Affine3f to_world_tf_;
    Eigen::Affine3f to_device_tf_;
  };
}
#endif //TABLETOP_CALIBRATION_H
