/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TABLETOP_OBJECT_H
#define TABLETOP_OBJECT_H
#include <tabletop_segmentation/tabletop_calibration.h>
#include <string>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/eigen.h>
#include <opencv2/opencv.hpp>
//#include <boost/scoped_ptr.hpp>

namespace ActionParsing{
  class TableTopObject;
  typedef boost::shared_ptr<TableTopObject> TableTopObjectPtr;
  typedef boost::shared_ptr<const TableTopObject> TableTopObjectConstPtr;

  class TableTopObject{
  protected:
    typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloud;
    typedef typename PointCloud::Ptr PointCloudPtr;
    typedef typename PointCloud::ConstPtr PointCloudConstPtr;
  public:
    /* TableTopObject(const PointCloud &object_cloud, */
    /* 		   const cv::Mat &object_rgb_image, */
    /* 		   const std::string frame_id, */
    /* 		   const unsigned long stamp);//copy pointcloud data */

    TableTopObject(const PointCloud &object_cloud,
		   const cv::Mat &object_rgb_image);//copy pointcloud data

    /* TableTopObject(const PointCloud &objects_cloud, */
    /* 		   const std::vector<int> object_indices, */
    /* 		   const cv::Mat &object_rgb_image, */
    /* 		   const std::string frame_id, */
    /* 		   const unsigned long stamp);//copy pointcloud data */

    TableTopObject(const PointCloud &objects_cloud,
		   const std::vector<int> object_indices,
		   const cv::Mat &object_rgb_image);//copy pointcloud data

    /* TableTopObject(const PointCloudConstPtr &object_cloud, */
    /* 		   const cv::Mat &object_rgb_image, */
    /* 		   const std::string frame_id, */
    /* 		   const unsigned long stamp);//no copy pointcloud data */

    TableTopObject(const PointCloudConstPtr &object_cloud,
		   const cv::Mat &object_rgb_image);//no copy pointcloud data
    

    ~TableTopObject();

    Eigen::Vector3f getCentroid() const;
    /* const std::string getFrameId() const { return frame_id_; } */
    /* unsigned long getStamp() const { return stamp_; } */
    /* const PointCloud& getCloud() const { return *cloud_; } */
    /* const PointCloudConstPtr& getCloudConstPtr() const { return cloud_; } */
    /* const cv::Mat& getRGBImage() const { return rgb_image_; } */
    /* const cv::Mat& getGRAYImage() const { return gray_image_; } */
    /* int getLabelId() const { return label_id_; } */
    /* const std::string getLabel() const { return label_; } */

    /* void setLabelId(const int label_id) { label_id_ = label_id; } */
    /* void setLabel(const std::string& label) { label_ = label; } */
    

    //(Length|x, Width|y, Height|z, Area)
    //Eigen::Vector4f& getDimensions() const;

    static const cv::Mat findObjectRGBImage(const PointCloud &object_cloud, const cv::Mat &full_rgb_image, const TableTopCalibration &calibrator, const int border_size=0);
    static const cv::Mat findObjectRGBImage(const PointCloud &objects_cloud, const std::vector<int> object_indices, const cv::Mat &full_rgb_image, const TableTopCalibration &calibrator, const int border_size=0);

    const PointCloudConstPtr cloud;
    const cv::Mat rgb_image;
    const pcl::PCLHeader& header;
    int label_id;
    std::string label;
  };

  
}//namespace ActionParsing

#endif //TABLETOP_OBJECT_H
