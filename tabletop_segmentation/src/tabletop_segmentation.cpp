/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tabletop_segmentation/tabletop_segmentation.h>
// PCL specific includes
#include <pcl/common/time.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <pcl/common/eigen.h>
#include <pcl/common/transforms.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/surface/convex_hull.h>
#include <cstring>
#include <cmath>

#define PROFILE 0
#define DEBUG 0
#include <pcl/myutils/debugutils.h>


namespace ActionParsing{
  struct TableTopSegmentation::PImpl{
    pcl::SACSegmentation<pcl::PointXYZRGBA> seg;
    pcl::ProjectInliers<pcl::PointXYZRGBA> proj_inliers;
    pcl::ExtractIndices<pcl::PointXYZRGBA> extract;
    pcl::ConvexHull<pcl::PointXYZRGBA> chull;
    pcl::EuclideanClusterExtraction<pcl::PointXYZRGBA> ec;
    pcl::EuclideanClusterExtraction<pcl::PointXYZRGBA> object_clustering;
    pcl::ExtractPolygonalPrismData<pcl::PointXYZRGBA> polygon_extract;
  };

  TableTopSegmentation::TableTopSegmentation(const float plane_segmentation_threshold,
					     const int plane_segmentation_sac_max_iterations,
					     const bool plane_segmentation_find_main_cluster,
					     const float plane_segmentation_cluster_tolerance,
					     const int plane_segmentation_cluster_min_size,
					     const int plane_segmentation_cluster_max_size,
					     const float object_segmentation_min_height,
					     const float object_segmentation_max_height,
					     const float object_segmentation_cluster_tolerance,
					     const int object_segmentation_cluster_min_size,
					     const int object_segmentation_cluster_max_size)
    :impl_(new PImpl()), plane_segmentation_find_main_cluster_(plane_segmentation_find_main_cluster)
  {
    impl_->seg.setOptimizeCoefficients (true);
    impl_->seg.setModelType (pcl::SACMODEL_PLANE);
    impl_->seg.setMethodType (pcl::SAC_RANSAC);
    impl_->seg.setMaxIterations (plane_segmentation_sac_max_iterations);
    impl_->seg.setDistanceThreshold (plane_segmentation_threshold);

    impl_->proj_inliers.setModelType (pcl::SACMODEL_PLANE);

    impl_->extract.setNegative (false);

    impl_->chull.setDimension (2);

    impl_->ec.setClusterTolerance (plane_segmentation_cluster_tolerance);
    impl_->ec.setMinClusterSize (plane_segmentation_cluster_min_size);
    impl_->ec.setMaxClusterSize (plane_segmentation_cluster_max_size);

    impl_->object_clustering.setClusterTolerance (object_segmentation_cluster_tolerance);
    impl_->object_clustering.setMinClusterSize (object_segmentation_cluster_min_size);
    impl_->object_clustering.setMaxClusterSize (object_segmentation_cluster_max_size);

    assert(object_segmentation_min_height < object_segmentation_max_height && "min_height < max_height");
    impl_->polygon_extract.setHeightLimits (object_segmentation_min_height,
					    object_segmentation_max_height);
  } 

  TableTopSegmentation::~TableTopSegmentation() {}

  void TableTopSegmentation::setPlaneThreshold(const float threshold){
    impl_->seg.setDistanceThreshold (threshold);
  }

  TableTopSegmentation::PointCloudPtr
  TableTopSegmentation::segmentTableTop (const PointCloudConstPtr &input_cloud,
					 PointCloudPtr &table_cloud,
					 Eigen::Vector4f &table_plane_normal){
    assert(input_cloud && "input_cloud == null");
    if (input_cloud->empty()){
      PCL_ERROR("[ActionParsing::TableTopSegmentation::segmentTableTop] input_cloud empty!\n");
      return PointCloudPtr();
    }

    FPS_CALC_BEGIN;
    pcl::ModelCoefficientsPtr table_plane_coefficients(new pcl::ModelCoefficients);
    
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    impl_->seg.setInputCloud (input_cloud);
    impl_->seg.segment (*inliers, *table_plane_coefficients);

    //no plane found
    if (inliers->indices.empty()){
      table_cloud.reset(new PointCloud);
      table_plane_normal = Eigen::Vector4f::Zero();
      return table_cloud;
    }

    table_plane_normal << table_plane_coefficients->values[0],
      table_plane_coefficients->values[1],
      table_plane_coefficients->values[2],
      table_plane_coefficients->values[3];
    table_plane_normal.normalize();

    if (!table_cloud)
      table_cloud.reset(new PointCloud);
    table_cloud->header = input_cloud->header;

    //project inliers using plane model
    impl_->proj_inliers.setInputCloud (input_cloud);
    impl_->proj_inliers.setIndices(inliers);
    impl_->proj_inliers.setModelCoefficients(table_plane_coefficients);
    impl_->proj_inliers.filter (*table_cloud);

    if (plane_segmentation_find_main_cluster_){
      std::vector<pcl::PointIndices> cluster_indices;
      pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBA>);
      tree->setInputCloud (table_cloud);

      impl_->ec.setSearchMethod (tree);
      impl_->ec.setInputCloud (table_cloud);
      impl_->ec.extract (cluster_indices);

      if (cluster_indices.size() != 0){//if clustering fails, table_cloud isn't changed
	PointCloudPtr table_cloud_filtered (new PointCloud);
	pcl::PointIndices::Ptr main_cluster_indices(new pcl::PointIndices(*cluster_indices.begin()));
	impl_->extract.setInputCloud (table_cloud);
	impl_->extract.setIndices (main_cluster_indices);
	impl_->extract.filter (*table_cloud_filtered);
	table_cloud = table_cloud_filtered;
      }
    }
    FPS_CALC_END("segmentTableTop");
    return table_cloud;//success
  }

  // TableTopSegmentation::PointCloudPtr
  // TableTopSegmentation::segmentObjects (const PointCloudConstPtr &input_cloud, 
  // 					const PointCloudConstPtr &table_cloud,
  // 					PointCloudPtr &objects_cloud){
  //   assert(input_cloud && "input_cloud == null");
  //   if (input_cloud->empty()){
  //     PCL_ERROR("[ActionParsing::TableTopSegmentation::segmentObjects] input_cloud empty!\n");
  //     return PointCloudPtr();
  //   }

  //   assert(table_cloud && "table_cloud == null");
  //   if (table_cloud->empty()){
  //     PCL_ERROR("[ActionParsing::TableTopSegmentation::segmentObjects] table_cloud empty!\n");
  //     return PointCloudPtr();
  //   }

  //   if (!objects_cloud)
  //     objects_cloud.reset(new PointCloud);
  //   objects_cloud->header = input_cloud->header;

  //   FPS_CALC_BEGIN;
  //   PointCloudPtr table_hull (new PointCloud);
  //   pcl::PointIndices::Ptr objects_indices(new pcl::PointIndices ());

  //   calcTabletopHull(table_cloud,table_hull);

  //   assert(table_hull->size() != 0 && "error reconstructing table hull");

  //   segmentObjects(input_cloud,table_hull,0.02,2.0,objects_cloud);
  //   FPS_CALC_END("segmentObjects");
  //   return objects_cloud;//success
  // }

  TableTopSegmentation::PointCloudPtr
  TableTopSegmentation::segmentObjects (const PointCloudConstPtr &input_cloud, 
					const PointCloudConstPtr &table_hull,
					PointCloudPtr &objects_cloud){
    assert(input_cloud && "input_cloud == null");
    if (input_cloud->empty()){
      PCL_ERROR("[ActionParsing::TableTopSegmentation::segmentObjects] input_cloud empty!\n");
      return PointCloudPtr();
    }

    assert(table_hull && "table_hull == null");
    if (table_hull->empty()){
      PCL_ERROR("[ActionParsing::TableTopSegmentation::segmentObjects] table_hull empty!\n");
      return PointCloudPtr();
    }

    if (!objects_cloud)
      objects_cloud.reset(new PointCloud);
    objects_cloud->width = objects_cloud->height = 0;
    objects_cloud->points.clear();

    FPS_CALC_BEGIN;
    pcl::PointIndices::Ptr objects_indices(new pcl::PointIndices ());
    
    impl_->polygon_extract.setInputPlanarHull (table_hull);
    impl_->polygon_extract.setInputCloud (input_cloud);
    impl_->polygon_extract.segment (*objects_indices);

    // if (objects_indices->indices.empty())
    //   objects_cloud->points.resize(0);
    // else{
    impl_->extract.setInputCloud (input_cloud);
    impl_->extract.setIndices (objects_indices);
    impl_->extract.filter (*objects_cloud);
    //}
    FPS_CALC_END("segmentObjects");
    return objects_cloud;//success
  }
  
  TableTopSegmentation::PointCloudPtr
  TableTopSegmentation::segmentAndClusterObjects (const PointCloudConstPtr &input_cloud, 
						  const PointCloudConstPtr &table_hull,
						  PointCloudPtr &objects_cloud, 
						  std::vector<pcl::PointIndices> &object_clusters ){
    FPS_CALC_BEGIN;
    if (segmentObjects(input_cloud,table_hull,objects_cloud)){
      object_clusters.clear();
      pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBA>);
      tree->setInputCloud (objects_cloud);
      impl_->object_clustering.setSearchMethod (tree);
      impl_->object_clustering.setInputCloud (objects_cloud);
      impl_->object_clustering.extract (object_clusters);
    }
    FPS_CALC_END("segmentAndClusterObjects");
    return objects_cloud;
  }

  

  // std::vector<pcl::PointIndices>&
  // TableTopSegmentation::segmentHands(const PointCloudConstPtr &input_cloud, 
  // 				     std::vector<pcl::PointIndices> &object_clusters, 
  // 				     std::vector<pcl::PointIndices> &hand_clusters){
  //   if (!objects_cloud || input_cloud->empty())

  //   FPS_CALC_BEGIN;
  //   std::vector<pcl::PointIndices> new_object_clusters;
  //   std::vector<pcl::PointIndices>::iterator object_clusters_it;

  //   hand_clusters.clear();

  //   for (object_clusters_it = object_clusters.begin();
  // 	 object_clusters_it != object_clusters.end();
  // 	 object_clusters_it++){
  //     pcl::PointIndices hand_indices;
  //     if (segmentHand(objects_cloud,*object_clusters_it,hand_indices))//if hand detected add to hand_clusters
  // 	hand_clusters.push_back(hand_indices);
  //     else //add to new objects cluster
  // 	new_object_clusters.push_back(*object_clusters_it);
  //   }
  //   object_clusters = new_object_clusters;//should be object_clusters minus hand_clusters
  //   FPS_CALC_END("segmentHands");
  //   return hand_clusters;
  // }

  // pcl::PointIndices&
  // TableTopSegmentation::segmentHand(const PointCloudConstPtr &input_cloud, 
  // 				    pcl::PointIndices &indices, 
  // 				    pcl::PointIndices &hand_indices, int min_points){
    

  //   FPS_CALC_BEGIN;
  //   PointCloudPtr hand_color_cloud (new PointCloud);
  //   pcl::ConditionalRemoval<pcl::PointXYZRGBA> hand_color_filter;
  //   pcl::IndicesConstPtr indices_p = boost::make_shared<const std::vector<int> >(indices.indices);

  //   hand_indices.header = input_cloud->header;
  //   hand_indices.indices.clear();
    
  //   //filter input cloud by color
  //   hand_color_filter.setInputCloud(input_cloud);
  //   hand_color_filter.setIndices(indices_p);
  //   hand_color_filter.filter(*hand_color_cloud);
  //   if (hand_color_cloud->size() > min_points){
  //     std::cout << "hand_color_cloud.size=" << hand_color_cloud->size() << std::endl;
  //     //get cloud centroid
  //     Eigen::Vector4f hand_color_cloud_centroid;
  //     pcl::compute3DCentroid(*hand_color_cloud, hand_color_cloud_centroid);
  //     //find neighbors
  //     pcl::PointXYZRGBA search_point;
  //     search_point.x = hand_color_cloud_centroid[0];
  //     search_point.y = hand_color_cloud_centroid[1];
  //     search_point.z = hand_color_cloud_centroid[2];
  //     pcl::search::KdTree<pcl::PointXYZRGBA> tree;
  //     tree.setInputCloud (input_cloud,indices_p);
  //     float hand_radius = 0.07;//m FIXME: set as parameter
  //     std::vector<int> k_indices;
  //     std::vector<float> k_sqr_distances;
  //     int search_out;
  //     if (search_out = tree.radiusSearch (search_point, hand_radius, k_indices, k_sqr_distances))
  // 	hand_indices.indices = k_indices;
  //     std::cout << "radiusSearch=" << search_out << std::endl;
  //   }
  //   FPS_CALC_END("segmentHand");
  //   return hand_indices;
  // }

  
  Eigen::Vector3f TableTopSegmentation::calcTabletopSize(const PointCloud &table_cloud){
    Eigen::Vector3f tabletop_size;
    PointCloud::PointType min3d,max3d;
    pcl::getMinMax3D(table_cloud,min3d,max3d);
    tabletop_size << std::abs(min3d.x-max3d.x),
      std::abs(min3d.y-max3d.y),
      std::abs(min3d.z-max3d.z);
    return tabletop_size;
  }

  void TableTopSegmentation::calcTabletopMinMax3D(const PointCloud &table_cloud, Eigen::Vector3f &min, Eigen::Vector3f &max){
    PointCloud::PointType min3d,max3d;
    pcl::getMinMax3D(table_cloud,min3d,max3d);
    min << min3d.x, min3d.y, min3d.z;
    max << max3d.x, max3d.y, max3d.z;
  }

  TableTopSegmentation::PointCloudPtr
  TableTopSegmentation::calcTabletopHull(const PointCloudConstPtr &table_cloud,
					 PointCloudPtr &table_hull){
    assert(table_cloud && "table_cloud == null");
    if (table_cloud->empty()){
      PCL_ERROR("[ActionParsing::TableTopSegmentation::calcTabletopHull] table_cloud empty");
      return PointCloudPtr();
    }

    if (!table_hull)
      table_hull.reset(new PointCloud);
    table_hull->header = table_cloud->header;

    pcl::ConvexHull<pcl::PointXYZRGBA> chull;
    chull.setDimension (2);
    chull.setInputCloud (table_cloud);
    chull.reconstruct (*table_hull);
    return table_hull;
  }
}
