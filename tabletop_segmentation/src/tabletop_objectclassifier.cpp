/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tabletop_segmentation/tabletop_objectclassifier.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <vl_common.h>
#include <vl_keypoints.h>
#include <benchmark.h>
#include <kptools.h>
#include <sstream>
#include <fstream>

#define PROFILE 0
#define DEBUG 0
#include <pcl/myutils/debugutils.h>

namespace ActionParsing{
  void extractFeaturesMat(const cv::Mat& input_img, const std::vector<Type>& lambdas, const float alpha, const float base, 
			  const cv::SiftDescriptorExtractor& extractor, ImgInfo& info){
    // std::cout <<"A" << std::endl;
    //std::cout << "[extractFeaturesMat] input_img.size(rowsxcols)=" << input_img.rows << "x" << input_img.cols << std::endl;
    float xf = input_img.cols, yf = input_img.rows;
    cv::Mat img; // = _img;
    if(input_img.cols > input_img.rows) { xf = yf = input_img.cols; }
    else                                { xf = yf = input_img.rows; }
            
    cv::resize(input_img, img, cv::Size(), base/xf, base/yf, INTER_CUBIC );
    // std::cout <<"B" << std::endl;

    //std::cout << "[extractFeaturesMat] img.size(rows x cols)=" << img.rows << "x" << img.cols << std::endl;
  
    // img = _img;
    // if(useROI) img = img(files_train[i].ROI[0]);
    info.width = input_img.cols;
    info.height = input_img.rows;
  
    std::vector<bimp::KPData> datas;
    std::vector<cv::KeyPoint> points;
    //points = getKeypointsGrid(img,lambdas,10);
    points = bimp::keypoints(img,lambdas,datas,8,true);
    for(unsigned a=0; a<points.size(); a++) points[a].size *= 2;
    //std::cout << "[extractFeaturesMat] points.size()=" << points.size() << std::endl;
    cv::Mat descs3;
    extractor.compute( img, points, descs3 );
    //std::cout << "[extractFeaturesMat] desc3.size(row x cols)=" << descs3.rows << "x" << descs3.cols << std::endl;
    //int labnum = info.labelnum;

    // Points + position
    for(int row=0; row<descs3.rows; row++){
      // if(points[row].size > 2) continue;
      cv::Mat currow = descs3.row(row);
      cv::Mat newrow(1,130,CV_32F);
      for(int col=0; col<currow.cols; col++)
	newrow.at<float>(0,col) = currow.at<float>(0,col);
      newrow.at<float>(0,128) = (points[row].pt.x - img.cols/2) * alpha;
      newrow.at<float>(0,129) = (points[row].pt.y - img.rows/2) * alpha;
    
      if(info.descs3.empty()) info.descs3 = newrow;
      else info.descs3.push_back(newrow);
    }
    info.points = points; 
  }

  void extractFeaturesFiles(std::vector<ImgInfo>& files_info, const std::vector<Type>& lambdas, 
			    const float alpha, const float base, const cv::SiftDescriptorExtractor& extractor ){
    #pragma omp parallel for schedule(dynamic,1) default(shared)
    for(unsigned i=0; i<files_info.size(); i++){
      cv::Mat _img = imread(files_info[i].filename,0);
      extractFeaturesMat(_img,lambdas,alpha,base,extractor,files_info[i]);
      //if(i%10 == 0) std::cout << "." << std::flush;
    }
  }

  struct TableTopObjectClassifier::PImpl{
    PImpl(const std::string datasetdir,
	  const unsigned int num_classes, const unsigned int num_views,
	  const bool randomise=false,const std::string ext="png",const unsigned int image_limit=1000)
      : num_classes(num_classes), alpha(2), base(100), classes_train(num_classes), 
	extractor(cv::SiftDescriptorExtractor(cv::SIFT::DescriptorParams::GET_DEFAULT_MAGNIFICATION(),true,false)) {
	
      std::vector<ImgInfo> files_test;//not used but needed by the function
      getGenericDatasets(datasetdir, num_classes, num_views, objects, files_train, files_test, ext, randomise, image_limit);
	
      for(unsigned i=0; i<num_classes; i++){
	classes_train[i].labelnum = i;
	classes_train[i].label = objects[i];
      }
      lambdas = bimp::makeLambdasLog(8, 16, 2);
      //lambdas = bimp::makeLambdasLin(16, 16, 1);
	
      // Extract features of all learning objects
      DEBUG_MESSAGE("Extracting features from training objects ");
      extractFeaturesFiles( files_train, lambdas, alpha, base, extractor );
      DEBUG_MESSAGE(std::cout << "done");
      DEBUG_MESSAGE(files_train.size());
      
      // Create a unified set of descriptors and class labels for all images and classes
      for(unsigned i=0; i<files_train.size(); i++)
	{
	  int labnum = files_train[i].labelnum;
	  cv::Mat descs3 = files_train[i].descs3;
	  
	  for(int row=0; row<descs3.rows; row++)
	    {
	      cv::Mat currow = descs3.row(row);
	      
	      if(alldescs.empty()) alldescs = currow;
	      else alldescs.push_back(currow);
	      alllabels.push_back(labnum);
	    }
	}

      DEBUG_MESSAGE(alldescs.rows << " " << alldescs.type());
      DEBUG_MESSAGE("Creating K-D tree... ");
      alldescs_index.reset(new cv::flann::Index(alldescs, cv::flann::KDTreeIndexParams(4)));
      DEBUG_MESSAGE("done");
    }

    unsigned int num_classes;
    unsigned int alpha;
    unsigned int base;
    std::vector<std::string> objects;
    std::vector<ImgInfo> files_train;
    // bimp::ConfusionMatrix conf_matrix;
    std::vector<ImgInfo> classes_train;
    cv::SiftDescriptorExtractor extractor;
    vector<Type> lambdas;
    cv::Mat alldescs;
    boost::shared_ptr<cv::flann::Index> alldescs_index;
    std::vector<int> alllabels;
  };

  TableTopObjectClassifier::TableTopObjectClassifier(const std::string datasetdir,
						     const unsigned int num_classes, const unsigned int num_views,
						     const bool randomise, const std::string ext,
						     const unsigned int image_limit,
						     const unsigned int knnSearchChecks)
    :impl_(new PImpl(datasetdir,num_classes,num_views,randomise, ext, image_limit)),
     knnSearchChecks_(knnSearchChecks){
  }

  TableTopObjectClassifier::~TableTopObjectClassifier() {}

  bool TableTopObjectClassifier::classify(TableTopObjectPtr &object) const{
    FPS_CALC_BEGIN;
    ImgInfo img_info;
    int label_id;
    cv::Mat gray_image;
    cv::cvtColor(object->rgb_image,gray_image,CV_RGB2GRAY);
    extractFeaturesMat(gray_image, impl_->lambdas, impl_->alpha, impl_->base, impl_->extractor, img_info);
    label_id = classifyObjectsSiftLocalNN( img_info, impl_->alllabels, impl_->alldescs_index.get(), impl_->num_classes, knnSearchChecks_ );
    assert(label_id < impl_->num_classes);
    object->label_id = label_id;
    object->label = impl_->classes_train[label_id].label;
    FPS_CALC_END(__PRETTY_FUNCTION__);
    return true;//close world classifier, always classify object
  }
}//namespace ActionParsing
