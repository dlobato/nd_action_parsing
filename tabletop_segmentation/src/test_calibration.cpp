/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tabletop_segmentation/tabletop_segmentation.h>
#include <tabletop_segmentation/tabletop_calibration.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/console/parse.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/io/openni_camera/openni_driver.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/image_viewer.h>
#include <opencv2/opencv.hpp>
#include <sstream>

namespace ActionParsing{
  class OpenNITableTopApp {
  protected:
    typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloud;
    typedef typename PointCloud::Ptr PointCloudPtr;
    typedef typename PointCloud::ConstPtr PointCloudConstPtr;
    typedef openni_wrapper::Image Image;
    typedef typename Image::Ptr ImagePtr;
    typedef typename Image::ConstPtr ImageConstPtr;
  public:
    OpenNITableTopApp(const std::string& device_id, const bool invert_normal,
		      const float focal_length,
		      const float image_center_x,
		      const float image_center_y,
		      const std::vector<float> &o_marker_hsv_min,
		      const std::vector<float> &o_marker_hsv_max,
		      const std::vector<float> &x_marker_hsv_min,
		      const std::vector<float> &x_marker_hsv_max)
      : segmenter(),
	calibrator(focal_length, image_center_x, image_center_y, o_marker_hsv_min, o_marker_hsv_max,x_marker_hsv_min, x_marker_hsv_max),
	device_id_ (device_id), invert_normal_(invert_normal), viewer ("Tabletop Calibration") {
      //downsampling grid
      grid_.setFilterFieldName ("z");
      grid_.setFilterLimits (0.0f, 2.0f);
      grid_.setLeafSize (0.005f, 0.005f, 0.005f);
    }
  
    void 
    cloud_cb_ (const PointCloudConstPtr& cloud){
      set (cloud);
    }

    void image_cb_(const ImagePtr& image){
      set_image(image);
    }

    void
    set (const PointCloudConstPtr& cloud){
      //lock while we set our cloud;
      boost::mutex::scoped_lock lock (mtx_);
      cloud_  = cloud;
    }

    PointCloudPtr
    get(){
      //lock while we swap our cloud and reset it.
      boost::mutex::scoped_lock lock (mtx_);
      PointCloudPtr c(new PointCloud(*cloud_));
      return c;
    }

    
    PointCloudPtr
    get_downsampled(){
      //lock while we swap our cloud and reset it.
      boost::mutex::scoped_lock lock (mtx_);
      PointCloudPtr downsampled_cloud (new PointCloud);
      
      grid_.setInputCloud (cloud_);
      grid_.filter (*downsampled_cloud);//downsample
      return (downsampled_cloud);
    }
    
    void set_image(const ImageConstPtr& image){
      //lock while we set our image;
      boost::mutex::scoped_lock lock (image_mtx_);
      image_ = image;
    }

    cv::Mat&
    get_image_cv(cv::Mat &image_cv){
      //lock while we set our image;
      boost::mutex::scoped_lock lock (image_mtx_);
      unsigned int width = image_->getWidth();
      unsigned int height = image_->getHeight();
      image_cv.create(height,width,CV_8UC3);
      assert(image_cv.isContinuous() && "image_cv.isContinuous()");
      image_->fillRGB(width,height,image_cv.ptr<unsigned char>());
      return image_cv;
    }

    void
    run (){
      pcl::Grabber* interface = new pcl::OpenNIGrabber (device_id_);
      
      //register cloud cb
      boost::function<void (const PointCloudConstPtr&)> f1 = boost::bind (&OpenNITableTopApp::cloud_cb_, this, _1);
      boost::signals2::connection c1 = interface->registerCallback (f1);
      //register image cb
      boost::function<void (const ImagePtr&)> f2 = boost::bind (&OpenNITableTopApp::image_cb_, this, _1);
      boost::signals2::connection c2 = interface->registerCallback (f2);

      PointCloudPtr downsampled_cloud;
      PointCloudPtr table_cloud(new PointCloud);
      PointCloudPtr objects_cloud(new PointCloud);
      cv::Mat image_cv;
      Eigen::Vector4f table_plane_normal;
      
      //set viewer state
      viewer.setBackgroundColor (0, 0, 0);
      viewer.createViewPort (0, 0, 0.25, 1, vp_1);
      viewer.createViewPort (0.25, 0, 0.5, 1, vp_2);
      viewer.createViewPort (0.5, 0, 0.75, 1, vp_3);
      viewer.createViewPort (0.75, 0, 1, 1, vp_4);
      viewer.addCoordinateSystem (0.5, vp_4);
      interface->start ();
      

      int tabletop_counter=0;
      bool calibrated = false;
      PointCloudPtr table_hull(new PointCloud);

      while (!viewer.wasStopped ()){
	if (cloud_ && image_){
	  downsampled_cloud = get_downsampled();
	  get_image_cv(image_cv);
	  //draw downsampled cloud
	  if (!viewer.updatePointCloud(downsampled_cloud,"cloud"))
	    viewer.addPointCloud(downsampled_cloud,"cloud",vp_1);

	  if (tabletop_counter%10 == 0){//segment plane every 50 frames
	    segmenter.segmentTableTop(downsampled_cloud,table_cloud,table_plane_normal);
	    if (invert_normal_){
	      table_plane_normal *= -1.0;
	    }
	    segmenter.calcTabletopHull(table_cloud,table_hull);
	    //draw table cloud
	    if (!viewer.updatePointCloud(table_cloud,"table"))
	      viewer.addPointCloud(table_cloud,"table",vp_2);

	    
	    PointCloudPtr o_marker_cloud(new PointCloud);
	    PointCloudPtr x_marker_cloud(new PointCloud);
	    if (calibrator.segmentMarkers(table_cloud, *o_marker_cloud, *x_marker_cloud)){
	      std::cerr << "O marker size=" << o_marker_cloud->size() << std::endl;
	      std::cerr << "X marker size=" << x_marker_cloud->size() << std::endl;
	      //draw markers before filtering
	      if (!viewer.updatePointCloud(o_marker_cloud,"o_marker"))
		viewer.addPointCloud(o_marker_cloud,"o_marker",vp_3);
	      if (!viewer.updatePointCloud(x_marker_cloud,"x_marker"))
		viewer.addPointCloud(x_marker_cloud,"x_marker",vp_3);
	      
	      PointCloudPtr o_marker_cloud_filtered(new PointCloud);
	      PointCloudPtr x_marker_cloud_filtered(new PointCloud);
	      if (calibrator.filterMarkers(o_marker_cloud, x_marker_cloud, *o_marker_cloud_filtered, *x_marker_cloud_filtered)){
		std::cerr << "O marker filtered size=" << o_marker_cloud_filtered->size() << std::endl;
		std::cerr << "X marker filtered size=" << x_marker_cloud_filtered->size() << std::endl;
		
		//draw markers after filtering
		if (!viewer.updatePointCloud(o_marker_cloud_filtered,"o_marker"))
		  viewer.addPointCloud(o_marker_cloud_filtered,"o_marker",vp_4);
		if (!viewer.updatePointCloud(x_marker_cloud_filtered,"x_marker"))
		  viewer.addPointCloud(x_marker_cloud_filtered,"x_marker",vp_4);
		
		Eigen::Vector4f o_marker_position,x_marker_position;
		calibrator.calcMarkersCoordinates(*o_marker_cloud_filtered, *x_marker_cloud_filtered, o_marker_position, x_marker_position);
	    
		if ((calibrated = calibrator.calibrate(o_marker_position, x_marker_position, table_plane_normal))){
		  const Eigen::Affine3f &to_device_tf = calibrator.getTransformationToDeviceCoordinates();
		  Eigen::Vector3f o_position,x_direction,y_direction,z_direction;
		  o_position = to_device_tf * Eigen::Vector3f::Zero(3);
		  x_direction = to_device_tf * (Eigen::Vector3f::UnitX() * 0.5);
		  y_direction = to_device_tf * (Eigen::Vector3f::UnitY() * 0.5);
		  z_direction = to_device_tf * (Eigen::Vector3f::UnitZ() * 0.5);
		  viewer.removeAllShapes(vp_3);
		  viewer.addLine(pcl::PointXYZ(o_position[0],o_position[1],o_position[2]),
				 pcl::PointXYZ(x_direction[0],x_direction[1],x_direction[2]),
				 255.0,0.0,0.0,"tabletop_tf_arrow_x",vp_3);
		  viewer.addLine(pcl::PointXYZ(o_position[0],o_position[1],o_position[2]),
				 pcl::PointXYZ(y_direction[0],y_direction[1],y_direction[2]),
				 0.0,255.0,0.0,"tabletop_tf_arrow_y",vp_3);
		  viewer.addLine(pcl::PointXYZ(o_position[0],o_position[1],o_position[2]),
				 pcl::PointXYZ(z_direction[0],z_direction[1],z_direction[2]),
				 0.0,0.0,255.0,"tabletop_tf_arrow_z",vp_3);
		}
	      }
	    }
	  }
	
	  
	  if (segmenter.segmentObjects(downsampled_cloud,table_hull,objects_cloud)->size() != 0){
	    PointCloudPtr objects_cloud_worldf(new PointCloud);
	    calibrator.transformPointCloudToWorldCoordinates(*objects_cloud,*objects_cloud_worldf);
	    
	    //draw objects cloud
	    if (!viewer.updatePointCloud(objects_cloud_worldf,"objects"))
	      viewer.addPointCloud(objects_cloud_worldf,"objects",vp_4);
	  }
	
	  if (calibrated){
	    Eigen::Vector3f o_worldf(0.0,0.0,0.0);//origin
	    Eigen::Vector3f o_devicef = calibrator.transformPointToDeviceCoordinates(o_worldf);
	    Eigen::Vector2f o_imagef = calibrator.transformPointToImageCoordinates(o_devicef);
	    cv::Point p(static_cast<int>(pcl_round(o_imagef[0])),static_cast<int>(pcl_round(o_imagef[1])));
	    
	    cv::circle(image_cv, p, 5, cv::Scalar(255,0,0), -1);
	  }

	  image_viewer.addRGBImage(image_cv.ptr<unsigned char>(),image_cv.cols, image_cv.rows);
	    
	  viewer.spinOnce(100);
	  image_viewer.spinOnce(100);
	  tabletop_counter++;
	}
      }
      interface->stop ();
    }


    TableTopSegmentation segmenter;
    TableTopCalibration calibrator;
    std::string device_id_;
    bool invert_normal_;
    boost::mutex mtx_;
    boost::mutex image_mtx_;
    PointCloudConstPtr cloud_;
    ImageConstPtr image_;
    pcl::visualization::PCLVisualizer viewer;
    pcl::visualization::ImageViewer image_viewer;
    int vp_1,vp_2,vp_3,vp_4;

    pcl::VoxelGrid<pcl::PointXYZRGBA> grid_;
  };
}

void
usage (char ** argv){
  std::cout << "usage: " << argv[0] << " <device_id> <options>\n\n"
	    << "         -invert_normal X                :: invert plane normal 0=no, yes otherwise\n"
	    << "         -o_marker_hsv_min H,S,V         :: set the HSV min values for the O marker (pcl HSV)\n"
	    << "         -o_marker_hsv_max H,S,V         :: set the HSV max values for the O marker (pcl HSV)\n"
	    << "         -x_marker_hsv_min H,S,V         :: set the HSV min values for the X marker (pcl HSV)\n"
	    << "         -x_marker_hsv_max H,S,V         :: set the HSV max values for the X marker (pcl HSV)\n";

  openni_wrapper::OpenNIDriver& driver = openni_wrapper::OpenNIDriver::getInstance ();
  if (driver.getNumberDevices () > 0){
    for (unsigned deviceIdx = 0; deviceIdx < driver.getNumberDevices (); ++deviceIdx){
      cout << "Device: " << deviceIdx + 1 << ", vendor: " << driver.getVendorName (deviceIdx) << ", product: " << driver.getProductName (deviceIdx)
	   << ", connected: " << driver.getBus (deviceIdx) << " @ " << driver.getAddress (deviceIdx) << ", serial number: \'" << driver.getSerialNumber (deviceIdx) << "\'" << endl;
      cout << "device_id may be #1, #2, ... for the first second etc device in the list or" << endl
	   << "                 bus@address for the device connected to a specific usb-bus / address combination (works only in Linux) or" << endl
	   << "                 <serial-number> (only in Linux and for devices which provide serial numbers)"  << endl;
    }
  }else
    cout << "No devices connected." << endl;
}

/**
   Command for Ualg environment:
   ./test_calibration A00362A04950051A -invert_normal 1 -o_marker_hsv_min 80,0.3,0.6 -o_marker_hsv_max 130,0.6,1.0 -x_marker_hsv_min 10,0.3,0.6 -x_marker_hsv_max 30,0.6,1.0
*/

int main (int argc, char ** argv){
  if (argc < 2){
    usage (argv);
    return 1; 
  }

  std::string arg (argv[1]);
  
  if (arg == "--help" || arg == "-h"){
    usage (argv);
    return 1;
  }

  bool invert_normal = false;
  std::vector<float> o_marker_hsv_min;
  std::vector<float> o_marker_hsv_max;
  std::vector<float> x_marker_hsv_min;
  std::vector<float> x_marker_hsv_max;

  pcl::console::parse_argument (argc, argv, "-invert_normal", invert_normal);

  if (pcl::console::parse_x_arguments (argc, argv, "-o_marker_hsv_min", o_marker_hsv_min) < 0 && o_marker_hsv_min.size() != 3){
    cout << "o_marker_hsv_min needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }

  if (pcl::console::parse_x_arguments (argc, argv, "-o_marker_hsv_max", o_marker_hsv_max) < 0 && o_marker_hsv_max.size() != 3){
    cout << "o_marker_hsv_max needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }
    
  if (pcl::console::parse_x_arguments (argc, argv, "-x_marker_hsv_min", x_marker_hsv_min) < 0 && x_marker_hsv_min.size() != 3){
    cout << "x_marker_hsv_min needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }

  if (pcl::console::parse_x_arguments (argc, argv, "-x_marker_hsv_max", x_marker_hsv_max) < 0 && x_marker_hsv_max.size() != 3){
    cout << "x_marker_hsv_max needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }

  pcl::OpenNIGrabber grabber (arg);
  if (!grabber.providesCallback<pcl::OpenNIGrabber::sig_cb_openni_point_cloud_rgba> ()){
    cout << "Only RGBA devices supported";
    usage (argv);
    return 1;
  }

  if (!grabber.providesCallback<pcl::OpenNIGrabber::sig_cb_openni_image> ()){
    cout << "Image callback not supported";
    usage (argv);
    return 1;
  }

  const XnMapOutputMode &default_mode = grabber.getDevice()->getDefaultImageMode();
  const float focal_length = grabber.getDevice()->getImageFocalLength(default_mode.nXRes);
  const float image_center_x = ((float)default_mode.nXRes)/2.0;
  const float image_center_y = ((float)default_mode.nYRes)/2.0;

  std::cout << "Default: xRes=" << default_mode.nXRes << ", yRes=" << default_mode.nYRes << ",f=" << focal_length << ",cx=" << image_center_x << ",cy=" << image_center_y << std::endl;

  ActionParsing::OpenNITableTopApp v(arg, invert_normal, focal_length, image_center_x, image_center_y, o_marker_hsv_min, o_marker_hsv_max, x_marker_hsv_min, x_marker_hsv_max);

  v.run();
  return (0);
}
