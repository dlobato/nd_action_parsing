/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tabletop_segmentation/tabletop_3dpointtracker.h>
#include <utils/munkres.h>
#include <iostream>
#include <list>

#define DEBUG 0
#define PROFILE 0
#include <pcl/myutils/debugutils.h>

namespace ActionParsing{
  typedef boost::shared_ptr<cv::KalmanFilter> KalmanFilterPtr;

  struct Filter {
    KalmanFilterPtr kf;
    int not_tracked_hits;
  };

  struct TableTop3DPointTracker::PImpl{
    std::list<Filter> filters;
  };

  TableTop3DPointTracker::TableTop3DPointTracker(const int max_tracked_points,
						 const float max_distance_observation_to_prediction,
						 const int max_not_tracked_hits)
    : impl_(new PImpl()),max_tracked_points_(max_tracked_points), 
      max_distance_observation_to_prediction_(max_distance_observation_to_prediction),
      max_not_tracked_hits_(max_not_tracked_hits) {
    // impl_->filters.resize(max_tracked_points_);
    // impl_->filters_not_tracked_hits.resize(max_tracked_points_);
    // for (int i; i<max_tracked_points_; i++)
    //   filter_slots_free.push_back(i);
  }
  
  TableTop3DPointTracker::~TableTop3DPointTracker() {}

  void TableTop3DPointTracker::track(const std::vector<cv::Point3f> &points_to_track, std::vector<cv::Point3f> &tracked_points_position, std::vector<cv::Point3f> &tracked_points_velocity){
    FPS_CALC_BEGIN
    int n_filters = impl_->filters.size();
    std::vector< std::list<Filter>::iterator > filters_by_idx(n_filters, impl_->filters.end());
    std::vector<int> filter_assignments(n_filters, -1);//-1 means not assigned
    std::vector<int> points_to_track_assignments(points_to_track.size(), -1);
    unsigned int f_idx;
    unsigned int p_idx;

    //find optimal assigments for existing filters/predictions
    if (!impl_->filters.empty()){
      //get predictions and fill costs(L2 distance)
      munkres::Matrix<double> cost_matrix(n_filters, points_to_track.size());
      //assign filters with points to track
      f_idx=0;
      for (std::list<Filter>::iterator f_it=impl_->filters.begin(),
	     end=impl_->filters.end(); f_it!=end; ++f_it){
	cv::Mat prediction = f_it->kf->predict();
	cv::Point3f xy_prediction(prediction.at<float>(0),prediction.at<float>(1),prediction.at<float>(2));
	p_idx=0;
	for(std::vector<cv::Point3f>::const_iterator p_it=points_to_track.begin(),
	      end=points_to_track.end(); p_it!=end; ++p_it){
	  cost_matrix(f_idx,p_idx) = cv::norm(xy_prediction-(*p_it));
	  DEBUG_MESSAGE("Filter prediction=" << xy_prediction << ", point to track=" << *p_it << ", norm=" << cost_matrix(f_idx,p_idx));
	  p_idx++;
	}
	filters_by_idx[f_idx++] = f_it;
      }
      munkres::Matrix<double> assign_matrix(cost_matrix);//keep a copy
      munkres::Munkres m;
#if DEBUG    
      for ( int row = 0 ; row < n_filters ; row++ ) {
	for ( int col = 0 ; col < points_to_track.size() ; col++ ) {
	  std::cout.width(2);
	  std::cout << assign_matrix(row,col) << ",";
	}
	std::cout << std::endl;
      }
      std::cout << std::endl;
#endif
      m.solve(assign_matrix);
#if DEBUG    
      for ( int row = 0 ; row < n_filters ; row++ ) {
	for ( int col = 0 ; col < points_to_track.size() ; col++ ) {
	  std::cout.width(2);
	  std::cout << assign_matrix(row,col) << ",";
	}
	std::cout << std::endl;
      }
      std::cout << std::endl;
#endif

      //read out assignments. If distance is too big discard assignment
      for (f_idx=0; f_idx<filter_assignments.size(); ++f_idx){
	for(p_idx=0; p_idx<points_to_track_assignments.size(); ++p_idx){
	  if (assign_matrix(f_idx,p_idx) == 0 && cost_matrix(f_idx,p_idx) < max_distance_observation_to_prediction_){
	    filter_assignments[f_idx] = p_idx;//filter r is assigned point c
	    points_to_track_assignments[p_idx] = f_idx;//point c is assigned filter r
	    break;//next row
	  }
	}
      }

      //check for not assigned filters
      for (f_idx=0; f_idx<filter_assignments.size(); ++f_idx){
	assert(filters_by_idx[f_idx] != impl_->filters.end());//check we have a valid iterator
	if (filter_assignments[f_idx] == -1){
	  filters_by_idx[f_idx]->not_tracked_hits++;
	  //remove filters that reach max_not_tracked_hits_
	  if (filters_by_idx[f_idx]->not_tracked_hits >= max_not_tracked_hits_){
	    DEBUG_MESSAGE("filter #" << f_idx << " max_not_tracked_hits reached. deleted");
	    impl_->filters.erase(filters_by_idx[f_idx]);
	    filters_by_idx[f_idx] = impl_->filters.end();
	  }
	}
      }
    }

    n_filters = impl_->filters.size();
	    
    //assigned points: update filter,get corrected values; not assigned points: create filter
    for (p_idx=0; p_idx<points_to_track_assignments.size(); ++p_idx){
      cv::Point3f pos,vel;
      if (points_to_track_assignments[p_idx] == -1){//not assigned: create filter, return same position, 0 velocity
	if (n_filters <= max_tracked_points_){
	  DEBUG_MESSAGE("new filter created");
	  Filter new_f;
	  new_f.kf.reset(new cv::KalmanFilter(9, 3, 0));
	  //initial guess
	  //pos
	  new_f.kf->statePost.at<float>(0) = points_to_track[p_idx].x;
	  new_f.kf->statePost.at<float>(1) = points_to_track[p_idx].y;
	  new_f.kf->statePost.at<float>(2) = points_to_track[p_idx].z;
	  //vel
	  new_f.kf->statePost.at<float>(3) = 0;
	  new_f.kf->statePost.at<float>(4) = 0;
	  new_f.kf->statePost.at<float>(5) = 0;
	  //accel
	  new_f.kf->statePost.at<float>(6) = 0;
	  new_f.kf->statePost.at<float>(7) = 0;
	  new_f.kf->statePost.at<float>(8) = 0;
	  //motion eqs: s = s0 + v0*t + a*t^2/2 , v = v0 + a*t; time interval 1
	  new_f.kf->transitionMatrix = *(cv::Mat_<float>(9, 9) << 
					 1,0,0, 1,0,0, 0.5,0,0, 
					 0,1,0, 0,1,0, 0,0.5,0, 
					 0,0,1, 0,0,1, 0,0,0.5, 
					 0,0,0, 1,0,0, 1,0,0,
					 0,0,0, 0,1,0, 0,1,0,
					 0,0,0, 0,0,1, 0,0,1,
					 0,0,0, 0,0,0, 1,0,0,
					 0,0,0, 0,0,0, 0,1,0,
					 0,0,0, 0,0,0, 0,0,1);
	  new_f.kf->measurementMatrix = *(cv::Mat_<float>(3, 9) << 1,0,0, 1,0,0, 0.5,0,0, 0,1,0, 0,1,0, 0,0.5,0, 0,0,1, 0,0,1, 0,0,0.5);
	  cv::setIdentity(new_f.kf->processNoiseCov, cv::Scalar::all(1e-4));
	  cv::setIdentity(new_f.kf->measurementNoiseCov, cv::Scalar::all(1e-1));
	  cv::setIdentity(new_f.kf->errorCovPost, cv::Scalar::all(.1));
	  new_f.not_tracked_hits = 0;
	  impl_->filters.push_back(new_f);
	}
	pos = points_to_track[p_idx];
	vel = cv::Point3f(0.0,0.0,0.0);
      }else{
	f_idx = points_to_track_assignments[p_idx];
	DEBUG_MESSAGE("updating filter #" << f_idx);
	cv::Mat_<float> measurement(3,1);
	measurement(0) = points_to_track[filter_assignments[f_idx]].x;
	measurement(1) = points_to_track[filter_assignments[f_idx]].y;
	measurement(2) = points_to_track[filter_assignments[f_idx]].z;
	cv::Mat estimated = filters_by_idx[f_idx]->kf->correct(measurement);
	assert(filters_by_idx[f_idx] != impl_->filters.end());
	filters_by_idx[f_idx]->not_tracked_hits = 0;//reset not tracked hits
	pos = cv::Point3f(estimated.at<float>(0), estimated.at<float>(1),estimated.at<float>(2));
	vel = cv::Point3f(estimated.at<float>(3), estimated.at<float>(4),estimated.at<float>(5));
      }
      tracked_points_position[p_idx] = pos;
      tracked_points_velocity[p_idx] = vel;
    }
    FPS_CALC_END(__PRETTY_FUNCTION__);
  }
}

