/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tabletop_segmentation/tabletop_handclassifier.h>
// PCL specific includes
#include <pcl/common/time.h>
#include <pcl/common/common.h>
#include <pcl/search/kdtree.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/myutils/point_types_rgbatohsv.h>
#include <pcl/myutils/packedhsv_comparison.h>
#include <cstring>
#include <cmath>

#define DEBUG 0
#define PROFILE 0
#include <pcl/myutils/debugutils.h>

namespace ActionParsing{
  struct TableTopHandClassifier::PImpl{
    pcl::ConditionAnd<pcl::PointXYZRGBA>::Ptr hand_range_cond;
    pcl::ConditionalRemoval<pcl::PointXYZRGBA> hand_color_filter;
  };

  TableTopHandClassifier::TableTopHandClassifier(const std::vector<float> &hand_hsv_min,
						 const std::vector<float> &hand_hsv_max,
						 const unsigned int min_points)
    :impl_(new PImpl()),min_points_(min_points)
  {
    setHandHSVValues(hand_hsv_min,hand_hsv_max);
  } 

  TableTopHandClassifier::~TableTopHandClassifier() {}

  void TableTopHandClassifier::setHandHSVValues(const std::vector<float> &hand_hsv_min,
						const std::vector<float> &hand_hsv_max){

    
    assert(hand_hsv_min.size() >= 3 && "hand_hsv_min needs 3 values");
    assert(hand_hsv_max.size() >= 3 && "hand_hsv_max needs 3 values");
    this->impl_->hand_range_cond.reset(new pcl::ConditionAnd<pcl::PointXYZRGBA>);
    this->impl_->hand_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("h", pcl::ComparisonOps::GT, hand_hsv_min[0])));
    this->impl_->hand_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("h", pcl::ComparisonOps::LT, hand_hsv_max[0])));
    this->impl_->hand_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("s", pcl::ComparisonOps::GT, hand_hsv_min[1])));
    this->impl_->hand_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("s", pcl::ComparisonOps::LT, hand_hsv_max[1])));
    this->impl_->hand_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("v", pcl::ComparisonOps::GT, hand_hsv_min[2])));
    this->impl_->hand_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("v", pcl::ComparisonOps::LT, hand_hsv_max[2])));
    this->impl_->hand_color_filter.setCondition (this->impl_->hand_range_cond);
  }

  void TableTopHandClassifier::setMinPoints(const unsigned int min_points){
    min_points_ = min_points;
  }

  bool TableTopHandClassifier::classify(TableTopObjectPtr &object) const{
    FPS_CALC_BEGIN;
    PointCloudPtr hand_cloud(new PointCloud);
    bool rc;
    if ((rc=classifyHand(object->cloud, *hand_cloud))){
      object.reset(new TableTopObject(hand_cloud, object->rgb_image));//FIXME: image doesn't represent cloud
      object->label = "hand";
    }
    FPS_CALC_END(__PRETTY_FUNCTION__);
    return rc;
  }

  // bool
  // TableTopHandClassifier::classifyHand(const PointCloudConstPtr &object_cloud,
  // 				       PointCloud &hand_cloud){
  //   FPS_CALC_BEGIN;
  //   assert(object_cloud && "objects_cloud == null");
  //   if (object_cloud->empty())
  //     return false;
    
  //   //filter input cloud by color
  //   this->impl_->hand_color_filter.setInputCloud(object_cloud);
  //   this->impl_->hand_color_filter.filter(hand_cloud);
  //   FPS_CALC_END(__PRETTY_FUNCTION__);
  //   DEBUG_MESSAGE("hand cloud size=" << hand_cloud.size());
  //   return (hand_cloud.size() >= min_points_);
  // }

  // bool
  // TableTopHandClassifier::classifyHand(const PointCloudConstPtr &object_cloud,
  // 				       const pcl::PointIndices &object_indices,
  // 				       PointCloud &hand_cloud){
  //   FPS_CALC_BEGIN;
  //   assert(object_cloud && "objects_cloud == null");
  //   if (object_cloud->empty())
  //     return false;
    
  //   //filter input cloud by color
  //   this->impl_->hand_color_filter.setInputCloud(object_cloud);
  //   this->impl_->hand_color_filter.setIndices(boost::make_shared<pcl::PointIndices>(object_indices));
  //   this->impl_->hand_color_filter.filter(hand_cloud);
  //   FPS_CALC_END(__PRETTY_FUNCTION__);
  //   DEBUG_MESSAGE("hand cloud size=" << hand_cloud.size());
  //   return (hand_cloud.size() >= min_points_);
  // }

  bool
  TableTopHandClassifier::classifyHand(const PointCloudConstPtr &object_cloud,
				       PointCloud &hand_cloud) const{
    FPS_CALC_BEGIN;
    assert(object_cloud && "objects_cloud == null");
    if (object_cloud->empty())
      return false;

    pcl::ConditionalRemoval<pcl::PointXYZRGBA> hand_color_filter;
    assert(this->impl_->hand_range_cond && "hand_range_cond == null");
    hand_color_filter.setCondition (this->impl_->hand_range_cond);
    hand_color_filter.setInputCloud(object_cloud);
    hand_color_filter.filter(hand_cloud);
    FPS_CALC_END(__PRETTY_FUNCTION__);
    DEBUG_MESSAGE("hand cloud size=" << hand_cloud.size());
    return (hand_cloud.size() >= min_points_);
  }

  bool
  TableTopHandClassifier::classifyHand(const PointCloudConstPtr &object_cloud,
				       const pcl::PointIndices &object_indices,
				       PointCloud &hand_cloud) const{
    FPS_CALC_BEGIN;
    assert(object_cloud && "objects_cloud == null");
    if (object_cloud->empty())
      return false;
    
    pcl::ConditionalRemoval<pcl::PointXYZRGBA> hand_color_filter;
    assert(this->impl_->hand_range_cond && "hand_range_cond == null");
    hand_color_filter.setCondition (this->impl_->hand_range_cond);
    hand_color_filter.setInputCloud(object_cloud);
    hand_color_filter.setIndices(boost::make_shared<pcl::PointIndices>(object_indices));
    hand_color_filter.filter(hand_cloud);
    FPS_CALC_END(__PRETTY_FUNCTION__);
    DEBUG_MESSAGE("hand cloud size=" << hand_cloud.size());
    return (hand_cloud.size() >= min_points_);
  }
}
