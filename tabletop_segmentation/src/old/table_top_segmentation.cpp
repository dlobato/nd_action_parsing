#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include "point_types_rgbatohsv.h"
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <pcl/common/eigen.h>
#include <pcl/common/transforms.h>
#include <pcl/common/time.h>
#include <pcl/console/parse.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/io/openni_camera/openni_driver.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/surface/convex_hull.h>

#define FPS_CALC_BEGIN                          \
    static double duration = 0;                 \
    double start_time = pcl::getTime ();        \

#define FPS_CALC_END(_WHAT_)                    \
  {                                             \
    double end_time = pcl::getTime ();          \
    static unsigned count = 0;                  \
    if (++count == 10)                          \
    {                                           \
      std::cout << "Average framerate("<< _WHAT_ << "): " << double(count)/double(duration) << " Hz" <<  std::endl; \
      count = 0;                                                        \
      duration = 0.0;                                                   \
    }                                           \
    else                                        \
    {                                           \
      duration += end_time - start_time;        \
    }                                           \
  }

class OpenNITableTopSegmentation{
public:
  typedef pcl::PointCloud<pcl::PointXYZRGBA> RGBACloud;
  typedef pcl::PointCloud<pcl::PointXYZHSV> HSVCloud;
  typedef pcl::PointCloud<pcl::PointXYZ> Cloud;
  typedef typename RGBACloud::Ptr RGBACloudPtr;
  typedef typename RGBACloud::ConstPtr RGBACloudConstPtr;
  typedef typename HSVCloud::Ptr HSVCloudPtr;
  typedef typename HSVCloud::ConstPtr HSVCloudConstPtr;

  OpenNITableTopSegmentation(const std::string& device_id = "", double threshold = 0.01, float hmin=0.0, float hmax=360.0)
    : viewer ("PCL OpenNI Table Top Segmentation"),
      device_id_ (device_id), hmin_(hmin), hmax_(hmax){

    viewer.setBackgroundColor (0, 0, 0);
    viewer.createViewPort (0, 0, 0.5, 1, vp_1);
    viewer.createViewPort (0.5, 0, 1, 1, vp_2);
    //viewer.createViewPort (0, 0, 0.3, 1, vp_1);
    //viewer.createViewPort (0.3, 0, 0.6, 1, vp_2);
    //viewer.createViewPort (0.6, 0, 1, 1, vp_3);
    viewer.addCoordinateSystem (0.5, vp_1);

    grid_.setFilterFieldName ("z");
    grid_.setFilterLimits (0.0f, 2.0f);
    grid_.setLeafSize (0.01f, 0.01f, 0.01f);

    seg_.setOptimizeCoefficients (true);
    seg_.setModelType (pcl::SACMODEL_PLANE);
    seg_.setMethodType (pcl::SAC_RANSAC);
    seg_.setMaxIterations (1000);
    seg_.setDistanceThreshold (threshold);

    proj.setModelType (pcl::SACMODEL_PLANE);

    extract_.setNegative (false);

    chull_.setDimension (2);

    ec_.setClusterTolerance (0.02); // 2cm
    ec_.setMinClusterSize (100);
    ec_.setMaxClusterSize (25000);

    object_clustering_.setClusterTolerance (0.02); // 2cm
    object_clustering_.setMinClusterSize (50);
    object_clustering_.setMaxClusterSize (2500);
  }
  
  void 
  cloud_cb_ (const RGBACloudConstPtr& cloud){
    set (cloud);
  }

  void
  set (const RGBACloudConstPtr& cloud){
    //lock while we set our cloud;
    boost::mutex::scoped_lock lock (mtx_);
    cloud_  = cloud;
  }

  // RGBACloudPtr
  // get (){
  //   //lock while we swap our cloud and reset it.
  //   boost::mutex::scoped_lock lock (mtx_);
  //   RGBACloudPtr temp_cloud (new RGBACloud);
  //   RGBACloudPtr temp_cloud2 (new RGBACloud);
  //   Cloud::Ptr temp_cloud3(new Cloud);


  //   pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients ());
  //   pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
  //   pcl::PointIndices::Ptr s_indices(new pcl::PointIndices ());
  //   pcl::PointIndices::Ptr o_indices(new pcl::PointIndices ());
  //   std::vector<pcl::PointIndices> cluster_indices;
  //   pcl::IndicesPtr table_top_indices;

  //   grid_.setInputCloud (cloud_);
  //   grid_.filter (*temp_cloud);

  //   seg_.setInputCloud (temp_cloud);
  //   seg_.segment (*inliers, *coefficients);

  //   extract_.setInputCloud (temp_cloud);
  //   extract_.setIndices (inliers);//hs_indices);
  //   extract_.filter (*temp_cloud2);

  //   pcl::copyPointCloud(*temp_cloud2,*temp_cloud3);//XYZRGBA->XYZ

  //   pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  //   tree->setInputCloud (temp_cloud3);
    
  //   ec_.setSearchMethod (tree);
  //   ec_.setInputCloud (temp_cloud3);
  //   ec_.extract (cluster_indices);

  //   std::vector<pcl::PointIndices>::const_iterator bigger_it = cluster_indices.begin ();
  //   for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it){
  //     //std::cout << "Cluster found with " << it->indices.size() << " elements\n";
  //     if (it->indices.size()>bigger_it->indices.size())
  // 	bigger_it = it;
  //   }
  //   if (bigger_it != cluster_indices.end()){//bigger cluster
  //     //extract_.setInputCloud (temp_cloud2);
  //     //there's no way to pass bigger_it to setIndices !?
  //     table_top_indices.reset (new std::vector<int> (bigger_it->indices));
  //     //extract_.setIndices (table_top_indices);
  //     //extract_.filter (*temp_cloud);
  //   }

  //   //Segment markers
  //   HSVCloudPtr hsv_cloud(new HSVCloud);
  //   HSVCloudPtr s_cloud(new HSVCloud);
  //   pcl::PointCloudXYZRGBAtoXYZHSV(*temp_cloud2,*hsv_cloud);//XYZRGBA to XYZHSV

  //   // //HSV thresholding
  //   color_filter_.setInputCloud (hsv_cloud);
  //   color_filter_.setIndices(table_top_indices);
  //   color_filter_.setNegative (false);
  //   color_filter_.setFilterFieldName ("s");
  //   color_filter_.setFilterLimits (0.3, 1.0);//just saturated colors
  //   color_filter_.filter(*s_cloud);//S filtered

  //   // //filter origin marker: blue:H=[200,240]
  //   float hmin=200.0,hmax=240.0;
  //   color_filter_.setInputCloud (s_cloud);
  //   color_filter_.setNegative (hmin>hmax);
  //   color_filter_.setFilterFieldName ("h");
  //   color_filter_.setFilterLimits (hmin, hmax);
  //   color_filter_.filter(o_indices->indices);//H filtered

  //   // color_filter_.setIndices(o_indices);
  //   // for (int i=0; i < o_indices->indices.size(); i++)
  //   //   std::cout << "H=" << color_filter_[i].h << " S=" << color_filter_[i].s << " V=" << color_filter_[i].v << "\n";

  //   //compute origin
  //   Eigen::Vector4f origin;
  //   pcl::compute3DCentroid(*temp_cloud2, *o_indices, origin);
  //   std::cout << "Origin:" << origin << "\n";
    
  //   extract_.setInputCloud (temp_cloud2);
  //   //extract_.setIndices (table_top_indices);
  //   extract_.setIndices (o_indices);
  //   extract_.filter (*temp_cloud);
    
  //   //proj.setInputCloud (temp_cloud);
  //   //proj.setIndices (inliers);
  //   //proj.setModelCoefficients (coefficients);
  //   //proj.filter (*temp_cloud2);

  //   return (temp_cloud);
  // }

  RGBACloudPtr
  get(){
    //lock while we swap our cloud and reset it.
    boost::mutex::scoped_lock lock (mtx_);
    RGBACloudPtr downsampled_cloud (new RGBACloud);

    grid_.setInputCloud (cloud_);
    grid_.filter (*downsampled_cloud);//downsample
    return (downsampled_cloud);
  }

  RGBACloudPtr
  segmentTableTop (const RGBACloudConstPtr &input_cloud, RGBACloudPtr &table_cloud, pcl::ModelCoefficients::Ptr &table_plane_coefficients){
    FPS_CALC_BEGIN;
    RGBACloudPtr temp_cloud2 (new RGBACloud);
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices ());
    pcl::PointIndices::Ptr table_indices;
    pcl::PointIndices::Ptr objects_indices(new pcl::PointIndices ());
    std::vector<pcl::PointIndices> cluster_indices;

    seg_.setInputCloud (input_cloud);
    seg_.segment (*inliers, *table_plane_coefficients);

    extract_.setInputCloud (input_cloud);
    extract_.setIndices (inliers);
    extract_.filter (*temp_cloud2);//just main plane inliers

    pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBA>);
    tree->setInputCloud (temp_cloud2);
    
    ec_.setSearchMethod (tree);
    ec_.setInputCloud (temp_cloud2);
    ec_.extract (cluster_indices);

    //we just want to keep the biggest cluster:first one
    if (!cluster_indices.empty()){
      table_indices.reset (new pcl::PointIndices (*cluster_indices.begin()));

      extract_.setInputCloud (temp_cloud2);
      extract_.setIndices (table_indices);
      extract_.filter (*table_cloud);
    }
    FPS_CALC_END("segmentTableTop");
    return (table_cloud);//return the downsampled version with complete data.
  }

  std::vector<pcl::PointIndices>&
  segmentObjects (const RGBACloudConstPtr &input_cloud, const RGBACloudConstPtr &table_cloud,  RGBACloudPtr &objects_cloud, std::vector<pcl::PointIndices> &object_clusters ){
    FPS_CALC_BEGIN;
    RGBACloudPtr table_hull (new RGBACloud);
    pcl::PointIndices::Ptr objects_indices(new pcl::PointIndices ());

    //find points above table
    chull_.setInputCloud (table_cloud);
    chull_.reconstruct (*table_hull);

    polygon_extract_.setHeightLimits (0.03, 2.0);//FIXME: min height set as constant
    polygon_extract_.setInputPlanarHull (table_hull);
    polygon_extract_.setInputCloud (input_cloud);
    polygon_extract_.segment (*objects_indices);

    extract_.setInputCloud (input_cloud);
    extract_.setIndices (objects_indices);
    extract_.filter (*objects_cloud);

    
    pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBA>);
    tree->setInputCloud (objects_cloud);

    object_clusters.clear();
    object_clustering_.setSearchMethod (tree);
    object_clustering_.setInputCloud (objects_cloud);
    object_clustering_.extract (object_clusters);

    FPS_CALC_END("segmentObjects");
    return (object_clusters);
  }

  /**
   * \return 0 if any of the marker_clouds is empty, >0 otherwise
   */
  int segmentMarkers(const RGBACloudConstPtr &table_cloud,
		     RGBACloudPtr &blue_marker_cloud,
		     RGBACloudPtr &red_marker_cloud){
    FPS_CALC_BEGIN;
    pcl::PointIndices::Ptr s_indices(new pcl::PointIndices ());
    pcl::PointIndices::Ptr sv_indices(new pcl::PointIndices ());
    pcl::PointIndices::Ptr blue_marker_indices(new pcl::PointIndices ());
    pcl::PointIndices::Ptr red_marker_indices(new pcl::PointIndices ());
    
    HSVCloudPtr hsv_cloud(new HSVCloud);
    pcl::PointCloudXYZRGBAtoXYZHSV(*table_cloud,*hsv_cloud);//XYZRGBA to XYZHSV

    //HSV thresholding
    pcl::PassThrough<pcl::PointXYZHSV> color_filter;
    color_filter.setInputCloud (hsv_cloud);
    color_filter.setNegative (false);
    color_filter.setFilterFieldName ("s");
    color_filter.setFilterLimits (0.4, 1.0);//just saturated colors FIXME: set as parameter
    color_filter.filter(s_indices->indices);//S filtered

    color_filter.setIndices(s_indices);
    //color_filter.setNegative (false);
    color_filter.setFilterFieldName ("v");
    color_filter.setFilterLimits (150.0, 255.0);//FIXME: set as parameter
    color_filter.filter(sv_indices->indices);//V filtered

    // color_filter.setIndices(sv_indices);
    // for (int i=0; i < sv_indices->indices.size(); i++)
    //   std::cout << "H=" << color_filter[i].h << " S=" << color_filter[i].s << " V=" << color_filter[i].v << "\n";

    //filter blue marker: blue:H=[220,240]
    float hmin=220.0,hmax=240.0;//FIXME: make parameters
    //color_filter.setNegative (hmin>hmax);
    color_filter.setIndices(sv_indices);
    color_filter.setFilterFieldName ("h");
    color_filter.setFilterLimits (hmin, hmax);
    color_filter.filter(blue_marker_indices->indices);//H filtered

    // color_filter.setIndices(blue_marker_indices);
    // std::cout << "Blue marker cloud size=" << blue_marker_indices->indices.size() << ":";
    // for (int i=0; i<blue_marker_cloud->size(); i++)
    // 	  std::cout << color_filter[i] << ',';
    // std::cout << "\n";
    
    extract_.setInputCloud (table_cloud);
    extract_.setIndices (blue_marker_indices);
    extract_.filter (*blue_marker_cloud);

    //filter red marker: red:H=[340,360]
    hmin=340.0;hmax=360.0;//FIXME: make parameters
    //color_filter.setNegative (hmin>hmax);
    color_filter.setIndices(sv_indices);
    color_filter.setFilterFieldName ("h");
    color_filter.setFilterLimits (hmin, hmax);
    color_filter.filter(red_marker_indices->indices);//H filtered

    extract_.setInputCloud (table_cloud);
    extract_.setIndices (red_marker_indices);
    extract_.filter (*red_marker_cloud);
    FPS_CALC_END("segmentMarkers");
    return (blue_marker_cloud->size() != 0 and red_marker_cloud->size() != 0);
  }

  void calcMarkersCoordinates(const RGBACloud &blue_marker_cloud,
			      const RGBACloud &red_marker_cloud,
			      Eigen::Vector4f &blue_marker_position,
			      Eigen::Vector4f &red_marker_position){
    pcl::compute3DCentroid(blue_marker_cloud, blue_marker_position);
    pcl::compute3DCentroid(red_marker_cloud, red_marker_position);
  }

  Eigen::Affine3f& calcTransformationToTableCoordinates(Eigen::Vector4f &blue_marker_position,
					    Eigen::Vector4f &red_marker_position,
					    pcl::ModelCoefficients::Ptr &table_plane_coefficients,
					    Eigen::Affine3f &transformation,
					    Eigen::Vector3f &o_position,
					    Eigen::Vector3f &x_direction,
					    Eigen::Vector3f &y_direction,
					    Eigen::Vector3f &z_direction){
    o_position << blue_marker_position[0],blue_marker_position[1],blue_marker_position[2];
    x_direction  << 
      red_marker_position[0]-blue_marker_position[0],
      red_marker_position[1]-blue_marker_position[1],
      red_marker_position[2]-blue_marker_position[2];
    x_direction.normalize();
    z_direction << 
      -table_plane_coefficients->values[0],
      -table_plane_coefficients->values[1],
      -table_plane_coefficients->values[2];//plane normal inverted
    y_direction = z_direction.cross(x_direction);
    y_direction.normalize();

    pcl::getTransformationFromTwoUnitVectorsAndOrigin(y_direction,z_direction,o_position,transformation);
    return transformation;
  }

  Eigen::Affine3f& calcTransformationToTableCoordinates(Eigen::Vector4f &blue_marker_position,
					    Eigen::Vector4f &red_marker_position,
					    pcl::ModelCoefficients::Ptr &table_plane_coefficients,
					    Eigen::Affine3f &transformation){
    Eigen::Vector3f o_position,x_direction,y_direction,z_direction;
    calcTransformationToTableCoordinates(blue_marker_position,red_marker_position,
					 table_plane_coefficients,transformation,
					 o_position,x_direction,y_direction,z_direction);
    return transformation;
  }

  void
  run (){
    pcl::Grabber* interface = new pcl::OpenNIGrabber (device_id_);
    
    boost::function<void (const RGBACloudConstPtr&)> f = boost::bind (&OpenNITableTopSegmentation::cloud_cb_, this, _1);
    boost::signals2::connection c = interface->registerCallback (f);

    interface->start ();

    
    pcl::ModelCoefficients::Ptr table_plane_coefficients(new pcl::ModelCoefficients ());
    RGBACloudPtr downsampled_cloud;
    RGBACloudPtr table_cloud (new RGBACloud);
    RGBACloudPtr table_cloud_transformed (new RGBACloud);
    RGBACloudPtr objects_cloud (new RGBACloud);
    std::vector<pcl::PointIndices> object_clusters;
    RGBACloudPtr objects_cloud_transformed (new RGBACloud);
    RGBACloudPtr blue_marker_cloud(new RGBACloud);
    RGBACloudPtr red_marker_cloud(new RGBACloud);
    Eigen::Vector4f blue_marker_position,red_marker_position;
    Eigen::Vector3f o_position,x_direction,y_direction,z_direction,tmp;
    Eigen::Affine3f transformation;
    unsigned int i = 0;

    while (!viewer.wasStopped ()){
      if (cloud_){
	downsampled_cloud = get();
	if (i%20==0){
	  segmentTableTop(downsampled_cloud,table_cloud,table_plane_coefficients);
	  segmentMarkers(table_cloud,blue_marker_cloud,red_marker_cloud);
	  calcMarkersCoordinates(*blue_marker_cloud,*red_marker_cloud,blue_marker_position,red_marker_position);
	  calcTransformationToTableCoordinates(blue_marker_position,red_marker_position,
					       table_plane_coefficients,transformation,
					       o_position,x_direction,y_direction,z_direction);
	  
	  pcl::transformPointCloud(*table_cloud,*table_cloud_transformed,transformation);
	  
	  viewer.removeAllShapes(vp_1);
	  if (!viewer.updatePointCloud(table_cloud_transformed,"table"))
	    viewer.addPointCloud(table_cloud_transformed,"table",vp_1);
	  tmp = o_position+x_direction;
	  viewer.addLine(pcl::PointXYZ(o_position[0],o_position[1],o_position[2]),
			 pcl::PointXYZ(tmp[0],tmp[1],tmp[2]),
			 255.0,0.0,0.0,"arrow_x",vp_1);
	  tmp = o_position+y_direction;
	  viewer.addLine(pcl::PointXYZ(o_position[0],o_position[1],o_position[2]),
			 pcl::PointXYZ(tmp[0],tmp[1],tmp[2]),
			 0.0,255.0,0.0,"arrow_y",vp_1);
	  tmp = o_position+z_direction;
	  viewer.addLine(pcl::PointXYZ(o_position[0],o_position[1],o_position[2]),
			 pcl::PointXYZ(tmp[0],tmp[1],tmp[2]),
			 0.0,0.0,255.0,"arrow_z",vp_1);
	}
	segmentObjects(downsampled_cloud,table_cloud,objects_cloud,object_clusters);
	if (i%10 == 0) std::cout << object_clusters.size() << " objects found\n";
	
	pcl::transformPointCloud(*objects_cloud,*objects_cloud_transformed,transformation);
	
	//draw markers
	//draw spheres in marker places to verify we are reading the right coordinates
	
	// viewer.removeAllShapes(vp_2);
	// if (!viewer.updatePointCloud(blue_marker_cloud,"blue_marker"))
	//   viewer.addPointCloud(blue_marker_cloud,"blue_marker",vp_2);
	// viewer.addSphere(pcl::PointXYZ(blue_marker_position[0],
	// 			       blue_marker_position[1],
	// 			       blue_marker_position[2]),
	// 		 0.01,0.0,0.0,255.0,"sphere_blue_marker",vp_2);
	// if (!viewer.updatePointCloud(red_marker_cloud,"red_marker"))
	//   viewer.addPointCloud(red_marker_cloud,"red_marker",vp_2);
	// viewer.addSphere(pcl::PointXYZ(red_marker_position[0],
	// 			       red_marker_position[1],
	// 			       red_marker_position[2]),
	// 		 0.01,255.0,0.0,0.0,"sphere_red_marker",vp_2);
	

	if (!viewer.updatePointCloud(objects_cloud_transformed,"objects"))
	  viewer.addPointCloud(objects_cloud_transformed,"objects",vp_2);

	
	i++;
	viewer.spinOnce(100);
      }
    }
    interface->stop ();
  }

  
  pcl::visualization::PCLVisualizer viewer;
  pcl::VoxelGrid<pcl::PointXYZRGBA> grid_;
  pcl::SACSegmentation<pcl::PointXYZRGBA> seg_;
  pcl::ExtractIndices<pcl::PointXYZRGBA> extract_;
  pcl::ProjectInliers<pcl::PointXYZRGBA> proj;
  pcl::ConvexHull<pcl::PointXYZRGBA> chull_;
  pcl::EuclideanClusterExtraction<pcl::PointXYZRGBA> ec_;
  pcl::EuclideanClusterExtraction<pcl::PointXYZRGBA> object_clustering_;
  pcl::ExtractPolygonalPrismData<pcl::PointXYZRGBA> polygon_extract_;

  std::string device_id_;
  boost::mutex mtx_;
  RGBACloudConstPtr cloud_;
  float hmin_,hmax_;
  int vp_1,vp_2,vp_3;
};

void
usage (char ** argv){
  std::cout << "usage: " << argv[0] << " <device_id> <options>\n\n"
            << "where options are:\n         -thresh X        :: set the planar segmentation threshold (default: 0.5)\n"
	    << "         -hmin X        :: set the Hue min threshold (default 0.0)\n"
	    << "         -hmax X        :: set the Hue max threshold (default 360.0)\n";

  openni_wrapper::OpenNIDriver& driver = openni_wrapper::OpenNIDriver::getInstance ();
  if (driver.getNumberDevices () > 0){
    for (unsigned deviceIdx = 0; deviceIdx < driver.getNumberDevices (); ++deviceIdx){
      cout << "Device: " << deviceIdx + 1 << ", vendor: " << driver.getVendorName (deviceIdx) << ", product: " << driver.getProductName (deviceIdx)
	   << ", connected: " << driver.getBus (deviceIdx) << " @ " << driver.getAddress (deviceIdx) << ", serial number: \'" << driver.getSerialNumber (deviceIdx) << "\'" << endl;
      cout << "device_id may be #1, #2, ... for the first second etc device in the list or" << endl
	   << "                 bus@address for the device connected to a specific usb-bus / address combination (works only in Linux) or" << endl
	   << "                 <serial-number> (only in Linux and for devices which provide serial numbers)"  << endl;
    }
  }else
    cout << "No devices connected." << endl;
}

int main (int argc, char ** argv){
  if (argc < 2){
    usage (argv);
    return 1; 
  }

  std::string arg (argv[1]);
  
  if (arg == "--help" || arg == "-h"){
    usage (argv);
    return 1;
  }

  double threshold = 0.05;
  float hmin=0.0, hmax=360.0;
  pcl::console::parse_argument (argc, argv, "-thresh", threshold);
  pcl::console::parse_argument (argc, argv, "-hmin", hmin);
  pcl::console::parse_argument (argc, argv, "-hmax", hmax);

  pcl::OpenNIGrabber grabber (arg);
  if (!grabber.providesCallback<pcl::OpenNIGrabber::sig_cb_openni_point_cloud_rgba> ()){
    cout << "Only RGBA devices supported";
    usage (argv);
    return 1;
  }

  OpenNITableTopSegmentation v (arg, threshold, hmin, hmax);
  v.run ();

  return (0);
}
