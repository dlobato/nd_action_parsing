/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tabletop_segmentation/tabletop_calibration.h>
// PCL specific includes
#include <pcl/common/time.h>
#include <pcl/common/common.h>
#include <pcl/common/eigen.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/conditional_removal.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/myutils/point_types_rgbatohsv.h>
#include <pcl/myutils/packedhsv_comparison.h>

#define PROFILE 0
#define DEBUG 0
#include <pcl/myutils/debugutils.h>

namespace ActionParsing{
  struct TableTopCalibration::PImpl{
    pcl::ConditionalRemoval<pcl::PointXYZRGBA> o_marker_color_filter;
    pcl::ConditionalRemoval<pcl::PointXYZRGBA> x_marker_color_filter;
  };

  TableTopCalibration::TableTopCalibration(const float focal_length,
					   const float image_center_x,
					   const float image_center_y,
					   const std::vector<float> &o_marker_hsv_min,
					   const std::vector<float> &o_marker_hsv_max,
					   const std::vector<float> &x_marker_hsv_min,
					   const std::vector<float> &x_marker_hsv_max,
					   const int min_marker_points,
					   const float marker_filter_radius)
    : impl_(new PImpl()),
      focal_length_(focal_length),image_center_x_(image_center_x), image_center_y_(image_center_y),
      min_marker_points_(min_marker_points), marker_filter_radius_(marker_filter_radius),
      to_world_tf_(Eigen::Affine3f::Identity()){
    setMarkerHSVValues(o_marker_hsv_min,o_marker_hsv_max,x_marker_hsv_min,x_marker_hsv_max);
  }

  TableTopCalibration::~TableTopCalibration() {}

  void
  TableTopCalibration::setMarkerHSVValues(const std::vector<float> &o_marker_hsv_min,
					  const std::vector<float> &o_marker_hsv_max,
					  const std::vector<float> &x_marker_hsv_min,
					  const std::vector<float> &x_marker_hsv_max){
#if DEBUG
    std::cerr << "o_marker_hsv_min=[";
    for (std::vector<float>::const_iterator i=o_marker_hsv_min.begin();
	 i != o_marker_hsv_min.end(); i++)
      std::cerr << *i << " ";
    std::cerr << "]\n";

    std::cerr << "o_marker_hsv_max=[";
    for (std::vector<float>::const_iterator i=o_marker_hsv_max.begin();
	 i != o_marker_hsv_max.end(); i++)
      std::cerr << *i << " ";
    std::cerr << "]\n";

    std::cerr << "x_marker_hsv_min=[";
    for (std::vector<float>::const_iterator i=x_marker_hsv_min.begin();
	 i != x_marker_hsv_min.end(); i++)
      std::cerr << *i << " ";
    std::cerr << "]\n";

    std::cerr << "x_marker_hsv_max=[";
    for (std::vector<float>::const_iterator i=x_marker_hsv_max.begin();
	 i != x_marker_hsv_max.end(); i++)
      std::cerr << *i << " ";
    std::cerr << "]\n";
#endif
    assert(o_marker_hsv_min.size() >= 3 && "o_marker_hsv_min needs 3 values");
    assert(o_marker_hsv_max.size() >= 3 && "o_marker_hsv_max needs 3 values");
    assert(x_marker_hsv_min.size() >= 3 && "x_marker_hsv_min needs 3 values");
    assert(x_marker_hsv_max.size() >= 3 && "o_marker_hsv_max needs 3 values");
    //set o/x markers filter conditions
    pcl::ConditionAnd<pcl::PointXYZRGBA>::Ptr o_marker_range_cond(new pcl::ConditionAnd<pcl::PointXYZRGBA>);
    pcl::ConditionAnd<pcl::PointXYZRGBA>::Ptr x_marker_range_cond(new pcl::ConditionAnd<pcl::PointXYZRGBA>);
    o_marker_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("h", pcl::ComparisonOps::GT, o_marker_hsv_min[0])));
    o_marker_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("h", pcl::ComparisonOps::LT, o_marker_hsv_max[0])));
    o_marker_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("s", pcl::ComparisonOps::GT, o_marker_hsv_min[1])));
    o_marker_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("s", pcl::ComparisonOps::LT, o_marker_hsv_max[1])));
    o_marker_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("v", pcl::ComparisonOps::GT, o_marker_hsv_min[2])));
    o_marker_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("v", pcl::ComparisonOps::LT, o_marker_hsv_max[2])));
    this->impl_->o_marker_color_filter.setCondition (o_marker_range_cond);
    
    x_marker_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("h", pcl::ComparisonOps::GT, x_marker_hsv_min[0])));
    x_marker_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("h", pcl::ComparisonOps::LT, x_marker_hsv_max[0])));
    x_marker_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("s", pcl::ComparisonOps::GT, x_marker_hsv_min[1])));
    x_marker_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("s", pcl::ComparisonOps::LT, x_marker_hsv_max[1])));
    x_marker_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("v", pcl::ComparisonOps::GT, x_marker_hsv_min[2])));
    x_marker_range_cond->addComparison (pcl::PackedHSVComparison<pcl::PointXYZRGBA>::Ptr (new pcl::PackedHSVComparison<pcl::PointXYZRGBA>("v", pcl::ComparisonOps::LT, x_marker_hsv_max[2])));
    this->impl_->x_marker_color_filter.setCondition (x_marker_range_cond);
  }

  int
  TableTopCalibration::segmentMarkers(const PointCloudConstPtr &input_cloud,
				      PointCloud &o_marker_cloud,
				      PointCloud &x_marker_cloud){
    assert(input_cloud && "input_cloud == null");
    if (input_cloud->empty()){
      PCL_ERROR("[ActionParsing::TableTopCalibration::segmentMarkers] input_cloud empty!\n");
      return 0;
    }
    
    this->impl_->o_marker_color_filter.setInputCloud(input_cloud);
    this->impl_->o_marker_color_filter.filter(o_marker_cloud);
    this->impl_->x_marker_color_filter.setInputCloud(input_cloud);
    this->impl_->x_marker_color_filter.filter(x_marker_cloud);
#if DEBUG
    std::cerr << "O marker size=" << o_marker_cloud.size() << std::endl;
    std::cerr << "X marker size=" << x_marker_cloud.size() << std::endl;
#endif
    return (o_marker_cloud.size() != 0 and x_marker_cloud.size() != 0);
  }

  int
  TableTopCalibration::filterMarkers(const PointCloudConstPtr &o_marker_cloud,
				     const PointCloudConstPtr &x_marker_cloud,
				     PointCloud &o_marker_cloud_filtered,
				     PointCloud &x_marker_cloud_filtered){
    assert(o_marker_cloud && "o_marker_cloud == null");
    if (o_marker_cloud->empty()){
      PCL_ERROR("[ActionParsing::TableTopCalibration::filterMarkers] o_marker_cloud empty");
      return 0;
    }

    assert(x_marker_cloud && "x_marker_cloud == null");
    if (x_marker_cloud->empty()){
      PCL_ERROR("[ActionParsing::TableTopCalibration::filterMarkers] x_marker_cloud empty");
      return 0;
    }

    //filter markers
    //find clusters
    pcl::RadiusOutlierRemoval<pcl::PointXYZRGBA> marker_filter;
    marker_filter.setRadiusSearch(marker_filter_radius_);
    marker_filter.setMinNeighborsInRadius(min_marker_points_);
    marker_filter.setInputCloud(o_marker_cloud);
    marker_filter.filter(o_marker_cloud_filtered);
    marker_filter.setInputCloud(x_marker_cloud);
    marker_filter.filter(x_marker_cloud_filtered);
#if DEBUG
    std::cerr << "O marker filtered size=" << o_marker_cloud_filtered.size() << std::endl;
    std::cerr << "X marker filtered size=" << x_marker_cloud_filtered.size() << std::endl;
#endif
    return (o_marker_cloud_filtered.size() != 0 and x_marker_cloud_filtered.size() != 0);
  }

  int 
  TableTopCalibration::calibrate(const PointCloudConstPtr &input_cloud,//cloud with markers in it
				 const Eigen::Vector4f &plane_normal){
    PointCloudPtr o_marker_cloud(new PointCloud);
    PointCloudPtr x_marker_cloud(new PointCloud);
    if (!segmentMarkers(input_cloud, *o_marker_cloud, *x_marker_cloud)){//markers not found
      PCL_ERROR("[ActionParsing::TableTopCalibration::calibrate] markers not found");
      return 0;
    }

    PointCloudPtr o_marker_cloud_filtered(new PointCloud);
    PointCloudPtr x_marker_cloud_filtered(new PointCloud);
    if (!filterMarkers(o_marker_cloud, x_marker_cloud, *o_marker_cloud_filtered, *x_marker_cloud_filtered)){//markers vanished after filtering
      PCL_ERROR("[ActionParsing::TableTopCalibration::calibrate] markers vanished after filtering");
      return 0;
    }

    Eigen::Vector4f o_marker_position,x_marker_position;
    calcMarkersCoordinates(*o_marker_cloud_filtered, *x_marker_cloud_filtered, o_marker_position, x_marker_position);
    calibrate(o_marker_position, x_marker_position, plane_normal);
    return 1;
  }
    
  int 
  TableTopCalibration::calibrate(const Eigen::Vector4f &o_marker_position,//origin
				 const Eigen::Vector4f &x_marker_position,//x
				 const Eigen::Vector4f &plane_normal){
    calcTransformationToWorldCoordinates(o_marker_position, x_marker_position, plane_normal, to_world_tf_);
    to_device_tf_ = to_world_tf_.inverse();
    return 1;
  }

  void 
  TableTopCalibration::transformPointCloudToWorldCoordinates(const PointCloud &cloud_in, PointCloud &cloud_out) const{
    pcl::transformPointCloud(cloud_in, cloud_out, to_world_tf_);
  }

  void 
  TableTopCalibration::transformPointCloudToDeviceCoordinates(const PointCloud &cloud_in, PointCloud &cloud_out) const{
    pcl::transformPointCloud(cloud_in, cloud_out, to_device_tf_);
  }

  // const Eigen::Affine3f& 
  // TableTopCalibration::getToXYProjectionTransformation(const float x_scaling,
  // 						       const float y_scaling,
  // 						       const float x_translation,
  // 						       const float y_translation) const{
    
  //   return calcTransformationToXYProjection(x_scaling,y_scaling,x_translation,y_translation);
  // }

  void 
  TableTopCalibration::calcMarkersCoordinates(const PointCloud &o_marker_cloud,
					      const PointCloud &x_marker_cloud,
					      Eigen::Vector4f &o_marker_position,
					      Eigen::Vector4f &x_marker_position) const{
    pcl::compute3DCentroid(o_marker_cloud, o_marker_position);
    pcl::compute3DCentroid(x_marker_cloud, x_marker_position);
  }
  
  // Eigen::Affine3f&
  // calcTransformationToWorldCoordinates(const Eigen::Vector4f &o_marker_position,
  // 					 const Eigen::Vector4f &x_marker_position,
  // 					 const Eigen::Vector4f &plane_normal,
  // 					 Eigen::Affine3f &transformation,
  // 					 Eigen::Vector3f &o_position,
  // 					 Eigen::Vector3f &x_direction,
  // 					 Eigen::Vector3f &y_direction,
  // 					 Eigen::Vector3f &z_direction);

  void
  TableTopCalibration::calcTransformationToWorldCoordinates(const Eigen::Vector4f &o_marker_position,
							    const Eigen::Vector4f &x_marker_position,
							    const Eigen::Vector4f &plane_normal,
							    Eigen::Affine3f &to_world_tf) const{
    Eigen::Vector3f o_position,x_direction,y_direction,z_direction;
    o_position << o_marker_position[0],o_marker_position[1],o_marker_position[2];
    x_direction  << 
      x_marker_position[0]-o_marker_position[0],
      x_marker_position[1]-o_marker_position[1],
      x_marker_position[2]-o_marker_position[2];
    x_direction.normalize();
    z_direction << plane_normal[0], plane_normal[1], plane_normal[2];
    y_direction = z_direction.cross(x_direction);
    y_direction.normalize();
    pcl::getTransformationFromTwoUnitVectorsAndOrigin(y_direction,z_direction,o_position,to_world_tf);
  }

  // Eigen::Affine3f&
  // TableTopCalibration::calcTransformationToXYProjection(const float x_scaling,
  // 							const float y_scaling,
  // 							const float x_translation,
  // 							const float y_translation,
  // 							Eigen::Affine3f &to_xyprojection_tf) const{//FIXME: either change name or make more general(signs)
  //    /****************************************************
  //    *   __________________         ____ x_____________
  //    *  |                  |       |\                 |
  //    *  |                  |       | \                |
  //    *  |                  |  -->  y  z               |
  //    *  y  z               |       |                  |
  //    *  | /                |       |                  |
  //    *  |/__ x_____________|       |__________________|
  //    *
  //    * z points out the screen
  //    *****************************************************/
  //   to_xyprojection_tf = Eigen::Scaling(x_scaling,-y_scaling,(float)1.0) * Eigen::Translation3f(-x_translation,-y_translation,0.0) * to_world_tf_;
  //   return to_xyprojection_tf;
  // }
}


