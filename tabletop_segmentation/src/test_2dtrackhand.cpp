/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tabletop_segmentation/tabletop_segmentation.h>
#include <tabletop_segmentation/tabletop_handclassifier.h>
#include <tabletop_segmentation/tabletop_calibration.h>
#include <tabletop_segmentation/tabletop_xyprojection.h>
#include <tabletop_segmentation/tabletop_object.h>
#include <tabletop_segmentation/tabletop_2dpointtracker.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/console/parse.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/io/openni_camera/openni_driver.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/image_viewer.h>
#include <opencv2/opencv.hpp>
#include <sstream>
#include <cmath>


#define drawCross( img, center, color, d )				\
  cv::line( img, cv::Point( center.x - d, center.y - d ),		\
	    cv::Point( center.x + d, center.y + d ), color, 2, CV_AA, 0); \
  cv::line( img, cv::Point( center.x + d, center.y - d ),		\
	    cv::Point( center.x - d, center.y + d ), color, 2, CV_AA, 0 )

namespace ActionParsing{
  class OpenNITableTopApp {
  protected:
    typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloud;
    typedef typename PointCloud::Ptr PointCloudPtr;
    typedef typename PointCloud::ConstPtr PointCloudConstPtr;
    typedef openni_wrapper::Image Image;
    typedef typename Image::Ptr ImagePtr;
    typedef typename Image::ConstPtr ImageConstPtr;
  public:
    OpenNITableTopApp(const std::string& device_id,
		      const bool invert_normal,
		      const float focal_length,
		      const float image_center_x,
		      const float image_center_y,
		      const std::vector<float> &o_marker_hsv_min,
		      const std::vector<float> &o_marker_hsv_max,
		      const std::vector<float> &x_marker_hsv_min,
		      const std::vector<float> &x_marker_hsv_max,
		      const std::vector<float> &hand_hsv_min,
		      const std::vector<float> &hand_hsv_max,
		      const float projection_width,
		      const float projection_height )
      : segmenter(),
	handclassifier(hand_hsv_min,hand_hsv_max),
	calibrator(focal_length, image_center_x, image_center_y, o_marker_hsv_min, o_marker_hsv_max,x_marker_hsv_min, x_marker_hsv_max),
	tracker(),
	device_id_ (device_id), invert_normal_(invert_normal), projection_width_(projection_width), projection_height_(projection_height),
	viewer ("Test 2d tracker") {
      //downsampling grid
      grid_.setFilterFieldName ("z");
      grid_.setFilterLimits (0.0f, 2.0f);
      grid_.setLeafSize (0.005f, 0.005f, 0.005f);
    }
  
    void 
    cloud_cb_ (const PointCloudConstPtr& cloud){
      set (cloud);
    }

    void image_cb_(const ImagePtr& image){
      set_image(image);
    }

    void
    set (const PointCloudConstPtr& cloud){
      //lock while we set our cloud;
      boost::mutex::scoped_lock lock (mtx_);
      cloud_  = cloud;
    }

    PointCloudPtr
    get(){
      //lock while we swap our cloud and reset it.
      boost::mutex::scoped_lock lock (mtx_);
      PointCloudPtr c(new PointCloud(*cloud_));
      return c;
    }

    
    PointCloudPtr
    get_downsampled(){
      //lock while we swap our cloud and reset it.
      boost::mutex::scoped_lock lock (mtx_);
      PointCloudPtr downsampled_cloud (new PointCloud);
      
      grid_.setInputCloud (cloud_);
      grid_.filter (*downsampled_cloud);//downsample
      return (downsampled_cloud);
    }

    void set_image(const ImageConstPtr& image){
      //lock while we set our image;
      boost::mutex::scoped_lock lock (image_mtx_);
      image_ = image;
    }

    cv::Mat&
    get_image_cv(cv::Mat &image_cv){
      //lock while we set our image;
      boost::mutex::scoped_lock lock (image_mtx_);
      unsigned int width = image_->getWidth();
      unsigned int height = image_->getHeight();
      image_cv.create(height,width,CV_8UC3);
      assert(image_cv.isContinuous() && "image_cv.isContinuous()");
      image_->fillRGB(width,height,image_cv.ptr<unsigned char>());
      return image_cv;
    }

    void
    run (){
      pcl::OpenNIGrabber* interface = new pcl::OpenNIGrabber (device_id_);
      //register cloud cb
      boost::function<void (const PointCloudConstPtr&)> f1 = boost::bind (&OpenNITableTopApp::cloud_cb_, this, _1);
      boost::signals2::connection c1 = interface->registerCallback (f1);
      //register image cb
      boost::function<void (const ImagePtr&)> f2 = boost::bind (&OpenNITableTopApp::image_cb_, this, _1);
      boost::signals2::connection c2 = interface->registerCallback (f2);

      PointCloudPtr downsampled_cloud;
      PointCloudPtr table_cloud(new PointCloud);
      PointCloudPtr objects_cloud(new PointCloud);
      std::vector<pcl::PointIndices> object_clusters;
      cv::Mat image_cv;
      Eigen::Vector4f table_plane_normal;
       
      //set viewer state
      viewer.setBackgroundColor (0, 0, 0);
      viewer.createViewPort (0, 0, 0.33, 1, vp_1);
      viewer.createViewPort (0.33, 0, 0.66, 1, vp_2);
      viewer.createViewPort (0.66, 0, 1, 1, vp_3);
      viewer.addCoordinateSystem (0.5, vp_2);
      interface->start ();
      

      int tabletop_counter=0;
      bool calibrated = false;
      PointCloudPtr table_hull(new PointCloud);
      
      while (!viewer.wasStopped ()){
	if (cloud_ && image_){
	  downsampled_cloud = get_downsampled();
	  get_image_cv(image_cv);

	  //draw downsampled cloud
	  // if (!viewer.updatePointCloud(downsampled_cloud,"cloud"))
	  //   viewer.addPointCloud(downsampled_cloud,"cloud",vp_1);

	  if (tabletop_counter%10 == 0){//segment plane every 50 frames
	    segmenter.segmentTableTop(downsampled_cloud,table_cloud,table_plane_normal);
	    if (invert_normal_){
	      table_plane_normal *= -1.0;
	    }
	    segmenter.calcTabletopHull(table_cloud,table_hull);
	    //draw table cloud
	    // if (!viewer.updatePointCloud(table_cloud,"table"))
	    //   viewer.addPointCloud(table_cloud,"table",vp_2);

	    if (!calibrated){
	      if (calibrated = calibrator.calibrate(table_cloud, table_plane_normal)){
		//calc tabletop size in world coordinates
		Eigen::Vector3f table_size_worldf;
		PointCloudPtr table_cloud_worldf(new PointCloud);
		calibrator.transformPointCloudToWorldCoordinates(*table_cloud, *table_cloud_worldf);
		table_size_worldf = segmenter.calcTabletopSize(*table_cloud_worldf);
		std::cerr << "Tabletop size(m)=" << table_size_worldf << std::endl;
		xyprojection.reset(new TableTopXYProjection(projection_width_,projection_height_,table_size_worldf[0],table_size_worldf[1]));
	      }
	    }
	  }
	
	  PointCloudPtr tmp_cloud(new PointCloud);
	  std::vector<boost::shared_ptr<TableTopObject> > objects;
	  std::vector<boost::shared_ptr<TableTopObject> > hands;
	  if (segmenter.segmentAndClusterObjects(downsampled_cloud,table_hull,objects_cloud, object_clusters)->size() != 0){
	    std::cerr << "Segmented " << object_clusters.size() << " objects\n";
	    if (calibrated){
	      for (int i=0; i < object_clusters.size(); i++){
		
		if (handclassifier.classifyHand(objects_cloud, object_clusters[i],*tmp_cloud)){
		  std::cerr << "Object #" << i << " is a hand\n";
		  boost::shared_ptr<TableTopObject> o(new TableTopObject(*tmp_cloud, TableTopObject::findObjectRGBImage(*tmp_cloud, image_cv, calibrator, 5)));
		  hands.push_back(o);
		}else{
		  std::cerr << "Object #" << i << " is not a hand\n";
		  boost::shared_ptr<TableTopObject> o(new TableTopObject(*objects_cloud, object_clusters[i].indices,
									 TableTopObject::findObjectRGBImage(*objects_cloud, object_clusters[i].indices, image_cv, calibrator, 5)));
		  objects.push_back(o);
		}
	      }
	    }

	   
	    //draw objects cloud
	    // if (!viewer.updatePointCloud(objects_cloud,"objects"))
	    //   viewer.addPointCloud(objects_cloud,"objects",vp_3);
	  }
	  
	  // if (calibrated){
	  //   cv::Mat image_cv_rgb(image_rgb_height, image_rgb_width, CV_8UC3, &(image_rgb[0]));
	    
	  //image_viewer.addRGBImage(image_cv.ptr<unsigned char>(),image_cv.cols, image_cv.rows);
	  
	  std::vector<cv::Point2f> points_to_track(hands.size());
	  std::vector<cv::Point2f> tracked_points_position(hands.size());
	  std::vector<cv::Point2f> tracked_points_velocity(hands.size());
	  for (int i=0; i<hands.size(); i++){
	    Eigen::Vector3f c_devicef(hands[i]->getCentroid());
	    Eigen::Vector3f c_worldf(calibrator.transformPointToWorldCoordinates(c_devicef));
	    //std::cerr << "Object #" << i << " device coordinates=" << c_devicef << " world coordinates=" << c_worldf << std::endl;
	    //track object x,y in world coordinates
	    points_to_track[i] = cv::Point2f(c_worldf[0],c_worldf[1]);
	  }
	  tracker.track(points_to_track, tracked_points_position, tracked_points_velocity);

	  cv::Mat objects_image(projection_height_, projection_width_, CV_8UC3, cv::Scalar::all(0) );
	  //draw tracked points in worldf
	  if (xyprojection){
	    std::vector<float> approaching_activation(objects.size(), 0.0);
	    std::vector<float> away_activation(objects.size(), 0.0);
	    std::vector< Eigen::Vector3f > object_centroids(objects.size());
	    std::vector< Eigen::Vector3f > object_centroids_worldf(objects.size());

	    for (int i=0; i<objects.size(); i++){
	      object_centroids[i] = objects[i]->getCentroid();
	      object_centroids_worldf[i] = calibrator.transformPointToWorldCoordinates(object_centroids[i]);
	    }

	    //draw hands (points tracked)
	    for (int i=0; i<points_to_track.size(); i++){
	      Eigen::Vector3f p_worldf(points_to_track[i].x,points_to_track[i].y, 0.0);
	      Eigen::Vector3f p_xyprojectionf = xyprojection->getTransformationToXYProjection() * p_worldf;

	      Eigen::Vector3f tp_worldf(tracked_points_position[i].x,tracked_points_position[i].y, 0.0);
	      Eigen::Vector3f tp_xyprojectionf = xyprojection->getTransformationToXYProjection() * tp_worldf;

	      Eigen::Vector3f v_worldf(tracked_points_velocity[i].x, tracked_points_velocity[i].y, 0.0 );

	      Eigen::Vector3f tv_worldf(tracked_points_position[i].x+tracked_points_velocity[i].x,tracked_points_position[i].y+tracked_points_velocity[i].y, 0.0);
	      Eigen::Vector3f tv_xyprojectionf = xyprojection->getTransformationToXYProjection() * tv_worldf;
	      
	      std::cerr << "Hand #" << i << " world coordinates=" << tp_worldf << " xyproj coordinates=" << tp_xyprojectionf << std::endl;
	      drawCross(objects_image, cv::Point(static_cast<int>(pcl_round(tp_xyprojectionf[0])),static_cast<int>(pcl_round(tp_xyprojectionf[1]))), cv::Scalar(255,0,0), 5);
	      cv::line(objects_image,
		       cv::Point(static_cast<int>(pcl_round(tp_xyprojectionf[0])),static_cast<int>(pcl_round(tp_xyprojectionf[1]))),
		       cv::Point(static_cast<int>(pcl_round(tv_xyprojectionf[0])),static_cast<int>(pcl_round(tv_xyprojectionf[1]))), cv::Scalar(0,0,255), 2, CV_AA);

	      //calc aproching act for each object
	      for (int j=0; j<objects.size(); j++){
		Eigen::Vector3f hand_object_worldf(object_centroids_worldf[j]-tp_worldf);
		hand_object_worldf[2] = 0.0;//discard z
		std::cerr << "Object #" << j << " hand_object_worldf=" << hand_object_worldf << std::endl;
		std::cerr << "Object #" << j << " hand_v_worldf=" << v_worldf << std::endl;
		std::cerr << "Object #" << j << " hand_v_worldf.norm()=" << v_worldf.norm() << std::endl;
		const float sigma2 = 0.5*0.5;
		const float min_distance = 0.05;
		float angle = atan2((v_worldf[0]*hand_object_worldf[1]-v_worldf[1]*hand_object_worldf[0]),
				    (v_worldf[0]*hand_object_worldf[0]+v_worldf[1]*hand_object_worldf[1]));//-2pi..2pi
		approaching_activation[j] += expf(-(angle*angle)/(2*sigma2)) * 1./(hand_object_worldf.norm()+min_distance) * v_worldf.norm();
		float away_angle = M_PI-fabs(angle);
		away_activation[j] += expf(-(away_angle*away_angle)/(2*sigma2)) * 1./(hand_object_worldf.norm()+min_distance) * v_worldf.norm();
		std::cerr << "Object #" << j << " angle=" << angle << std::endl;
		std::cerr << "Object #" << j << " approaching activation for hand #" << i << "=" << approaching_activation[j] << std::endl;
	      }
	    }

	    //draw objects
	    for (int i=0; i<objects.size(); i++){
	      std::stringstream ss;
	      ss << i;
	      Eigen::Vector3f o_xyprojectionf = xyprojection->getTransformationToXYProjection() * object_centroids_worldf[i];
	      cv::circle(objects_image, cv::Point(static_cast<int>(pcl_round(o_xyprojectionf[0])),static_cast<int>(pcl_round(o_xyprojectionf[1]))),
			 static_cast<int>(approaching_activation[i]*100.0), cv::Scalar(0,0,255), 2);
	      cv::circle(objects_image, cv::Point(static_cast<int>(pcl_round(o_xyprojectionf[0])),static_cast<int>(pcl_round(o_xyprojectionf[1]))),
			 static_cast<int>(away_activation[i]*100.0), cv::Scalar(0,255,255), 2);
	      cv::putText(objects_image, ss.str(), cv::Point(static_cast<int>(pcl_round(o_xyprojectionf[0])),static_cast<int>(pcl_round(o_xyprojectionf[1]))), cv::FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0,255,0));
	      //drawCross(objects_image, cv::Point(static_cast<int>(pcl_round(o_xyprojectionf[0])),static_cast<int>(pcl_round(o_xyprojectionf[1]))), cv::Scalar(0,255,0), 5);
	    }

	  }

	  objects_viewer.addRGBImage(objects_image.ptr<unsigned char>(), objects_image.cols, objects_image.rows);

	  // viewer.spinOnce(100);
	  // image_viewer.spinOnce(100);
	  objects_viewer.spinOnce(100);
	  tabletop_counter++;
	}

      }
      interface->stop ();
    }


    TableTopSegmentation segmenter;
    TableTopHandClassifier handclassifier;
    TableTopCalibration calibrator;
    boost::shared_ptr<TableTopXYProjection> xyprojection;
    TableTop2DPointTracker tracker;
    std::string device_id_;
    bool invert_normal_;
    float projection_width_;
    float projection_height_;
    boost::mutex mtx_;
    boost::mutex image_mtx_;
    PointCloudConstPtr cloud_;
    ImageConstPtr image_;
    pcl::visualization::PCLVisualizer viewer;
    pcl::visualization::ImageViewer image_viewer;
    pcl::visualization::ImageViewer objects_viewer;
    int vp_1,vp_2,vp_3,vp_4;

    pcl::VoxelGrid<pcl::PointXYZRGBA> grid_;
  };
}

void
usage (char ** argv){
  std::cout << "usage: " << argv[0] << " <device_id> <options>\n\n"
	    << "         -invert_normal X                :: invert plane normal 0=no, yes otherwise\n"
	    << "         -projection_width X             :: projection with in pixels\n"
	    << "         -projection_height X            :: projection with in pixels\n"
	    << "         -o_marker_hsv_min H,S,V         :: set the HSV min values for the O marker (pcl HSV)\n"
	    << "         -o_marker_hsv_max H,S,V         :: set the HSV max values for the O marker (pcl HSV)\n"
	    << "         -x_marker_hsv_min H,S,V         :: set the HSV min values for the X marker (pcl HSV)\n"
	    << "         -x_marker_hsv_max H,S,V         :: set the HSV max values for the X marker (pcl HSV)\n"
	    << "         -hand_hsv_max H,S,V             :: set the HSV max values for the hand (pcl HSV)\n"
	    << "         -hand_hsv_min H,S,V             :: set the HSV max values for the hand (pcl HSV)\n";

  openni_wrapper::OpenNIDriver& driver = openni_wrapper::OpenNIDriver::getInstance ();
  if (driver.getNumberDevices () > 0){
    for (unsigned deviceIdx = 0; deviceIdx < driver.getNumberDevices (); ++deviceIdx){
      cout << "Device: " << deviceIdx + 1 << ", vendor: " << driver.getVendorName (deviceIdx) << ", product: " << driver.getProductName (deviceIdx)
	   << ", connected: " << driver.getBus (deviceIdx) << " @ " << driver.getAddress (deviceIdx) << ", serial number: \'" << driver.getSerialNumber (deviceIdx) << "\'" << endl;
      cout << "device_id may be #1, #2, ... for the first second etc device in the list or" << endl
	   << "                 bus@address for the device connected to a specific usb-bus / address combination (works only in Linux) or" << endl
	   << "                 <serial-number> (only in Linux and for devices which provide serial numbers)"  << endl;
    }
  }else
    cout << "No devices connected." << endl;
}

int main (int argc, char ** argv){
  if (argc < 2){
    usage (argv);
    return 1; 
  }

  std::string arg (argv[1]);
  
  if (arg == "--help" || arg == "-h"){
    usage (argv);
    return 1;
  }

  bool invert_normal = false;
  float projection_width = 640;
  float projection_height = 480;
  std::vector<float> o_marker_hsv_min;
  std::vector<float> o_marker_hsv_max;
  std::vector<float> x_marker_hsv_min;
  std::vector<float> x_marker_hsv_max;
  std::vector<float> hand_hsv_min;
  std::vector<float> hand_hsv_max;

  pcl::console::parse_argument (argc, argv, "-invert_normal", invert_normal);
  pcl::console::parse_argument (argc, argv, "-projection_width", projection_width);
  pcl::console::parse_argument (argc, argv, "-projection_width", projection_height);

  if (pcl::console::parse_x_arguments (argc, argv, "-o_marker_hsv_min", o_marker_hsv_min) < 0 && o_marker_hsv_min.size() != 3){
    cout << "o_marker_hsv_min needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }

  if (pcl::console::parse_x_arguments (argc, argv, "-o_marker_hsv_max", o_marker_hsv_max) < 0 && o_marker_hsv_max.size() != 3){
    cout << "o_marker_hsv_max needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }
    
  if (pcl::console::parse_x_arguments (argc, argv, "-x_marker_hsv_min", x_marker_hsv_min) < 0 && x_marker_hsv_min.size() != 3){
    cout << "x_marker_hsv_min needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }

  if (pcl::console::parse_x_arguments (argc, argv, "-x_marker_hsv_max", x_marker_hsv_max) < 0 && x_marker_hsv_max.size() != 3){
    cout << "x_marker_hsv_max needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }

  if (pcl::console::parse_x_arguments (argc, argv, "-hand_hsv_min", hand_hsv_min) < 0 && hand_hsv_min.size() != 3){
    cout << "hand_hsv_min needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }

  if (pcl::console::parse_x_arguments (argc, argv, "-hand_hsv_max", hand_hsv_max) < 0 && hand_hsv_max.size() != 3){
    cout << "hand_hsv_max needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }

  pcl::OpenNIGrabber grabber (arg);
  if (!grabber.providesCallback<pcl::OpenNIGrabber::sig_cb_openni_point_cloud_rgba> ()){
    cout << "Only RGBA devices supported";
    usage (argv);
    return 1;
  }

  if (!grabber.providesCallback<pcl::OpenNIGrabber::sig_cb_openni_image> ()){
    cout << "Image callback not supported";
    usage (argv);
    return 1;
  }

  const XnMapOutputMode &default_mode = grabber.getDevice()->getDefaultImageMode();
  const float focal_length = grabber.getDevice()->getImageFocalLength(default_mode.nXRes);
  const float image_center_x = ((float)default_mode.nXRes)/2.0;
  const float image_center_y = ((float)default_mode.nYRes)/2.0;

  std::cout << "Default: xRes=" << default_mode.nXRes << ", yRes=" << default_mode.nYRes << ",f=" << focal_length << ",cx=" << image_center_x << ",cy=" << image_center_y << std::endl;

  ActionParsing::OpenNITableTopApp v(arg, invert_normal, focal_length, image_center_x, image_center_y, o_marker_hsv_min, o_marker_hsv_max, x_marker_hsv_min, x_marker_hsv_max, hand_hsv_min,hand_hsv_max, projection_width, projection_height);

  v.run();
  return (0);
}
