/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tabletop_segmentation/tabletop_object.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/common/centroid.h>
#include <cmath>


namespace ActionParsing{
  // struct TableTopObject::PImpl{
  // };

  inline const cv::Mat findImageROI(const Eigen::Vector3f &min, const Eigen::Vector3f &max, const cv::Mat &full_image, const TableTopCalibration &calibrator, const int border_size){
    Eigen::Vector2f min_imagef,max_imagef;
    min_imagef = calibrator.transformPointToImageCoordinates(min);
    max_imagef = calibrator.transformPointToImageCoordinates(max);
    cv::Point min_imagefi(std::max(0, std::min(static_cast<int>(pcl_round(min_imagef[0])) - border_size, full_image.cols-1)),
			  std::max(0, std::min(static_cast<int>(pcl_round(min_imagef[1])) - border_size, full_image.rows-1)));//inclusive in cv::Rect
    cv::Point max_imagefi(std::max(0, std::min(static_cast<int>(pcl_round(max_imagef[0])) + border_size, full_image.cols)),
			  std::max(0, std::min(static_cast<int>(pcl_round(max_imagef[1])) + border_size, full_image.rows)));
    return full_image(cv::Rect(min_imagefi,max_imagefi));
  }

  const cv::Mat TableTopObject::findObjectRGBImage(const PointCloud &object_cloud, const cv::Mat &full_rgb_image, const TableTopCalibration &calibrator, const int border_size){
    Eigen::Vector4f min;
    Eigen::Vector4f max;
    pcl::getMinMax3D(object_cloud,min,max);
    return findImageROI(min.segment<3>(0),max.segment<3>(0),full_rgb_image, calibrator, border_size);
  }

  const cv::Mat TableTopObject::findObjectRGBImage(const PointCloud &objects_cloud, const std::vector<int> object_indices, const cv::Mat &full_rgb_image, const TableTopCalibration &calibrator, const int border_size){
    Eigen::Vector4f min;
    Eigen::Vector4f max;
    pcl::getMinMax3D(objects_cloud,object_indices,min,max);
    return findImageROI(min.segment<3>(0),max.segment<3>(0),full_rgb_image, calibrator, border_size);
  }

  // TableTopObject::TableTopObject(const PointCloud &object_cloud,
  // 				 const cv::Mat &object_rgb_image,
  // 				 const std::string frame_id,
  // 				 const unsigned long time_stamp)
  //   : cloud_(new PointCloud(object_cloud)), rgb_image_(object_rgb_image), frame_id_(frame_id), time_stamp_(time_stamp), label_id_(-1) {
  //   cv::cvtColor(rgb_image_,gray_image_,CV_RGB2GRAY);
  // }

  TableTopObject::TableTopObject(const PointCloud &object_cloud,
				 const cv::Mat &object_rgb_image)
    : cloud(new PointCloud(object_cloud)), rgb_image(object_rgb_image), header(cloud->header) {}

  TableTopObject::TableTopObject(const PointCloud &objects_cloud,
				 const std::vector<int> object_indices,
				 const cv::Mat &object_rgb_image)
    : cloud(new PointCloud(objects_cloud, object_indices)), rgb_image(object_rgb_image), header(cloud->header) {}

  TableTopObject::TableTopObject(const PointCloudConstPtr &object_cloud,
				 const cv::Mat &object_rgb_image)
    : cloud(object_cloud), rgb_image(object_rgb_image), header(cloud->header) {}

  // TableTopObject::TableTopObject(const PointCloudConstPtr &object_cloud,
  // 				 const cv::Mat &object_rgb_image,
  // 				 const std::string frame_id,
  // 				 const unsigned long time_stamp)
  //   : cloud_(object_cloud), rgb_image_(object_rgb_image), frame_id_(frame_id), time_stamp_(time_stamp), label_id_(-1) {
  //   cv::cvtColor(rgb_image_,gray_image_,CV_RGB2GRAY);
  // }

  // TableTopObject::TableTopObject(const PointCloud &objects_cloud,
  // 				 const std::vector<int> object_indices,
  // 				 const cv::Mat &object_rgb_image,
  // 				 const std::string frame_id,
  // 				 const unsigned long time_stamp)
  //   : cloud_(new PointCloud(objects_cloud,object_indices)), rgb_image_(object_rgb_image), frame_id_(frame_id), time_stamp_(time_stamp), label_id_(-1) {
  //   cv::cvtColor(rgb_image_,gray_image_,CV_RGB2GRAY);
  // }

  

  // TableTopObject::TableTopObject(const TableTopObject &o)
  //   : frame_id_(o.frame_id_), object_cloud_(o.object_cloud_), object_image_(o.object_image_) {}

  TableTopObject::~TableTopObject() {}

  Eigen::Vector3f TableTopObject::getCentroid() const{
    Eigen::Vector4f centroid;
    pcl::compute3DCentroid(*cloud, centroid);
    return Eigen::Vector3f(centroid.segment<3>(0));//get 3 first elements x,y,z
  }

  // TableTopPCLObject::TableTopPCLObject(): object_cloud_(new RGBACloud) {}
  // TableTopPCLObject::TableTopPCLObject(TableTopPCLObject &tto) : object_cloud_(new RGBACloud(*(tto.object_cloud_))) {}
  // TableTopPCLObject::TableTopPCLObject(const TableTopPCLObject &tto) : object_cloud_(new RGBACloud(*(tto.object_cloud_))) {}

  // TableTopPCLObject::TableTopPCLObject(RGBACloud &pc) : object_cloud_(new RGBACloud(pc)) {}
  // TableTopPCLObject::TableTopPCLObject(const RGBACloud &pc) : object_cloud_(new RGBACloud(pc)) {}
  // TableTopPCLObject::TableTopPCLObject(const RGBACloud &pc, const std::vector< int > &indices) : object_cloud_(new RGBACloud(pc,indices)) {}
  
  // TableTopPCLObject::TableTopPCLObject(const TableTopPCLObject &tto, const std::string new_frame_id, const Eigen::Affine3f &transformation)
  //   : object_cloud_(new RGBACloud) {
  //   if (tto.object_cloud_->header.frame_id != new_frame_id){//if cloud comes from a different frame we apply the tranformation
  //     this->object_cloud_->header.frame_id = new_frame_id;
  //     pcl::transformPointCloud(*(tto.object_cloud_),*(this->object_cloud_),transformation);
  //   }else{//copy cloud
  //     object_cloud_.reset(new RGBACloud(*(tto.object_cloud_)));//FIXME:better way to do this??
  //   }
  // }

  // TableTopPCLObject::TableTopPCLObject(const RGBACloud &pc, const std::string new_frame_id, const Eigen::Affine3f &transformation)
  //   : object_cloud_(new RGBACloud) {
  //   if (pc.header.frame_id != new_frame_id){//if cloud comes from a different frame we apply the tranformation
  //     this->object_cloud_->header.frame_id = new_frame_id;
  //     pcl::transformPointCloud(pc,*(this->object_cloud_),transformation);
  //   }else{//copy cloud
  //     object_cloud_.reset(new RGBACloud(pc));//FIXME:better way to do this??
  //   }
  // }
  
  // TableTopPCLObject::TableTopPCLObject(const RGBACloud &pc, const std::vector< int > &indices, const std::string new_frame_id, const Eigen::Affine3f &transformation)
  //   : object_cloud_(new RGBACloud) {
  //   if (pc.header.frame_id != new_frame_id){//if cloud comes from a different frame we apply the tranformation
  //     this->object_cloud_->header.frame_id = new_frame_id;
  //     pcl::transformPointCloud(pc,indices,*(this->object_cloud_),transformation);
  //   }else{//copy cloud
  //     object_cloud_.reset(new RGBACloud(pc,indices));//FIXME:better way to do this??
  //   }
  // }

  // std::string& TableTopPCLObject::getFrameId() const { return this->object_cloud_->header.frame_id; }

  // unsigned int TableTopPCLObject::getCentroid(Eigen::Vector4f &centroid) const { return pcl::compute3DCentroid(*(this->object_cloud_), centroid); }

  // //(Length|x, Width|y, Height|z, Volume)
  // unsigned int TableTopPCLObject::getDimensions(Eigen::Vector4f &dimensions) const {
  //   //Eigen::Vector4f min3d,max3d;
  //   RGBACloud::PointType min3d,max3d;
  //   float l,w,h,v;
  //   pcl::getMinMax3D(*(this->object_cloud_),min3d,max3d);
  //   l = std::abs(min3d.x-max3d.x);
  //   w = std::abs(min3d.y-max3d.y);
  //   h = std::abs(min3d.z-max3d.z);
  //   v = l*w*h;
  //   dimensions << l,w,h,v;
  //   return 0;
  // }
}
