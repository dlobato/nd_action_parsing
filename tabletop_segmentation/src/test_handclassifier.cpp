/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tabletop_segmentation/tabletop_segmentation.h>
#include <tabletop_segmentation/tabletop_handclassifier.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/console/parse.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/io/openni_camera/openni_driver.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <sstream>

namespace ActionParsing{
  class OpenNITableTopApp {
  protected:
    typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloud;
    typedef typename PointCloud::Ptr PointCloudPtr;
    typedef typename PointCloud::ConstPtr PointCloudConstPtr;
  public:
    OpenNITableTopApp(const std::string& device_id,
		      const std::vector<float> &hand_hsv_min,
		      const std::vector<float> &hand_hsv_max)
      : segmenter(), handclassifier(hand_hsv_min,hand_hsv_max),
	device_id_ (device_id), viewer ("PCL OpenNI Table Top Segmentation") {
      //downsampling grid
      grid_.setFilterFieldName ("z");
      grid_.setFilterLimits (0.0f, 2.0f);
      grid_.setLeafSize (0.01f, 0.01f, 0.01f);
    }
  
    void 
    cloud_cb_ (const PointCloudConstPtr& cloud){
      set (cloud);
    }

    void
    set (const PointCloudConstPtr& cloud){
      //lock while we set our cloud;
      boost::mutex::scoped_lock lock (mtx_);
      cloud_  = cloud;
    }

    PointCloudPtr
    get(){
      //lock while we swap our cloud and reset it.
      boost::mutex::scoped_lock lock (mtx_);
      PointCloudPtr c(new PointCloud(*cloud_));
      return c;
    }

    
    PointCloudPtr
    get_downsampled(){
      //lock while we swap our cloud and reset it.
      boost::mutex::scoped_lock lock (mtx_);
      PointCloudPtr downsampled_cloud (new PointCloud);
      
      grid_.setInputCloud (cloud_);
      grid_.filter (*downsampled_cloud);//downsample
      return (downsampled_cloud);
    }
    void
    run (){
      pcl::Grabber* interface = new pcl::OpenNIGrabber (device_id_);
      
      boost::function<void (const PointCloudConstPtr&)> f = boost::bind (&OpenNITableTopApp::cloud_cb_, this, _1);
      boost::signals2::connection c = interface->registerCallback (f);

      PointCloudPtr downsampled_cloud;
      PointCloudPtr table_cloud(new PointCloud);
      PointCloudPtr objects_cloud(new PointCloud);
      std::vector<pcl::PointIndices> object_clusters;
      Eigen::Vector4f table_plane_normal;

      //set viewer state
      viewer.setBackgroundColor (0, 0, 0);
      //viewer.createViewPort (0, 0, 0.5, 1, vp_1);
      //viewer.createViewPort (0.5, 0, 1, 1, vp_2);
      viewer.createViewPort (0, 0, 0.3, 1, vp_1);
      viewer.createViewPort (0.3, 0, 0.6, 1, vp_2);
      viewer.createViewPort (0.6, 0, 1, 1, vp_3);
      viewer.addCoordinateSystem (0.5, vp_2);

      interface->start ();
      

      int tabletop_counter=0;
      PointCloudPtr table_hull(new PointCloud);

      while (!viewer.wasStopped ()){
	if (cloud_){
	  downsampled_cloud = get_downsampled();
	  if (tabletop_counter%100 == 0){//segment plane every 100 frames
	    segmenter.segmentTableTop(downsampled_cloud,table_cloud,table_plane_normal);
	    segmenter.calcTabletopHull(table_cloud,table_hull);
	  }
	  
	  PointCloudPtr tmp_cloud(new PointCloud);
	  PointCloudPtr hands(new PointCloud);
	  PointCloudPtr objects(new PointCloud);
	  if (segmenter.segmentAndClusterObjects(downsampled_cloud,table_hull,objects_cloud, object_clusters)->size() != 0){
	    std::cerr << "Segmented " << object_clusters.size() << " objects\n";
	    for (int i=0; i < object_clusters.size(); i++){
	      if (handclassifier.classifyHand(objects_cloud, object_clusters[i],*tmp_cloud)){
		//std::cerr << "Object #" << i << " is a hand\n";
		*hands += *tmp_cloud;
	      }else{
		//std::cerr << "Object #" << i << " is not a hand\n";
		PointCloudPtr o_cloud(new PointCloud(*objects_cloud, object_clusters[i].indices));
		*objects += *o_cloud;
	      }
	    }
	  }

	  //draw downsampled cloud
	  if (!viewer.updatePointCloud(downsampled_cloud,"cloud"))
	    viewer.addPointCloud(downsampled_cloud,"cloud",vp_1);

	  //draw table cloud
	  if (!viewer.updatePointCloud(table_cloud,"table"))
	    viewer.addPointCloud(table_cloud,"table",vp_2);

	  //draw objects
	  if (!viewer.updatePointCloud(objects,"objects"))
	    viewer.addPointCloud(objects,"objects",vp_3);

	  //draw hands
	  if (!viewer.updatePointCloud(hands,"hands"))
	    viewer.addPointCloud(hands,"hands",vp_3);
	  
	  viewer.spinOnce(100);
	  tabletop_counter++;
	}
      }
      interface->stop ();
    }

  
    TableTopSegmentation segmenter;
    TableTopHandClassifier handclassifier;
    std::string device_id_;
    boost::mutex mtx_;
    PointCloudConstPtr cloud_;
    pcl::visualization::PCLVisualizer viewer;
    int vp_1,vp_2,vp_3;

    pcl::VoxelGrid<pcl::PointXYZRGBA> grid_;
  };
}

void
usage (char ** argv){
  std::cout << "usage: " << argv[0] << " <device_id> <options>\n\n"
	    << "         -hand_hsv_max H,S,V             :: set the HSV max values for the hand (pcl HSV)\n"
	    << "         -hand_hsv_min H,S,V             :: set the HSV max values for the hand (pcl HSV)\n";

  openni_wrapper::OpenNIDriver& driver = openni_wrapper::OpenNIDriver::getInstance ();
  if (driver.getNumberDevices () > 0){
    for (unsigned deviceIdx = 0; deviceIdx < driver.getNumberDevices (); ++deviceIdx){
      cout << "Device: " << deviceIdx + 1 << ", vendor: " << driver.getVendorName (deviceIdx) << ", product: " << driver.getProductName (deviceIdx)
	   << ", connected: " << driver.getBus (deviceIdx) << " @ " << driver.getAddress (deviceIdx) << ", serial number: \'" << driver.getSerialNumber (deviceIdx) << "\'" << endl;
      cout << "device_id may be #1, #2, ... for the first second etc device in the list or" << endl
	   << "                 bus@address for the device connected to a specific usb-bus / address combination (works only in Linux) or" << endl
	   << "                 <serial-number> (only in Linux and for devices which provide serial numbers)"  << endl;
    }
  }else
    cout << "No devices connected." << endl;
}

int main (int argc, char ** argv){
  if (argc < 2){
    usage (argv);
    return 1; 
  }

  std::string arg (argv[1]);
  
  if (arg == "--help" || arg == "-h"){
    usage (argv);
    return 1;
  }

  std::vector<float> hand_hsv_min;
  std::vector<float> hand_hsv_max;

  if (pcl::console::parse_x_arguments (argc, argv, "-hand_hsv_min", hand_hsv_min) < 0 && hand_hsv_min.size() != 3){
    cout << "hand_hsv_min needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }

  if (pcl::console::parse_x_arguments (argc, argv, "-hand_hsv_max", hand_hsv_max) < 0 && hand_hsv_max.size() != 3){
    cout << "hand_hsv_max needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }

  pcl::OpenNIGrabber grabber (arg);
  if (!grabber.providesCallback<pcl::OpenNIGrabber::sig_cb_openni_point_cloud_rgba> ()){
    cout << "Only RGBA devices supported";
    usage (argv);
    return 1;
  }

  ActionParsing::OpenNITableTopApp v(arg,hand_hsv_min,hand_hsv_max);

  v.run();
  return (0);
}
