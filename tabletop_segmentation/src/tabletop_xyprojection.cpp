/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tabletop_segmentation/tabletop_xyprojection.h>
#include <pcl/myutils/projections.h>
// PCL specific includes
#include <pcl/common/time.h>
#include <pcl/common/common.h>
#include <pcl/common/eigen.h>
//#include <pcl/common/transforms.h>

#define PROFILE 0
#define DEBUG 0
#include <pcl/myutils/debugutils.h>

namespace ActionParsing{
  struct TableTopXYProjection::PImpl{
  };

  //pcl1.7 much much faster that the one included in 1.6
  template <typename PointT, typename Scalar> void
  transformPointCloud (const pcl::PointCloud<PointT> &cloud_in, 
		       pcl::PointCloud<PointT> &cloud_out,
		       const Eigen::Transform<Scalar, 3, Eigen::Affine> &transform)
  {
    FPS_CALC_BEGIN;
    if (&cloud_in != &cloud_out)
      {
	// Note: could be replaced by cloud_out = cloud_in
	cloud_out.header   = cloud_in.header;
	cloud_out.is_dense = cloud_in.is_dense;
	cloud_out.width    = cloud_in.width;
	cloud_out.height   = cloud_in.height;
	cloud_out.points.reserve (cloud_out.points.size ());
	cloud_out.points.assign (cloud_in.points.begin (), cloud_in.points.end ());
	cloud_out.sensor_orientation_ = cloud_in.sensor_orientation_;
	cloud_out.sensor_origin_      = cloud_in.sensor_origin_;
      }

    if (cloud_in.is_dense)
      {
	// If the dataset is dense, simply transform it!
	for (size_t i = 0; i < cloud_out.points.size (); ++i)
	  {
	    //cloud_out.points[i].getVector3fMap () = transform * cloud_in.points[i].getVector3fMap ();
	    Eigen::Matrix<Scalar, 3, 1> pt (cloud_in[i].x, cloud_in[i].y, cloud_in[i].z);
	    cloud_out[i].x = static_cast<float> (transform (0, 0) * pt.coeffRef (0) + transform (0, 1) * pt.coeffRef (1) + transform (0, 2) * pt.coeffRef (2) + transform (0, 3));
	    cloud_out[i].y = static_cast<float> (transform (1, 0) * pt.coeffRef (0) + transform (1, 1) * pt.coeffRef (1) + transform (1, 2) * pt.coeffRef (2) + transform (1, 3));
	    cloud_out[i].z = static_cast<float> (transform (2, 0) * pt.coeffRef (0) + transform (2, 1) * pt.coeffRef (1) + transform (2, 2) * pt.coeffRef (2) + transform (2, 3));
	  }
      }
    else
      {
	// Dataset might contain NaNs and Infs, so check for them first,
	// otherwise we get errors during the multiplication (?)
	for (size_t i = 0; i < cloud_out.points.size (); ++i)
	  {
	    if (!pcl_isfinite (cloud_in.points[i].x) || 
		!pcl_isfinite (cloud_in.points[i].y) || 
		!pcl_isfinite (cloud_in.points[i].z))
	      continue;
	    //cloud_out.points[i].getVector3fMap () = transform * cloud_in.points[i].getVector3fMap ();
	    Eigen::Matrix<Scalar, 3, 1> pt (cloud_in[i].x, cloud_in[i].y, cloud_in[i].z);
	    cloud_out[i].x = static_cast<float> (transform (0, 0) * pt.coeffRef (0) + transform (0, 1) * pt.coeffRef (1) + transform (0, 2) * pt.coeffRef (2) + transform (0, 3));
	    cloud_out[i].y = static_cast<float> (transform (1, 0) * pt.coeffRef (0) + transform (1, 1) * pt.coeffRef (1) + transform (1, 2) * pt.coeffRef (2) + transform (1, 3));
	    cloud_out[i].z = static_cast<float> (transform (2, 0) * pt.coeffRef (0) + transform (2, 1) * pt.coeffRef (1) + transform (2, 2) * pt.coeffRef (2) + transform (2, 3));
	  }
      }
    FPS_CALC_END(__PRETTY_FUNCTION__);
  }

  TableTopXYProjection::TableTopXYProjection(const float projection_width, const float projection_height,
					     const float cloud_width, const float cloud_height)
    : impl_(new PImpl()),
      projection_width_(projection_width),projection_height_(projection_height),
      cloud_width_(cloud_width),cloud_height_(cloud_height)
  {
    calcTransformationToXYProjection();
  }

  TableTopXYProjection::~TableTopXYProjection() {}

  void TableTopXYProjection::transformPointCloudToXYProjection(const PointCloud &cloud_in, std::vector<float> &depth_projection_data, std::vector<unsigned char> &rgb_projection_data,
							       const Eigen::Affine3f to_world_tf,
							       const bool fill, const float depth_fill, const unsigned char rgb_fill){
    FPS_CALC_BEGIN;
    PointCloudPtr cloud_in_xyprojectionf(new PointCloud);
    depth_projection_data.resize(projection_width_*projection_height_);
    rgb_projection_data.resize(projection_width_*projection_height_*3);
    transformPointCloudToXYProjection(cloud_in,&(depth_projection_data[0]), &(rgb_projection_data[0]), to_world_tf, fill, depth_fill, rgb_fill);
    FPS_CALC_END(__PRETTY_FUNCTION__);
  }

  void TableTopXYProjection::transformPointCloudToXYProjection(const PointCloud &cloud_in, float* depth_projection_data, unsigned char* rgb_projection_data,
							       const Eigen::Affine3f to_world_tf,
							       const bool fill, const float depth_fill, const unsigned char rgb_fill){
    FPS_CALC_BEGIN;
    PointCloudPtr cloud_in_xyprojectionf(new PointCloud);
    if (fill){
      std::fill(depth_projection_data,depth_projection_data + static_cast<unsigned int>(projection_width_*projection_height_),depth_fill);
      std::fill(rgb_projection_data,rgb_projection_data + static_cast<unsigned int>(projection_width_*projection_height_*3),rgb_fill);
    }
    transformPointCloud(cloud_in,*cloud_in_xyprojectionf,to_xyprojection_tf_*to_world_tf);
    pcl::orthographicXYProjection(*cloud_in_xyprojectionf,
				  projection_width_,projection_height_,
				  depth_projection_data, rgb_projection_data);
    FPS_CALC_END(__PRETTY_FUNCTION__);
  }

  void TableTopXYProjection::calcTransformationToXYProjection(){
     /****************************************************
     *   __________________         ____ x_____________
     *  |                  |       |\                 |
     *  |                  |       | \                |
     *  |                  |  -->  y  z               |
     *  y  z               |       |                  |
     *  | /                |       |                  |
     *  |/__ x_____________|       |__________________|
     *
     * z points out the screen
     *****************************************************/
    float x_scale = projection_width_/cloud_width_;
    float y_scale = projection_height_/cloud_height_;
    this->to_xyprojection_scaling_ = Eigen::Scaling(x_scale,-y_scale,(float)1.0);
    this->to_xyprojection_tf_ = this->to_xyprojection_scaling_ * Eigen::Translation3f(0.0,-cloud_height_,0.0);
  }
}


