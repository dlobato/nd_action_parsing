/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/myutils/async_grabber.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/image_viewer.h>
#include <pcl/io/openni_camera/openni_driver.h>
#include <iostream>

typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloud;
typedef typename PointCloud::Ptr PointCloudPtr;
typedef typename PointCloud::ConstPtr PointCloudConstPtr;

void
usage (char ** argv){
  std::cout << "usage: " << argv[0] << " <device_id> <options>\n\n";

  openni_wrapper::OpenNIDriver& driver = openni_wrapper::OpenNIDriver::getInstance ();
  if (driver.getNumberDevices () > 0){
    for (unsigned deviceIdx = 0; deviceIdx < driver.getNumberDevices (); ++deviceIdx){
      cout << "Device: " << deviceIdx + 1 << ", vendor: " << driver.getVendorName (deviceIdx) << ", product: " << driver.getProductName (deviceIdx)
	   << ", connected: " << driver.getBus (deviceIdx) << " @ " << driver.getAddress (deviceIdx) << ", serial number: \'" << driver.getSerialNumber (deviceIdx) << "\'" << endl;
      cout << "device_id may be #1, #2, ... for the first second etc device in the list or" << endl
	   << "                 bus@address for the device connected to a specific usb-bus / address combination (works only in Linux) or" << endl
	   << "                 <serial-number> (only in Linux and for devices which provide serial numbers)"  << endl;
    }
  }else
    cout << "No devices connected." << endl;
}

int main(int argc, char** argv){
  if (argc < 2){
    usage (argv);
    return 1; 
  }

  std::string arg (argv[1]);
  
  if (arg == "--help" || arg == "-h"){
    usage (argv);
    return 1;
  }

  pcl::AsyncGrabber agrabber(arg);
  agrabber.start();

  pcl::VoxelGrid<pcl::PointXYZRGBA> downsampling_grid;
  downsampling_grid.setFilterFieldName ("z");
  downsampling_grid.setFilterLimits (0.0f, 2.0f);
  downsampling_grid.setLeafSize (0.01f, 0.01f, 0.01f);

  pcl::visualization::PCLVisualizer cloud_viewer("PCL viewer");
  cloud_viewer.setBackgroundColor (0, 0, 0);
  pcl::visualization::ImageViewer image_viewer("Image viewer");
  
  PointCloudPtr downsampled_cloud (new PointCloud);
  std::vector<unsigned char> input_image_data;
  unsigned int i = 0;
  while(!cloud_viewer.wasStopped ()){
    PointCloudConstPtr input_cloud(agrabber.getLatestCloud());
    if (input_cloud){
      downsampling_grid.setInputCloud (input_cloud);
      downsampling_grid.filter(*downsampled_cloud);
      if (!cloud_viewer.updatePointCloud(downsampled_cloud,"cloud"))
	cloud_viewer.addPointCloud(downsampled_cloud,"cloud");
      cloud_viewer.spinOnce(100);
    }
    boost::shared_ptr<openni_wrapper::Image> input_image(agrabber.getLatestImage());
    if (input_image){
      input_image_data.resize(input_image->getWidth()*input_image->getHeight()*3);
      input_image->fillRGB(input_image->getWidth(),input_image->getHeight(),&(input_image_data[0]));
      
      image_viewer.addRGBImage(&(input_image_data[0]), input_image->getWidth(),input_image->getHeight());
      image_viewer.spinOnce(100);
    }
    if (++i%10){
      std::cout << "Cloud fps=" << agrabber.getCloudFps() << std::endl;
      std::cout << "Image fps=" << agrabber.getImageFps() << std::endl;
    }
  }
  agrabber.stop();
  std::cout << "Grabber run state=" << agrabber.isRunning() << std::endl;

}
