/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pcl/impl/instantiate.hpp>
#include <pcl/point_types.h>
#include <pcl/myutils/projections.h>
#include <pcl/myutils/impl/projections.hpp>

//instanciate for pcl::PointXYZRGBA and pcl::PointXYZRGB
PCL_INSTANTIATE(orthographicXYProjection1, PCL_XYZ_POINT_TYPES)
PCL_INSTANTIATE(orthographicXYProjection2, PCL_XYZ_POINT_TYPES)
PCL_INSTANTIATE(orthographicXYProjection3, PCL_XYZ_POINT_TYPES)
PCL_INSTANTIATE(orthographicXYProjection4, PCL_XYZ_POINT_TYPES)
PCL_INSTANTIATE(orthographicXYProjection5, PCL_XYZ_POINT_TYPES)
PCL_INSTANTIATE(orthographicXYProjection6, PCL_XYZ_POINT_TYPES)
