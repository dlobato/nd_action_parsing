/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tabletop_segmentation/tabletop_segmentation.h>
#include <tabletop_segmentation/tabletop_calibration.h>
#include <tabletop_segmentation/tabletop_xyprojection.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/console/parse.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/io/openni_camera/openni_driver.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/image_viewer.h>
#include <opencv2/opencv.hpp>
#include <sstream>

namespace ActionParsing{
  class OpenNITableTopApp {
  protected:
    typedef pcl::PointCloud<pcl::PointXYZRGBA> PointCloud;
    typedef typename PointCloud::Ptr PointCloudPtr;
    typedef typename PointCloud::ConstPtr PointCloudConstPtr;
  public:
    OpenNITableTopApp(const std::string& device_id,
		      const bool invert_normal,
		      const float focal_length,
		      const float image_center_x,
		      const float image_center_y,
		      const std::vector<float> &o_marker_hsv_min,
		      const std::vector<float> &o_marker_hsv_max,
		      const std::vector<float> &x_marker_hsv_min,
		      const std::vector<float> &x_marker_hsv_max,
		      const float projection_width,
		      const float projection_height)
      : segmenter(),
	calibrator(focal_length, image_center_x, image_center_y, o_marker_hsv_min, o_marker_hsv_max,x_marker_hsv_min, x_marker_hsv_max),
	device_id_ (device_id), invert_normal_(invert_normal), projection_width_(projection_width), projection_height_(projection_height),
	viewer ("Test xyprojection") {
      //downsampling grid
      grid_.setFilterFieldName ("z");
      grid_.setFilterLimits (0.0f, 2.0f);
      grid_.setLeafSize (0.005f, 0.005f, 0.005f);
    }
  
    void 
    cloud_cb_ (const PointCloudConstPtr& cloud){
      set (cloud);
    }

    void
    set (const PointCloudConstPtr& cloud){
      //lock while we set our cloud;
      boost::mutex::scoped_lock lock (mtx_);
      cloud_  = cloud;
    }

    PointCloudPtr
    get(){
      //lock while we swap our cloud and reset it.
      boost::mutex::scoped_lock lock (mtx_);
      PointCloudPtr c(new PointCloud(*cloud_));
      return c;
    }

    
    PointCloudPtr
    get_downsampled(){
      //lock while we swap our cloud and reset it.
      boost::mutex::scoped_lock lock (mtx_);
      PointCloudPtr downsampled_cloud (new PointCloud);
      
      grid_.setInputCloud (cloud_);
      grid_.filter (*downsampled_cloud);//downsample
      return (downsampled_cloud);
    }
    void
    run (){
      pcl::Grabber* interface = new pcl::OpenNIGrabber (device_id_);
      
      boost::function<void (const PointCloudConstPtr&)> f = boost::bind (&OpenNITableTopApp::cloud_cb_, this, _1);
      boost::signals2::connection c = interface->registerCallback (f);

      PointCloudPtr downsampled_cloud;
      PointCloudPtr table_cloud(new PointCloud);
      PointCloudPtr objects_cloud(new PointCloud);
      Eigen::Vector4f table_plane_normal;
      Eigen::Vector3f table_size_worldf;
      Eigen::Vector3f table_min_worldf,table_max_worldf;
      cv::Mat objects_depth(projection_height_,projection_width_,CV_32F);
      cv::Mat objects_color(projection_height_,projection_width_,CV_8UC3);
      //std::vector<unsigned char> objects_color_data(projection_height_*projection_width_*3);//RGB
      //std::vector<float> objects_depth_data(projection_height_*projection_width_);
      
      //set viewer state
      viewer.setBackgroundColor (0, 0, 0);
      viewer.createViewPort (0, 0, 0.33, 1, vp_1);
      viewer.createViewPort (0.33, 0, 0.66, 1, vp_2);
      viewer.createViewPort (0.66, 0, 1, 1, vp_3);
      viewer.addCoordinateSystem (0.5, vp_4);
      interface->start ();
      

      int tabletop_counter=0;
      bool calibrated = false;
      PointCloudPtr table_hull(new PointCloud);
      std::vector<Eigen::Vector3f> table_corners(4);//in device coordinates
      std::vector<Eigen::Vector3f> table_corners_worldf(4);//in world coordinates

      while (!viewer.wasStopped ()){
	if (cloud_){
	  downsampled_cloud = get_downsampled();
	  //draw downsampled cloud
	  if (!viewer.updatePointCloud(downsampled_cloud,"cloud"))
	    viewer.addPointCloud(downsampled_cloud,"cloud",vp_1);

	  if (tabletop_counter%10 == 0){//segment plane every 50 frames
	    segmenter.segmentTableTop(downsampled_cloud,table_cloud,table_plane_normal);
	    if (invert_normal_){
	      table_plane_normal *= -1.0;
	    }
	    segmenter.calcTabletopHull(table_cloud,table_hull);
	    //draw table cloud
	    if (!viewer.updatePointCloud(table_cloud,"table"))
	      viewer.addPointCloud(table_cloud,"table",vp_2);

	    
	    if (!calibrated){
	      if ((calibrated = calibrator.calibrate(table_cloud, table_plane_normal))){
		//calc tabletop size in world coordinates
		PointCloudPtr table_cloud_worldf(new PointCloud);
		calibrator.transformPointCloudToWorldCoordinates(*table_cloud, *table_cloud_worldf);
		table_size_worldf = segmenter.calcTabletopSize(*table_cloud_worldf);
		segmenter.calcTabletopMinMax3D(*table_cloud_worldf, table_min_worldf, table_max_worldf);
		  
		std::cerr << "Tabletop size(m)=" << table_size_worldf << std::endl;
		std::cerr << "Tabletop min=" << table_min_worldf << std::endl;
		std::cerr << "Tabletop max=" << table_max_worldf << std::endl;
		xyprojection.reset(new TableTopXYProjection(projection_width_,projection_height_,table_size_worldf[0],table_size_worldf[1]));
		
		viewer.removeAllShapes(vp_2);
		//draw world coordinates axes
		const Eigen::Affine3f &to_device_tf = calibrator.getTransformationToDeviceCoordinates();
		Eigen::Vector3f o_position,x_direction,y_direction,z_direction;
		o_position = to_device_tf * Eigen::Vector3f::Zero();
		x_direction = to_device_tf * (Eigen::Vector3f::UnitX() * 0.5);
		y_direction = to_device_tf * (Eigen::Vector3f::UnitY() * 0.5);
		z_direction = to_device_tf * (Eigen::Vector3f::UnitZ() * 0.5);
		viewer.addLine(pcl::PointXYZ(o_position[0],o_position[1],o_position[2]),
			       pcl::PointXYZ(x_direction[0],x_direction[1],x_direction[2]),
			       255.0,0.0,0.0,"tabletop_tf_arrow_x",vp_2);
		viewer.addLine(pcl::PointXYZ(o_position[0],o_position[1],o_position[2]),
			       pcl::PointXYZ(y_direction[0],y_direction[1],y_direction[2]),
			       0.0,255.0,0.0,"tabletop_tf_arrow_y",vp_2);
		viewer.addLine(pcl::PointXYZ(o_position[0],o_position[1],o_position[2]),
			       pcl::PointXYZ(z_direction[0],z_direction[1],z_direction[2]),
			       0.0,0.0,255.0,"tabletop_tf_arrow_z",vp_2);
		
		//calc table corners
		table_corners_worldf[0] = table_min_worldf;
		table_corners_worldf[1] = Eigen::Vector3f(table_min_worldf[0],table_max_worldf[1],0.0);//B:x=min.x,y=max.y
		table_corners_worldf[2] = Eigen::Vector3f(table_max_worldf[0],table_max_worldf[1],0.0);//C:x=max.x,y=max.y
		table_corners_worldf[3] = Eigen::Vector3f(table_max_worldf[0],table_min_worldf[1],0.0);//D:x=max.x,y=min.y
		table_corners[0] = to_device_tf * table_corners_worldf[0];
		table_corners[1] = to_device_tf * table_corners_worldf[1];
		table_corners[2] = to_device_tf * table_corners_worldf[2];
		table_corners[3] = to_device_tf * table_corners_worldf[3];

		//draw table area
		for (int i=0; i<table_corners.size(); i++){
		  int a=i;
		  int b=(i+1)%table_corners.size();
		  std::stringstream ss;
		  ss << "table_area_segment_" << i;
		  viewer.addLine(pcl::PointXYZ(table_corners[a][0],table_corners[a][1],table_corners[a][2]),
				 pcl::PointXYZ(table_corners[b][0],table_corners[b][1],table_corners[b][2]),
				 255.0,255.0,0.0,ss.str(),vp_2);
		}
	      }
	    }  
	  }
	
	  
	  if (segmenter.segmentObjects(downsampled_cloud,table_hull,objects_cloud)->size() != 0){
	    PointCloudPtr objects_cloud_worldf(new PointCloud);
	    calibrator.transformPointCloudToWorldCoordinates(*objects_cloud,*objects_cloud_worldf);
	    
	    //draw objects cloud
	    if (!viewer.updatePointCloud(objects_cloud_worldf,"objects"))
	      viewer.addPointCloud(objects_cloud_worldf,"objects",vp_3);

	    if (xyprojection){
	      xyprojection->transformPointCloudToXYProjection(*objects_cloud, objects_depth.ptr<float>(), objects_color.ptr<unsigned char>(),
							      calibrator.getTransformationToWorldCoordinates());
	      objects_color_viewer.addRGBImage(objects_color.ptr<unsigned char>(),projection_width_,projection_height_);
	      objects_depth_viewer.addFloatImage(objects_depth.ptr<float>(),projection_width_,projection_height_,0.0,1.0);
	    }
	  }
	
	    
	  viewer.spinOnce(100);
	  objects_color_viewer.spinOnce(100);
	  objects_depth_viewer.spinOnce(100);
	  tabletop_counter++;
	}
      }
      interface->stop ();
    }


    TableTopSegmentation segmenter;
    TableTopCalibration calibrator;
    boost::shared_ptr<TableTopXYProjection> xyprojection;
    std::string device_id_;
    bool invert_normal_;
    float projection_width_;
    float projection_height_;
    boost::mutex mtx_;
    PointCloudConstPtr cloud_;
    pcl::visualization::PCLVisualizer viewer;
    pcl::visualization::ImageViewer objects_color_viewer;
    pcl::visualization::ImageViewer objects_depth_viewer;
    int vp_1,vp_2,vp_3,vp_4;

    pcl::VoxelGrid<pcl::PointXYZRGBA> grid_;
  };
}

void
usage (char ** argv){
  std::cout << "usage: " << argv[0] << " <device_id> <options>\n\n"
	    << "         -invert_normal X                :: invert plane normal 0=no, yes otherwise\n"
	    << "         -projection_width X             :: projection with in pixels\n"
	    << "         -projection_height X            :: projection with in pixels\n"
	    << "         -o_marker_hsv_min H,S,V         :: set the HSV min values for the O marker (pcl HSV)\n"
	    << "         -o_marker_hsv_max H,S,V         :: set the HSV max values for the O marker (pcl HSV)\n"
	    << "         -x_marker_hsv_min H,S,V         :: set the HSV min values for the X marker (pcl HSV)\n"
	    << "         -x_marker_hsv_max H,S,V         :: set the HSV max values for the X marker (pcl HSV)\n";

  openni_wrapper::OpenNIDriver& driver = openni_wrapper::OpenNIDriver::getInstance ();
  if (driver.getNumberDevices () > 0){
    for (unsigned deviceIdx = 0; deviceIdx < driver.getNumberDevices (); ++deviceIdx){
      cout << "Device: " << deviceIdx + 1 << ", vendor: " << driver.getVendorName (deviceIdx) << ", product: " << driver.getProductName (deviceIdx)
	   << ", connected: " << driver.getBus (deviceIdx) << " @ " << driver.getAddress (deviceIdx) << ", serial number: \'" << driver.getSerialNumber (deviceIdx) << "\'" << endl;
      cout << "device_id may be #1, #2, ... for the first second etc device in the list or" << endl
	   << "                 bus@address for the device connected to a specific usb-bus / address combination (works only in Linux) or" << endl
	   << "                 <serial-number> (only in Linux and for devices which provide serial numbers)"  << endl;
    }
  }else
    cout << "No devices connected." << endl;
}

/**
   Command for Ualg environment:
   ./test_xyprojection A00362A04950051A -invert_normal 1 -o_marker_hsv_min 80,0.3,0.6 -o_marker_hsv_max 130,0.6,1.0 -x_marker_hsv_min 10,0.3,0.6 -x_marker_hsv_max 30,0.6,1.0
*/

int main (int argc, char ** argv){
  if (argc < 2){
    usage (argv);
    return 1; 
  }

  std::string arg (argv[1]);
  
  if (arg == "--help" || arg == "-h"){
    usage (argv);
    return 1;
  }

  bool invert_normal = false;
  float projection_width = 320;
  float projection_height = 240;
  std::vector<float> o_marker_hsv_min;
  std::vector<float> o_marker_hsv_max;
  std::vector<float> x_marker_hsv_min;
  std::vector<float> x_marker_hsv_max;

  pcl::console::parse_argument (argc, argv, "-invert_normal", invert_normal);
  pcl::console::parse_argument (argc, argv, "-projection_width", projection_width);
  pcl::console::parse_argument (argc, argv, "-projection_height", projection_height);

  if (pcl::console::parse_x_arguments (argc, argv, "-o_marker_hsv_min", o_marker_hsv_min) < 0 && o_marker_hsv_min.size() != 3){
    cout << "o_marker_hsv_min needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }

  if (pcl::console::parse_x_arguments (argc, argv, "-o_marker_hsv_max", o_marker_hsv_max) < 0 && o_marker_hsv_max.size() != 3){
    cout << "o_marker_hsv_max needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }
    
  if (pcl::console::parse_x_arguments (argc, argv, "-x_marker_hsv_min", x_marker_hsv_min) < 0 && x_marker_hsv_min.size() != 3){
    cout << "x_marker_hsv_min needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }

  if (pcl::console::parse_x_arguments (argc, argv, "-x_marker_hsv_max", x_marker_hsv_max) < 0 && x_marker_hsv_max.size() != 3){
    cout << "x_marker_hsv_max needs 3 values: H,S,V";
    usage (argv);
    return 1;
  }

  pcl::OpenNIGrabber grabber (arg);
  if (!grabber.providesCallback<pcl::OpenNIGrabber::sig_cb_openni_point_cloud_rgba> ()){
    cout << "Only RGBA devices supported";
    usage (argv);
    return 1;
  }

  if (!grabber.providesCallback<pcl::OpenNIGrabber::sig_cb_openni_image> ()){
    cout << "Image callback not supported";
    usage (argv);
    return 1;
  }

  const XnMapOutputMode &default_mode = grabber.getDevice()->getDefaultImageMode();
  const float focal_length = grabber.getDevice()->getImageFocalLength(default_mode.nXRes);
  const float image_center_x = ((float)default_mode.nXRes)/2.0;
  const float image_center_y = ((float)default_mode.nYRes)/2.0;

  std::cout << "Default: xRes=" << default_mode.nXRes << ", yRes=" << default_mode.nYRes << ",f=" << focal_length << ",cx=" << image_center_x << ",cy=" << image_center_y << std::endl;

  ActionParsing::OpenNITableTopApp v(arg, invert_normal, focal_length, image_center_x, image_center_y,  o_marker_hsv_min, o_marker_hsv_max, x_marker_hsv_min, x_marker_hsv_max, projection_width, projection_height);

  v.run();
  return (0);
}
