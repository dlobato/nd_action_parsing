/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tabletop_segmentation/hsvutil.h>
#include <cassert>


std::vector<float>
ActionParsing::hsvFromOpenCV(const std::vector<unsigned char> &HSV_in){
  assert(HSV_in.size() >= 3);
  std::vector<float> HSV_out(3);
  HSV_out[0] = static_cast<float>(HSV_in[0])*2.0;
  HSV_out[1] = static_cast<float>(HSV_in[1])/255.0;
  HSV_out[2] = static_cast<float>(HSV_in[2])/255.0;
  return HSV_out;
}

std::vector<unsigned char>
ActionParsing::hsvToOpenCV(const std::vector<float> &HSV_in){
  assert(HSV_in.size() >= 3);
  std::vector<unsigned char> HSV_out(3);
  HSV_out[0] = static_cast<unsigned char>(std::min(0.,std::max(HSV_in[0]*0.5,255.0)));
  HSV_out[1] = static_cast<unsigned char>(std::min(0.,std::max(HSV_in[1]*255.0,255.0)));
  HSV_out[2] = static_cast<unsigned char>(std::min(0.,std::max(HSV_in[2]*255.0,255.0)));
  return HSV_out;
}
  
