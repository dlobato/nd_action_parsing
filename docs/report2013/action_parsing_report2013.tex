% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...

\usepackage{hyperref} % \url{}

\usepackage{algorithm}% http://ctan.org/pkg/algorithm
\usepackage{algpseudocode}% http://ctan.org/pkg/algorithmicx
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}

\usepackage{amsmath}
\usepackage{amssymb}

%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%%% END Article customizations

%%% The "real" document content comes below...

\title{Action Parsing System}
\author{David Lobato}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle

\section{Introduction}
The serial order mechanism enables action sequences with a priori arbitrary order that is learned from instruction. \cite{Sandamirskaya2010,Sandamirskaya2010a} introduce a neural dynamic architecture which implements the serial order mechanism by chaining dynamical nodes. These nodes represent ordinal positions within the sequence and they may be coupled to different dynamic neural fields encoding the parameters of the behaviors in the sequence. In the original implementation, the system could acquire order of simple actions, which may be characterized by a single perceptual or motor parameter, represented in the intention DNF. The model allows the representation of sequences of multimodal actions, which are characterized by different parameters, although the problem of acquiring said actions, which temporal segmentation is challenging, is not solved in the original work. Examples of such actions are search for a target object, approaching the target object, grasping the object, lifting, and transporting the object.

This project aims to tackle the problem of acquiring sequences of multimodal actions from demonstrated actions based on visual perception of a table-top scene, in which objects are manipulated by an agent (teacher). Such approach is often referred in the published literature with a variety of terms such as Learning from Demonstration (LfD), Learning by Demonstration (LbD), Programming by Demonstration (PbD), Learning by Experienced Demonstrations, Assembly Plan from Observation, ... \cite{Argall2009} provides a recent survey on the different design choices and algorithms. Learning from Demonstrations is a topic getting a lot of attention lately both as a means to facilitate autonomy of robots and to understand cognitive processes of human learning. 
 
This document describes a first implementation of a system able to parse a dynamic scene by means of detecting user intentions, which provides cues to classify the observed actions, as well as the starting and ending points of those actions. Figure \ref{fig:complete_system} shows a snapshot of the implemented system with the visual scene (top left), the selected object receiving the detected action (bottom left) and the time course of activation of the detectors.

\begin{figure}[htp]
\centering
\includegraphics[width=0.8\textwidth]{figs/complete_system.png}
\caption{System snapshot}
\label{fig:complete_system}
\end{figure}


\section{System Overview}
To develop such a system, we had to address two major challenges: (1) The correspondence problem arises when the observing agent has a different body than the teacher. In order to overcome this problem, we represent actions through their goals rather than trajectories. Thus, the correspondence problem is replaced by the problem of figuring out how to achieve a set of goals, rather than replay specific movements with actuators that very likely will be different from the teacher's ones. The serial order architecture and the behavior organization system, developed within the project, solve this problem by specifying how goals could be achieved through the proper connections between the intention and the condition of satisfaction fields and the sensorimotor systems of the robot. (2) Since our agent does not directly record the teacher's action/states but observes them through its sensors, the teacher's intentions (goals) must be inferred. Specifically, in our scenario, the robot observes the scene through the vision sensors capable of extracting color and depth information, builds a scene representation of the table top scenario, and accesses features of the scene, characteristic for particular observed intentions.

The setup used for implementing and testing our system is shown in \ref{fig:calibration_frames}. A Microsoft Kinect device is used as the main sensor. The objects used for experiments come in different sizes, shapes, and colors. In this first implementation we use plain colors to distinguish  objects and the tabletop area, though more complex object representations with DNFs may be employed later in the project.

The system is split in three main subsystems. A first subsystem takes the sensory input coming from the Kinect camera and process it to extract meaningful information of the scene such as objects, their positions, their features (color, shape,...) and their motion information. This subsystem will be referred as sensory acquisition subsystem. A second subsystem that takes the preprocessed sensory input and tries to analyze it's temporal order to figure out the agent intentions. The output of this subsystem is a sequence of events that signal possible actions being performed by an agent. This subsystem will be referred as action parsing subsystem. A third subsystem takes the sequence of event produced by the action parsing subsystem and stores it as a serial ordered sequence of events as shown in \cite{Sandamirskaya2010a}. We will refer to this subsystem as event memory subsystem. The rest of this section covers these three subsystems in detail.

\begin{figure}[htp]
\centering
\includegraphics[width=0.8\textwidth]{figs/calibration_frames.png}
\caption{Reference frames}
\label{fig:calibration_frames}
\end{figure}

\subsection{Sensory acquisition subsystem}
The sensory acquisition subsystem is basically a pipeline that transform the raw data captured by the Kinect camera into something easier to process by later steps. Several steps are performed in order to go from raw images to objects, object features, motion detection, ... These steps are shown in figure \ref{fig:sensory_acquisition_pipeline}.

\begin{figure}[htp]
\centering
\includegraphics[width=0.8\textwidth]{figs/sensory_acquisition_pipeline.png}
\caption{Sensory acquisition pipeline}
\label{fig:sensory_acquisition_pipeline}
\end{figure}

The sensory acquisition subsystem starts grabbing raw data from the Kinect camera. This is represented as the Grabber box in figure \ref{fig:sensory_acquisition_pipeline}. The Kinect camera has a RGB camera and a depth sensor. Each frame captured by the Kinect camera is composed by a RGB image plus a depth image (often called RGBD image). From this images we can reconstruct the 3D scene in front of the sensor. An open source library called PCL \cite{Rusu2011} makes this straightforward, taking the Kinect raw data and producing a point cloud data structure, where each point represents a pixel plus its 3D position in the sensor coordinate system ($O_d$ in fig. \ref{fig:calibration_frames}). In addition, the PCL library provide a set of useful functions to analyze the point cloud, such as line and plane segmentation algorithms or clustering algorithms.

Next step in the pipeline is segmentation of the tabletop plane and the objects above it. This is represented as the Grabber box in figure \ref{fig:sensory_acquisition_pipeline}. Segmentation of the tabletop plane is performed using sample consensus method provided by PCL library. From all the segmented planes we select the biggest one assuming the tabletop area represent the biggest planar area in the scene. Once the tabletop plane coefficients (Hessian Normal form) are known we segment the space above it, which contains the objects in the scene. To segment each object from the space above the tabletop plane a euclidean clustering is performed that yields multiple clusters representing each of the objects in the scene. Once the objects in the scene have been segmented we want to do two things: classify each cluster as object or hand (effector) and track their positions for motion analysis.

Classification of objects is done as a simple color filter in HSV color space. Objects that have skin like colors are classified as hand (effector) and as objects otherwise. Further classification of objects into object classes is possible using the methods described in \cite{Terzi}, although it is not integrated yet.

Tracking is done using a standard Kalman filter for each segmented object centroid. The state vector corresponds to $x=(s,v,a)$, with $s$ being the position, $v$ velocity and $a$ acceleration in 3D coordinates. The measurement vector is $m=(s)$. From the estimate state we extract the velocity for further analysis.

After calculating the velocity of each object a simple analysis is done to find out which objects are moving and if those object are approaching or going away from other objects in the scene. %TODO: up and down

Once a raw frame from the Kinect is processed through the pipeline several outputs are available, which will serve as inputs for the action parsing subsystem. The action parsing subsystem is implemented using dynamic neural fields, so we have to provide inputs that can be easily plugged into dynamic neural fields. The outputs share the space dimension and differ in the feature represented in each case (color, motion,...). Space dimension is simplified from 3D to a 2D orthographic projection over the tabletop area as shown in figure \ref{fig:data_projection}. These projections are store in a matrix form that later will be plugged into the dynamic neural fields. The projection process needs a calibration step to calculate the transformation. This process is executed every time the system starts.



\begin{figure}[htp]
\centering
\includegraphics[width=0.8\textwidth]{figs/data_projection.png}
\caption{3D scene projected to 2D view}
\label{fig:data_projection}
\end{figure}

The rest of this section describe with more detail some of the processes that compose the sensory acquisition pipeline.

%\subsubsection{Grabber}

\subsubsection{Calibration}
In order to calculate the transformation needed to go from device coordinates (Kinect camera) to world coordinates, as shown in figure \ref{fig:calibration_frames}, the system needs to be calibrated. This process is performed every time the system starts, because we assume that the device will stay at the same position the whole time.

To find the transformation needed to go from device coordinates to world coordinates we need just need to find the vectors corresponding to the axes of world coordinates in terms of device coordinates. To achieve this we use two markers. One is placed in the origin and a second one along the x axis (of world coordinates). With this we can calculate $X_w$ which is the unit vector that defines the x axis of world coordinates in terms of device coordinates. The segmentation of the tabletop plane gives us the normal vector of that plane that we use to calculate $Z_w$. The cross product of $X_w$ and $Z_w$ gives us $Y_w$ and from that we can calculate the transformation matrix as:

\begin{center}
$M_{d\rightarrow w} = (X_w^T Y_w^T Z_w^T)$
\end{center}

After calibration all the data grabbed from the device is transformed to world coordinates.


\subsubsection{Classifier}
Segmented objects are classified as 'hand' or 'object' in order to simplify the data before plugging it into neural fields. This way we save some computational power avoiding expensive filtering operations with neural fields.

The implemented method is a simple color filter in HSV color space. Segmented object point clouds are filtered with $HSV_{min}$ and $HSV_{max}$ thresholds covering the skin color range (in test scene H=(2-15),S=(50-100),V=(50-180)). After filtering those point clouds with more that a minimum number of points (in test scene minPoints=100) are classified as hand and as object otherwise.

A more sophisticated object classification has been tested using methods described in \cite{Terzi}, although it is not integrated in the current revision of the system.


\subsubsection{Tracker}
We need to calculate the velocity vector of each of the segmented objects in order to analyze the interactions between the objects in the scene. In each step of the sensory acquisition pipeline we have access to the position of all segmented objects plus their classification as hand or object. Given two consecutive iterations $I_{t-1}$ and $I_t$ with segmented objects $S_{t-1}=\{o_0,o_1,...,o_n\}$ and $S_t=\{o'_0,o'_1,...,o'_n\}$, respectively, where $o_n$ represent an object features (position, class, ...), the first step before we can measure the velocity of each object is to assign the elements from $S_{t-1}$ to the elements of $S_t$. This is known as the assignment problem, that in mathematical terms is defined as: given two sets, $A$ and $T$, of equal size, together with a weight function $C:A \times T \rightarrow R$, find a bijection $f : A \rightarrow T$ such that the cost function $\sum\limits_{a\in A}C(a,f(a))$, is minimized\footnote{\url{http://en.wikipedia.org/wiki/Assignment_problem}}.

The cost function in our problem should be small for elements in $S_{t-1}$ and $S_t$ that represent the same real object. A simple cost function is the euclidean distance, assuming that $\Delta t$ between $S_{t-1}$ and $S_t$ is small. Other possible cost functions could use a combination of other object features, such as class match or color distance.

To solve the assignment problem we use the Hungarian algorithm, also known as Kuhn–Munkres algorithm or Munkres assignment algorithm, which will find the bijective function $f:S_{t-1} \times S_t$ that minimizes $\sum\limits_{o\in S_{t-1}} d(o,f(o))$ (d is the euclidean distance).

Once the assignment between $S_{t-1}$ and $S_t$ is found we could calculate the instantaneous velocity for each object. Unfortunately this can't be done that easily. First of all the object segmentation is far from perfect and in some occasions objects in the scene won't be segmented correctly (e.g. occlusions) leading to $S_{t-1}$ and $S_t$ sets with different sizes. And second the segmented object positions will suffer errors, given the noisy RGBD input from the Kinect device, leading to erroneous instantaneous velocities.

The first problem can be solved easily by adding dummy elements to the smallest set to equal the size of the biggest set. The cost function to any dummy element will be a value larger that the maximum cost value. Any assignment to a dummy value will be discarded. The number of assignments is always $min(|S_{t-1}|, |S_t|)$.

The second problems plus the fact in a given step not all elements from $S_{t-1}$ and $S_t$ could be assigned introduces the need of a mechanisms that can keep track of the state of previously seen objects and that can filter noisy measurements. To solve this the set $S_{t-1}$ is replaced by a set of Kalman filters, each of them tracking a previously segmented object. We will call this set $F$. The algorithm to create, update and delete elements from $F$ is shown in alg. \ref{alg:trackpoints}. In each step input is $F$ (empty in the first iteration) and $S$. Assignments are found between both sets using Munkres assignment algorithm. To avoid poor assignments a $max\_cost$ parameter is introduced to reject those assignments with a larger cost (in tests set to 0.2). Then, not assigned filters are check against stale. If a filter hasn't been assigned in a given step a not assigned counter is increased. When a filter reaches $max\_hits$ (in tests set to 10) is considered staled and it is deleted. Finally, for each element in $S$, if said element was assigned to a filter, that filter is updated (also its stale hit counter is set to 0). If said element wasn't assigned, a new filter is created.

\begin{algorithm}                      % enter the algorithm environment
\caption{Tracker}          % give the algorithm a caption
\label{alg:trackpoints} 
\begin{algorithmic}
\Require $F,S$
\Ensure $F$
\State $assignments \gets findAssignments(F,S,max\_cost)$

\If {$|F| > |assignments|$}
\State $F \gets hitNotAssignedFilters(assignments,F)$
\State $F \gets deleteStaleFilters(max\_hits)$
\EndIf

\For{$i \gets 0; i<|S|; i++$}
\If {$isAssigned(assignments,S[i])$}
\State $F \gets updateAssignedFilter(assignments,S[i])$
\Else
\State $F \gets createNewFilter(S[i])$
\EndIf
\EndFor
\end{algorithmic}
\end{algorithm}

The filters are standard Kalman filters implemented in OpenCV. The state vector corresponds to $x=(s,v,a)$, with $s$ being the position, $v$ velocity and $a$ acceleration of the tracked object. The measurement vector is $m=(s)$ extracted from the centroid of segmented objects in each step. The state transition matrix is derived from the equations of motion for uniform acceleration: $s=s_0 + v_0 t + \frac{at^2}{2}$ and  $v=v_0 t + at$. From the estimate state we extract the velocity for further analysis.

\subsubsection{Motion Analysis}
After an iteration from the tracker we have available the velocity vector of each segmented object. With this information we can perform some basic analysis to signal events related to the motion of the objects in our scene.

The first analysis is done per object to signal if an object is moving. This as simple as calculating the norm of $v$. if it is bigger that 0 it indicates that the object is moving. Its value indicates how fast the object is moving. We can also signal if an object is going up or down looking at the z component of $v$.

The second analysis is done pairwise to signal interactions between objects. We are interested in signaling whether an object is approaching or going away from other. Given two objects A and B we calculate how much A is approaching B with the equation:

\begin{center}
$approach_{AB}=\frac{1}{\lVert AB \rVert + \epsilon} \lVert v_A \rVert e^{-  \frac{\langle v_A , AB\rangle^2}{2 \sigma^2}}$
\end{center}

, that defines a Gaussian over the angle between $v_A$ and $AB$ (vector defined between objects A and B centroids), with height inversely proportional to the distance between the objects and proportional to $v_A$ norm. The standard deviation accounts for how much alignment between $v_A$ and $AB$ is needed to influence the signal output (in tests set to 0.3). Figure \ref{fig:approach_signal} shows a graphical representation.

\begin{figure}[htp]
\centering
\includegraphics[width=0.4\textwidth]{figs/approach_detection.png}
\caption{Approach signal}
\label{fig:approach_signal}
\end{figure}

A similar function is used to calculate how much A is going away from B:

\begin{center}
$away_{AB}=\frac{1}{\lVert AB \rVert + \epsilon} \lVert v_A \rVert e^{-  \frac{(\pi-\langle v_A , AB\rangle)^2}{2 \sigma^2}}$
\end{center}

, where we are just inverting the angle to account for the going away direction. Figure \ref{fig:away_signal} shows a graphical representation.

\begin{figure}[htp]
\centering
\includegraphics[width=0.4\textwidth]{figs/away_detection.png}
\caption{Away signal}
\label{fig:away_signal}
\end{figure}

All the signals produce continuous output values. The role to assign a meaning to the signal value and to analyze its temporal evolution is left to the neural dynamics in the action parsing subsystem.

\subsubsection{Output data}
The data produced in this subsystem is going to be plugged into the action parsing subsystem where dynamic neural fields will analyze the temporal evolution. We need to transform the data produced in this step in a way that can serve as input for dynamic neural fields. We can bind all the different data outputs by the shared space dimension and represent them as overlapping channels or layers. The 3D space is  projected onto a 2D view from above to reduce the computation power needed (3D dynamic neural fields are very computing intensive).

The sensory acquisition subsystem outputs several channels. The main ones are:
\begin{itemize}
\item objects color
\item objects height
\item hands color
\item hand height
\item motion
\item approach
\item away
\end{itemize}

The first four ones are projections of the segmentation output split in a color (RGB) and a height channels. The projection is calculated with the calibration data extracted after the calibration process shown before. The 3D data is projected orthogonally to the tabletop plane as if data were observed from above (see figure \ref{fig:data_projection}). The last three channels are produced placing the value of each signal (motion, approach and away) at the position of the related object.

\subsubsection{Cedar integration}
The described pipeline is integrated in the Cedar framework as a plugging, that once loaded provides several components that include the functionality described in this section.

\begin{figure}[htp]
\centering
\includegraphics[width=\textwidth]{figs/tabletop_segmentation_motion_cedar.png}
\caption{Pipeline in Cedar}
\label{fig:pipeline_cedar}
\end{figure}

Figure \ref{fig:pipeline_cedar} shows the main two components. The point cloud grabber (left) implements the hardware interface with the Kinect device, grabbing data and plugging it through cedar to other components. The rest of the functionality is implemented in the component to the right, that receives its input from the point cloud grabber and produces several data channels (object, hands, motion, ...).

\pagebreak 

\bibliographystyle{unsrt}
\bibliography{biblio}
\end{document}
