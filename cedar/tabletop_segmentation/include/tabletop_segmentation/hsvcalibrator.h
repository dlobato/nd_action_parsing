/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UTILITIES_HSVCALIBRATOR_H
#define UTILITIES_HSVCALIBRATOR_H

// LOCAL INCLUDES
//#include "utilities_namespace.h"

// PROJECT INCLUDES

// SYSTEM INCLUDES
#include <cedar/processing/Step.h>
#include <cedar/auxiliaries/MatData.h>
#include <pointcloud2cedar/RGBAPointCloudData.h>
#include <cedar/auxiliaries/StringParameter.h>
#include <cedar/auxiliaries/DoubleParameter.h>
#include <cedar/auxiliaries/IntVectorParameter.h>
#include <boost/scoped_ptr.hpp>


namespace utilities
{
  class HSVCalibrator;
}

/*!@brief Abstract description of the class.
 *
 * More detailed description of the class.
 */
class utilities::HSVCalibrator : public cedar::proc::Step
{
  Q_OBJECT
  //--------------------------------------------------------------------------------------------------------------------
  // macros
  //--------------------------------------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------------------------------------
  // constructors and destructor
  //--------------------------------------------------------------------------------------------------------------------
public:
  //!@brief The standard constructor.
  HSVCalibrator();

  //!@brief Destructor
  ~HSVCalibrator();

  //--------------------------------------------------------------------------------------------------------------------
  // public methods
  //--------------------------------------------------------------------------------------------------------------------
public:
  void compute(const cedar::proc::Arguments&);

  void inputConnectionChanged(const std::string& inputName);

public slots:
  void setPlaneThresh();
  void setMarkerHSVValues();
  void setHandHSVValues();
  void setHandMinPoints();
  void updateProjectionTransform();
  void showPCLVisualizer();

  //--------------------------------------------------------------------------------------------------------------------
  // protected methods
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

  //--------------------------------------------------------------------------------------------------------------------
  // private methods
  //--------------------------------------------------------------------------------------------------------------------
private:
  //--------------------------------------------------------------------------------------------------------------------
  // members
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

private:
  // inputs
  cedar::aux::ConstRGBAPointCloudDataPtr mPointCloud;

  // outputs
  cedar::aux::MatDataPtr mImage;
  

  struct PImpl;
  boost::scoped_ptr<PImpl> mImpl;
  
  //--------------------------------------------------------------------------------------------------------------------
  // parameters
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

private:
  cedar::aux::DoubleParameterPtr _mPlaneThresh;
  cedar::aux::BoolParameterPtr _mInvertPlaneNormal;
  cedar::aux::BoolParameterPtr _mShowPCLVisualizer;
  cedar::aux::BoolParameterPtr _mShowOMarker;
  cedar::aux::BoolParameterPtr _mShowXMarker;
  cedar::aux::BoolParameterPtr _mShowHand;
  cedar::aux::UIntParameterPtr _mOMarkerHMax;
  cedar::aux::UIntParameterPtr _mOMarkerHMin;
  cedar::aux::UIntParameterPtr _mOMarkerSMax;
  cedar::aux::UIntParameterPtr _mOMarkerSMin;
  cedar::aux::UIntParameterPtr _mOMarkerVMax;
  cedar::aux::UIntParameterPtr _mOMarkerVMin;
  cedar::aux::UIntParameterPtr _mXMarkerHMax;
  cedar::aux::UIntParameterPtr _mXMarkerHMin;
  cedar::aux::UIntParameterPtr _mXMarkerSMax;
  cedar::aux::UIntParameterPtr _mXMarkerSMin;
  cedar::aux::UIntParameterPtr _mXMarkerVMax;
  cedar::aux::UIntParameterPtr _mXMarkerVMin;
  cedar::aux::UIntParameterPtr _mHandMinPoints;
  cedar::aux::UIntParameterPtr _mHandHMax;
  cedar::aux::UIntParameterPtr _mHandHMin;
  cedar::aux::UIntParameterPtr _mHandSMax;
  cedar::aux::UIntParameterPtr _mHandSMin;
  cedar::aux::UIntParameterPtr _mHandVMax;
  cedar::aux::UIntParameterPtr _mHandVMin;
  cedar::aux::UIntParameterPtr _mOutputWidth;
  cedar::aux::UIntParameterPtr _mOutputHeight;
}; // class utilities::HSVCalibrator

#endif // UTILITIES_HSVCALIBRATOR_H
