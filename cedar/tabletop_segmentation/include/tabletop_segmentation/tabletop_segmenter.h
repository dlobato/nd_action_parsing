/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UTILITIES_TABLETOP_SEGMENTER_H
#define UTILITIES_TABLETOP_SEGMENTER_H

// LOCAL INCLUDES
//#include "utilities_namespace.h"

// PROJECT INCLUDES
#include "data_types.h"

// SYSTEM INCLUDES
#include <cedar/processing/Step.h>
#include <cedar/auxiliaries/MatData.h>
#include <pointcloud2cedar/RGBAPointCloudData.h>
#include <cedar/auxiliaries/StringParameter.h>
#include <cedar/auxiliaries/DoubleParameter.h>
#include <cedar/auxiliaries/IntVectorParameter.h>
#include <boost/scoped_ptr.hpp>


namespace utilities
{
  class TabletopSegmenter;
}

/*!@brief Abstract description of the class.
 *
 * More detailed description of the class.
 */
class utilities::TabletopSegmenter : public cedar::proc::Step
{
  Q_OBJECT
  //--------------------------------------------------------------------------------------------------------------------
  // macros
  //--------------------------------------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------------------------------------
  // constructors and destructor
  //--------------------------------------------------------------------------------------------------------------------
public:
  //!@brief The standard constructor.
  TabletopSegmenter();

  //!@brief Destructor
  ~TabletopSegmenter();

  //--------------------------------------------------------------------------------------------------------------------
  // public methods
  //--------------------------------------------------------------------------------------------------------------------
public:
  void compute(const cedar::proc::Arguments&);

  void inputConnectionChanged(const std::string& inputName);

  void calibrate();

public slots:
  void setPlaneThresh();
  void setMarkerHSVValues();
  void updateProjectionTransform();
  void showPCLVisualizer();

  //--------------------------------------------------------------------------------------------------------------------
  // protected methods
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

  //--------------------------------------------------------------------------------------------------------------------
  // private methods
  //--------------------------------------------------------------------------------------------------------------------
private:
  //--------------------------------------------------------------------------------------------------------------------
  // members
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

private:
  // inputs
  cedar::aux::ConstRGBAPointCloudDataPtr mPointCloud;
  cedar::aux::ConstMatDataPtr mImage;

  // outputs
  cedar::aux::MatDataPtr mTabletopColor;
  cedar::aux::MatDataPtr mTabletopHeight;
  cedar::aux::MatDataPtr mObjectsColor;
  cedar::aux::MatDataPtr mObjectsHeight;
  cedar::aux::ObjectSetDataPtr mObjectSet;
  

  struct PImpl;
  boost::scoped_ptr<PImpl> mImpl;
  
  //--------------------------------------------------------------------------------------------------------------------
  // parameters
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

private:
  cedar::aux::DoubleParameterPtr _mPlaneThresh;
  cedar::aux::BoolParameterPtr _mInvertPlaneNormal;
  cedar::aux::BoolParameterPtr _mShowPCLVisualizer;
  cedar::aux::UIntParameterPtr _mOMarkerHMax;
  cedar::aux::UIntParameterPtr _mOMarkerHMin;
  cedar::aux::UIntParameterPtr _mOMarkerSMax;
  cedar::aux::UIntParameterPtr _mOMarkerSMin;
  cedar::aux::UIntParameterPtr _mOMarkerVMax;
  cedar::aux::UIntParameterPtr _mOMarkerVMin;
  cedar::aux::UIntParameterPtr _mXMarkerHMax;
  cedar::aux::UIntParameterPtr _mXMarkerHMin;
  cedar::aux::UIntParameterPtr _mXMarkerSMax;
  cedar::aux::UIntParameterPtr _mXMarkerSMin;
  cedar::aux::UIntParameterPtr _mXMarkerVMax;
  cedar::aux::UIntParameterPtr _mXMarkerVMin;
  cedar::aux::UIntParameterPtr _mOutputWidth;
  cedar::aux::UIntParameterPtr _mOutputHeight;
}; // class utilities::TabletopSegmenter

#endif // UTILITIES_TABLETOP_SEGMENTER_H
