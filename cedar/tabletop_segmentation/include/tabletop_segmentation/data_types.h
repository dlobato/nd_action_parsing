/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UTILITIES_TABLETOP_SEGMENTATION_DATATYPES_H
#define UTILITIES_TABLETOP_SEGMENTATION_DATATYPES_H

// CEDAR INCLUDES
#include <cedar/auxiliaries/DataTemplate.h>

// PROJECT INCLUDES
#include <tabletop_segmentation/tabletop_object.h>
namespace cedar{
  namespace aux{
    //!@brief A concretization of DataTemplate for a set of objects
    typedef DataTemplate<std::vector<ActionParsing::TableTopObjectPtr> > ObjectSetData;
    CEDAR_GENERATE_POINTER_TYPES(ObjectSetData);
  }
}

#endif //UTILITIES_TABLETOP_SEGMENTATION_DATATYPES_H
