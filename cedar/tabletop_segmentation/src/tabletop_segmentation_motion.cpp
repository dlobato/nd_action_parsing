/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// LOCAL INCLUDES
#include <tabletop_segmentation/tabletop_segmentation_motion.h>

// PROJECT INCLUDES
#include <tabletop_segmentation/tabletop_segmentation.h>
#include <tabletop_segmentation/tabletop_handclassifier.h>
#include <tabletop_segmentation/tabletop_calibration.h>
#include <tabletop_segmentation/tabletop_xyprojection.h>
#include <tabletop_segmentation/tabletop_3dpointtracker.h>
#include <tabletop_segmentation/hsvutil.h>

// CEDAR INCLUDES
#include <cedar/auxiliaries/EnumParameter.h>
#include <cedar/auxiliaries/assert.h>
#include <cedar/auxiliaries/exceptions.h>
#include <cedar/auxiliaries/Log.h>

// SYSTEM INCLUDES
#include <pcl/filters/voxel_grid.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/centroid.h>
#include <boost/assign/list_of.hpp> 


//----------------------------------------------------------------------------------------------------------------------
// constructors and destructor
//----------------------------------------------------------------------------------------------------------------------

//default values for parameter initialization
//HSV values are in opencv ranges
const float defaultFocalLenght = 525;//kinect at 640x480 FIXME
const float defaultImageCenterX = 320;
const float defaultImageCenterY = 240;
const unsigned char defaultProjectionWidth=80;
const unsigned char defaultProjectionHeight=60;
const double planeThresholdDefault = 0.02;
const unsigned char oMarkerHMaxDefault = 70;
const unsigned char oMarkerHMinDefault = 40;
const unsigned char oMarkerSMaxDefault = 255;
const unsigned char oMarkerSMinDefault = 70;
const unsigned char oMarkerVMaxDefault = 255;
const unsigned char oMarkerVMinDefault = 150;
const unsigned char xMarkerHMaxDefault = 10;
const unsigned char xMarkerHMinDefault = 0;
const unsigned char xMarkerSMaxDefault = 150;
const unsigned char xMarkerSMinDefault = 70;
const unsigned char xMarkerVMaxDefault = 255;
const unsigned char xMarkerVMinDefault = 150;
const unsigned char handMinPointsDefault = 100;
const unsigned char handHMaxDefault = 15;
const unsigned char handHMinDefault = 2;
const unsigned char handSMaxDefault = 100;
const unsigned char handSMinDefault = 50;
const unsigned char handVMaxDefault = 180;
const unsigned char handVMinDefault = 50;
const double velocityThresholdDefault = 0.01;//m/s ??
const double approachAwayMinDistanceDefault = 0.05;//m
const double approachAwaySigmaDefault = 0.3;//rad


const std::vector<unsigned char> oMarkerHSVMin = boost::assign::list_of(oMarkerHMinDefault)(oMarkerSMinDefault)(oMarkerVMinDefault);
const std::vector<unsigned char> oMarkerHSVMax = boost::assign::list_of(oMarkerHMaxDefault)(oMarkerSMaxDefault)(oMarkerVMaxDefault);
const std::vector<unsigned char> xMarkerHSVMin = boost::assign::list_of(xMarkerHMinDefault)(xMarkerSMinDefault)(xMarkerVMinDefault);
const std::vector<unsigned char> xMarkerHSVMax = boost::assign::list_of(xMarkerHMaxDefault)(xMarkerSMaxDefault)(xMarkerVMaxDefault);
const std::vector<unsigned char> handHSVMin = boost::assign::list_of(handHMinDefault)(handSMinDefault)(handVMinDefault);
const std::vector<unsigned char> handHSVMax = boost::assign::list_of(handHMaxDefault)(handSMaxDefault)(handVMaxDefault);


struct utilities::TabletopSegmentationMotion::PImpl{
  PImpl():
    segmenter(planeThresholdDefault),
    handclassifier(ActionParsing::hsvFromOpenCV(handHSVMin),
		   ActionParsing::hsvFromOpenCV(handHSVMax)),
    calibrator(defaultFocalLenght, defaultImageCenterX, defaultImageCenterY,
	       ActionParsing::hsvFromOpenCV(oMarkerHSVMin),
	       ActionParsing::hsvFromOpenCV(oMarkerHSVMax),
	       ActionParsing::hsvFromOpenCV(xMarkerHSVMin),
	       ActionParsing::hsvFromOpenCV(xMarkerHSVMax)),
    downsampled_cloud(new cedar::aux::RGBAPointCloud),
    table_cloud(new cedar::aux::RGBAPointCloud),
    objects_cloud(new cedar::aux::RGBAPointCloud),
    hands_cloud(new cedar::aux::RGBAPointCloud),
    calibrated(false) {}
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
  int vp_1,vp_2,vp_3,vp_4;
  ActionParsing::TableTopSegmentation segmenter;
  ActionParsing::TableTopHandClassifier handclassifier;
  ActionParsing::TableTopCalibration calibrator;
  boost::shared_ptr<ActionParsing::TableTopXYProjection> xyprojection_ptr;
  ActionParsing::TableTop3DPointTracker tracker;
  pcl::VoxelGrid<pcl::PointXYZRGBA> downsampling_grid;
  cedar::aux::RGBAPointCloudPtr downsampled_cloud;
  cedar::aux::RGBAPointCloudPtr table_cloud;
  cedar::aux::RGBAPointCloudPtr table_hull;
  cedar::aux::RGBAPointCloudPtr objects_cloud;
  cedar::aux::RGBAPointCloudPtr hands_cloud;
  Eigen::Vector4f table_plane_normal;
  Eigen::Vector3f table_size_worldf;
  bool calibrated;
};

utilities::TabletopSegmentationMotion::TabletopSegmentationMotion()
  :
  mTabletopColor(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_8UC3))),
  mTabletopHeight(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_32FC1))),
  mObjectsColor(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_8UC3))),
  mObjectsHeight(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_32FC1))),
  mHandsColor(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_8UC3))),
  mHandsHeight(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_32FC1))),
  mMotion(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_32FC1))),
  mApproach(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_32FC1))),
  mAway(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_32FC1))),
  mOnTop(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_32FC1))),
  mUp(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_32FC1))),
  mDown(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_32FC1))),
  mImpl(new PImpl()),
  _mPlaneThresh(new cedar::aux::DoubleParameter(this,"plane threshold", planeThresholdDefault)),
  _mInvertPlaneNormal(new cedar::aux::BoolParameter(this,"invert plane normal", false)),
  _mShowPCLVisualizer(new cedar::aux::BoolParameter(this,"show PCL Viz", false)),
  _mOMarkerHMax(new cedar::aux::UIntParameter(this,"O marker H Max",oMarkerHMaxDefault,0,180)),
  _mOMarkerHMin(new cedar::aux::UIntParameter(this,"O marker H Min",oMarkerHMinDefault,0,180)),
  _mOMarkerSMax(new cedar::aux::UIntParameter(this,"O marker S Max",oMarkerSMaxDefault,0,255)),
  _mOMarkerSMin(new cedar::aux::UIntParameter(this,"O marker S Min",oMarkerSMinDefault,0,255)),
  _mOMarkerVMax(new cedar::aux::UIntParameter(this,"O marker V Max",oMarkerVMaxDefault,0,255)),
  _mOMarkerVMin(new cedar::aux::UIntParameter(this,"O marker V Min",oMarkerVMinDefault,0,255)),
  _mXMarkerHMax(new cedar::aux::UIntParameter(this,"X marker H Max",xMarkerHMaxDefault,0,180)),
  _mXMarkerHMin(new cedar::aux::UIntParameter(this,"X marker H Min",xMarkerHMinDefault,0,180)),
  _mXMarkerSMax(new cedar::aux::UIntParameter(this,"X marker S Max",xMarkerSMaxDefault,0,255)),
  _mXMarkerSMin(new cedar::aux::UIntParameter(this,"X marker S Min",xMarkerSMinDefault,0,255)),
  _mXMarkerVMax(new cedar::aux::UIntParameter(this,"X marker V Max",xMarkerVMaxDefault,0,255)),
  _mXMarkerVMin(new cedar::aux::UIntParameter(this,"X marker V Min",xMarkerVMinDefault,0,255)),
  _mSegmentHands(new cedar::aux::BoolParameter(this,"Segment hands", true)),
  _mHandMinPoints(new cedar::aux::UIntParameter(this,"Min hand points",handMinPointsDefault,0,1000)),
  _mHandHMax(new cedar::aux::UIntParameter(this,"Hand H Max",handHMaxDefault,0,180)),
  _mHandHMin(new cedar::aux::UIntParameter(this,"Hand H Min",handHMinDefault,0,180)),
  _mHandSMax(new cedar::aux::UIntParameter(this,"Hand S Max",handSMaxDefault,0,255)),
  _mHandSMin(new cedar::aux::UIntParameter(this,"Hand S Min",handSMinDefault,0,255)),
  _mHandVMax(new cedar::aux::UIntParameter(this,"Hand V Max",handVMaxDefault,0,255)),
  _mHandVMin(new cedar::aux::UIntParameter(this,"Hand V Min",handVMinDefault,0,255)),
  _mOutputWidth(new cedar::aux::UIntParameter(this,"projection width",defaultProjectionWidth,0,2000)),
  _mOutputHeight(new cedar::aux::UIntParameter(this,"projection height",defaultProjectionHeight,0,2000)),
  //_mVelocityThreshold(new cedar::aux::DoubleParameter(this,"velocity threshold", velocityThresholdDefault))
  _mApproachAwayMinDistance(new cedar::aux::DoubleParameter(this,"approach/away min distance", approachAwayMinDistanceDefault)),
  _mApproachAwaySigma(new cedar::aux::DoubleParameter(this,"approach/away sigma", approachAwaySigmaDefault))
{

  //declare inputs/outputs
  this->declareInput("input_cloud");
  this->declareOutput("tabletop_color", this->mTabletopColor);
  this->declareOutput("tabletop_height", this->mTabletopHeight);
  this->declareOutput("objects_color", this->mObjectsColor);
  this->declareOutput("objects_height", this->mObjectsHeight);
  this->declareOutput("hands_color", this->mHandsColor);
  this->declareOutput("hands_height", this->mHandsHeight);
  this->declareOutput("motion", this->mMotion);
  this->declareOutput("approach", this->mApproach);
  this->declareOutput("away", this->mAway);
  this->declareOutput("ontop", this->mOnTop);
  this->declareOutput("up", this->mUp);
  this->declareOutput("down", this->mDown);
  
  
  QObject::connect(_mPlaneThresh.get(), SIGNAL(valueChanged()), this, SLOT(setPlaneThresh()));
  QObject::connect(_mShowPCLVisualizer.get(), SIGNAL(valueChanged()), this, SLOT(showPCLVisualizer()));
  QObject::connect(_mOMarkerHMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerHMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerSMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerSMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerVMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerVMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerHMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerHMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerSMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerSMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerVMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerVMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mHandMinPoints.get(), SIGNAL(valueChanged()), this, SLOT(setHandMinPoints()));
  QObject::connect(_mHandHMax.get(), SIGNAL(valueChanged()), this, SLOT(setHandHSVValues()));
  QObject::connect(_mHandHMin.get(), SIGNAL(valueChanged()), this, SLOT(setHandHSVValues()));
  QObject::connect(_mHandSMax.get(), SIGNAL(valueChanged()), this, SLOT(setHandHSVValues()));
  QObject::connect(_mHandSMin.get(), SIGNAL(valueChanged()), this, SLOT(setHandHSVValues()));
  QObject::connect(_mHandVMax.get(), SIGNAL(valueChanged()), this, SLOT(setHandHSVValues()));
  QObject::connect(_mHandVMin.get(), SIGNAL(valueChanged()), this, SLOT(setHandHSVValues()));
  QObject::connect(_mOutputWidth.get(), SIGNAL(valueChanged()), this, SLOT(updateProjectionTransform()));
  QObject::connect(_mOutputHeight.get(), SIGNAL(valueChanged()), this, SLOT(updateProjectionTransform()));

  this->registerFunction("calibrate", boost::bind(&utilities::TabletopSegmentationMotion::calibrate, this));
  
  this->setPlaneThresh();
  this->setMarkerHSVValues();
  this->setHandHSVValues();
  this->setHandMinPoints();
  this->updateProjectionTransform();
  this->showPCLVisualizer();

  this->mImpl->downsampling_grid.setFilterFieldName ("z");//TODO: set as parameters
  this->mImpl->downsampling_grid.setFilterLimits (0.0f, 2.0f);
  this->mImpl->downsampling_grid.setLeafSize (0.005f, 0.005f, 0.005f);
}

utilities::TabletopSegmentationMotion::~TabletopSegmentationMotion(){}

//----------------------------------------------------------------------------------------------------------------------
// methods
//----------------------------------------------------------------------------------------------------------------------


void utilities::TabletopSegmentationMotion::compute(const cedar::proc::Arguments&)
{
  if (!this->mPointCloud || this->mPointCloud->getData().empty())
    return;

  if (!this->mImpl->calibrated)
    return;

  cedar::aux::ConstRGBAPointCloudPtr input_cloud(this->mPointCloud->getData().makeShared());//this make a copy. FIXME: better way to do this??
    
  this->mImpl->downsampling_grid.setInputCloud (input_cloud);
  this->mImpl->downsampling_grid.filter (*this->mImpl->downsampled_cloud);
  
  std::vector<pcl::PointIndices> object_clusters;
  if (this->mImpl->segmenter.segmentAndClusterObjects(this->mImpl->downsampled_cloud,this->mImpl->table_hull,this->mImpl->objects_cloud, object_clusters)->empty()){
    cedar::aux::LogSingleton::getInstance()->debugMessage("no objects segmented");
    return;
  }
 
  cedar::aux::RGBAPointCloudPtr new_objects_cloud(new cedar::aux::RGBAPointCloud);
  cedar::aux::RGBAPointCloudPtr new_hands_cloud(new cedar::aux::RGBAPointCloud);
  cedar::aux::RGBAPointCloudPtr tmp_cloud(new cedar::aux::RGBAPointCloud);
  std::vector<cv::Point3f> points_to_track_worldf(object_clusters.size());
  std::vector<cv::Point3f> tracked_points_position_worldf(object_clusters.size());
  std::vector<cv::Point3f> tracked_points_velocity_worldf(object_clusters.size());
  for (unsigned int i=0; i < object_clusters.size(); i++){
    Eigen::Vector4f centroid;
    Eigen::Vector3f centroid_worldf;
    if (this->_mSegmentHands->getValue() && this->mImpl->handclassifier.classifyHand(this->mImpl->objects_cloud, object_clusters[i],*tmp_cloud)){
      *new_hands_cloud += *tmp_cloud;
      pcl::compute3DCentroid(*tmp_cloud, centroid); 
    }else{
      *new_objects_cloud += cedar::aux::RGBAPointCloud(*this->mImpl->objects_cloud, object_clusters[i].indices);
      pcl::compute3DCentroid(*this->mImpl->objects_cloud, object_clusters[i].indices, centroid);
    }
    centroid_worldf = this->mImpl->calibrator.transformPointToWorldCoordinates(centroid.segment<3>(0));
    points_to_track_worldf[i] = cv::Point3f(centroid_worldf.x(),centroid_worldf.y(),centroid_worldf.z());
  }
  this->mImpl->objects_cloud.swap(new_objects_cloud);
  this->mImpl->hands_cloud.swap(new_hands_cloud);

  this->mImpl->tracker.track(points_to_track_worldf, tracked_points_position_worldf, tracked_points_velocity_worldf);

  //fill color & height outputs
  CEDAR_ASSERT(this->mImpl->xyprojection_ptr);
  const float output_width = (float)this->_mOutputWidth->getValue();
  const float output_height = (float)this->_mOutputHeight->getValue();
  //tabletop: complete cloud without segmentation
  cv::Mat &tabletop_height = this->mTabletopHeight->getData();
  cv::Mat &tabletop_color = this->mTabletopColor->getData();
  tabletop_height.create(output_height,output_width,CV_32FC1);
  tabletop_color.create(output_height,output_width,CV_8UC3);
  CEDAR_ASSERT(tabletop_height.isContinuous());
  CEDAR_ASSERT(tabletop_color.isContinuous());
  this->mImpl->xyprojection_ptr->transformPointCloudToXYProjection(*this->mImpl->downsampled_cloud, tabletop_height.ptr<float>(), tabletop_color.ptr<unsigned char>(),
  								   this->mImpl->calibrator.getTransformationToWorldCoordinates());
  cv::cvtColor(tabletop_color,tabletop_color,CV_RGB2BGR);

  //objects: just objects
  cv::Mat &objects_height = this->mObjectsHeight->getData();
  cv::Mat &objects_color = this->mObjectsColor->getData();
  objects_height.create(output_height,output_width,CV_32FC1);
  objects_color.create(output_height,output_width,CV_8UC3);
  CEDAR_ASSERT(objects_height.isContinuous());
  CEDAR_ASSERT(objects_color.isContinuous());
  this->mImpl->xyprojection_ptr->transformPointCloudToXYProjection(*this->mImpl->objects_cloud, objects_height.ptr<float>(), objects_color.ptr<unsigned char>(),
  								   this->mImpl->calibrator.getTransformationToWorldCoordinates());
  cv::cvtColor(objects_color,objects_color,CV_RGB2BGR);
  
  //hands: just hands
  if (this->_mSegmentHands->getValue()){
    cv::Mat &hands_height = this->mHandsHeight->getData();
    cv::Mat &hands_color = this->mHandsColor->getData();
    hands_height.create(output_height,output_width,CV_32FC1);
    hands_color.create(output_height,output_width,CV_8UC3);
    CEDAR_ASSERT(hands_height.isContinuous());
    CEDAR_ASSERT(hands_color.isContinuous());
    this->mImpl->xyprojection_ptr->transformPointCloudToXYProjection(*this->mImpl->hands_cloud, hands_height.ptr<float>(), hands_color.ptr<unsigned char>(),
    								     this->mImpl->calibrator.getTransformationToWorldCoordinates());
    cv::cvtColor(hands_color,hands_color,CV_RGB2BGR);
  }

  //fill motion, approach,away, ontop, up and down outputs
  cv::Mat &motion = this->mMotion->getData();
  motion.create(output_height,output_width,CV_32FC1);
  motion = cv::Scalar::all(0.0);
  cv::Mat &up = this->mUp->getData();
  up.create(output_height,output_width,CV_32FC1);
  up = cv::Scalar::all(0.0);
  cv::Mat &down = this->mDown->getData();
  down.create(output_height,output_width,CV_32FC1);
  down = cv::Scalar::all(0.0);
  cv::Mat &approach = this->mApproach->getData();
  approach.create(output_height,output_width,CV_32FC1);
  approach = cv::Scalar::all(0.0);
  cv::Mat &away = this->mAway->getData();
  away.create(output_height,output_width,CV_32FC1);
  away = cv::Scalar::all(0.0);
  cv::Mat &ontop = this->mOnTop->getData();
  ontop.create(output_height,output_width,CV_32FC1);
  ontop = cv::Scalar::all(0.0);
  //const float velocity_threshold = (float)this->_mVelocityThreshold->getValue();
  const float min_distance = (float)this->_mApproachAwayMinDistance->getValue();
  const float sigma = (float)this->_mApproachAwaySigma->getValue();
  const float sigma2 = sigma*sigma;
  for (unsigned int i=0; i<tracked_points_position_worldf.size(); i++){
    Eigen::Vector3f tracked_point_pos_worldf(tracked_points_position_worldf[i].x, tracked_points_position_worldf[i].y, tracked_points_position_worldf[i].z);
    Eigen::Vector3f tracked_point_vel_worldf(tracked_points_velocity_worldf[i].x, tracked_points_velocity_worldf[i].y, tracked_points_velocity_worldf[i].z);
     
    Eigen::Vector3f tracked_point_pos_xyprojectionf = this->mImpl->xyprojection_ptr->getTransformationToXYProjection() * tracked_point_pos_worldf;
    float tracked_point_velxy_norm_worldf = tracked_point_vel_worldf.segment<2>(0).norm();
    float tracked_point_velz_norm_worldf = tracked_point_vel_worldf.z();
    
    //std::cerr << "tracked_point_pos_xyprojectionf=" << tracked_point_pos_xyprojectionf << std::endl;
    cv::Point tracked_p(static_cast<int>(pcl_round(tracked_point_pos_xyprojectionf.x())),
			static_cast<int>(pcl_round(tracked_point_pos_xyprojectionf.y())));
    //std::cerr << "tracked_point_pos_xyprojectionf(cv::Point)=" << p << std::endl;
    if (tracked_p.x >= 0 && tracked_p.x < output_width && tracked_p.y >= 0 && tracked_p.y < output_height){//check tracked_p is inside limits 
	motion.at<float>(tracked_p) = tracked_point_velxy_norm_worldf;
	if (tracked_point_velz_norm_worldf > 0.0)
	  up.at<float>(tracked_p) = tracked_point_velz_norm_worldf; 
	else
	  down.at<float>(tracked_p) = -tracked_point_velz_norm_worldf;
    }
    //calc approach and away signals with all the other objects
    for (unsigned int j=0; j<tracked_points_position_worldf.size(); j++){
      if (i!=j){//skip check for same point
	Eigen::Vector3f target_point_pos_worldf(tracked_points_position_worldf[j].x,tracked_points_position_worldf[j].y, tracked_points_position_worldf[j].z);
	Eigen::Vector3f target_point_pos_xyprojectionf = this->mImpl->xyprojection_ptr->getTransformationToXYProjection() * target_point_pos_worldf;
	Eigen::Vector3f tracked_to_target_worldf = target_point_pos_worldf-tracked_point_pos_worldf;
	//XY plane atan2(perp_product(tracked_point_vel_worldf,tracked_to_target_worldf),dot_product(tracked_point_vel_worldf,tracked_to_target_worldf)) = [-pi..pi]
	float tracked_vel_to_target_angle = fabs(atan2(tracked_point_vel_worldf.x()*tracked_to_target_worldf.y()-tracked_point_vel_worldf.y()*tracked_to_target_worldf.x(),
						       tracked_point_vel_worldf.x()*tracked_to_target_worldf.x()+tracked_point_vel_worldf.y()*tracked_to_target_worldf.y()));
	float tracked_vel_to_target_away_angle = M_PI - tracked_vel_to_target_angle;
	float tracked_to_target_distance = tracked_to_target_worldf.norm() + min_distance;
	//std::cerr << "vel.norm(" << i << ")=" << tracked_point_vel_norm_worldf << ", angle(" << i << "," << j << ")=" << tracked_vel_to_target_angle << std::endl;
	//std::cerr << "target_point_pos_xyprojectionf=" << target_point_pos_xyprojectionf << std::endl;
	cv::Point target_p(static_cast<int>(pcl_round(target_point_pos_xyprojectionf.x())),
			   static_cast<int>(pcl_round(target_point_pos_xyprojectionf.y())));
	//std::cerr << "target_point_pos_xyprojectionf(cv::Point)=" << p << std::endl;
	if (target_p.x >= 0 && target_p.x < output_width && target_p.y >= 0 && target_p.y < output_height){
	  approach.at<float>(target_p) +=  (1./tracked_to_target_distance) * tracked_point_velxy_norm_worldf * expf(-powf(tracked_vel_to_target_angle,2.)/(2.*sigma2));
	  away.at<float>(target_p) +=  (1./tracked_to_target_distance) * tracked_point_velxy_norm_worldf * expf(-powf(tracked_vel_to_target_away_angle,2.)/(2.*sigma2));
	  ontop.at<float>(target_p) += expf(-powf(tracked_to_target_distance,2.)/(2.*sigma2));
	}
      }
    }
  }
	

  //if PCL viewer available
  if (!this->mImpl->viewer || this->mImpl->viewer->wasStopped())//not sure what happens when user close PCL window
    return;

  if (!this->mImpl->viewer->updatePointCloud(input_cloud,"input_cloud"))
    this->mImpl->viewer->addPointCloud(input_cloud,"input_cloud",this->mImpl->vp_1);
  if (!this->mImpl->viewer->updatePointCloud(this->mImpl->table_cloud,"table_cloud"))
    this->mImpl->viewer->addPointCloud(this->mImpl->table_cloud,"table_cloud",this->mImpl->vp_2);
  if (!this->mImpl->viewer->updatePointCloud(this->mImpl->objects_cloud,"objects_cloud"))
    this->mImpl->viewer->addPointCloud(this->mImpl->objects_cloud,"objects_cloud",this->mImpl->vp_3);
  if (!this->mImpl->viewer->updatePointCloud(this->mImpl->hands_cloud,"hands_cloud"))
    this->mImpl->viewer->addPointCloud(this->mImpl->hands_cloud,"hands_cloud",this->mImpl->vp_4);

  //draw axis
  Eigen::Vector3f o_position,x_direction,y_direction,z_direction,n_direction;
  o_position = this->mImpl->calibrator.transformPointToDeviceCoordinates(Eigen::Vector3f::Zero(3));
  x_direction = this->mImpl->calibrator.transformPointToDeviceCoordinates(Eigen::Vector3f::UnitX());
  y_direction = this->mImpl->calibrator.transformPointToDeviceCoordinates(Eigen::Vector3f::UnitY());
  z_direction = this->mImpl->calibrator.transformPointToDeviceCoordinates(Eigen::Vector3f::UnitZ());
  this->mImpl->viewer->removeAllShapes(this->mImpl->vp_2);
  this->mImpl->viewer->addLine(pcl::PointXYZ(o_position.x(),o_position.y(),o_position.z()),
  			      pcl::PointXYZ(x_direction.x(),x_direction.y(),x_direction.z()),
  			      255.0,0.0,0.0,"tabletop_tf_arrow_x",this->mImpl->vp_2);
  this->mImpl->viewer->addLine(pcl::PointXYZ(o_position.x(),o_position.y(),o_position.z()),
  			      pcl::PointXYZ(y_direction.x(),y_direction.y(),y_direction.z()),
  			      0.0,255.0,0.0,"tabletop_tf_arrow_y",this->mImpl->vp_2);
  this->mImpl->viewer->addLine(pcl::PointXYZ(o_position.x(),o_position.y(),o_position.z()),
  			      pcl::PointXYZ(z_direction.x(),z_direction.y(),z_direction.z()),
  			      0.0,0.0,255.0,"tabletop_tf_arrow_z",this->mImpl->vp_2);
  this->mImpl->viewer->spinOnce(100);
}

void utilities::TabletopSegmentationMotion::inputConnectionChanged(const std::string& inputName)
{
  if (inputName == "input_cloud")
  {
    this->mPointCloud = boost::shared_dynamic_cast<const cedar::aux::RGBAPointCloudData>(this->getInput("input_cloud"));
    this->calibrate();
  }
  this->onTrigger();
}

void utilities::TabletopSegmentationMotion::calibrate(){
  if (!this->mPointCloud || this->mPointCloud->getData().empty())
    return;

  this->mImpl->calibrated = false;
  cedar::aux::ConstRGBAPointCloudPtr input_cloud(this->mPointCloud->getData().makeShared());//this make a copy. FIXME: better way to do this??
  
  this->mImpl->downsampling_grid.setInputCloud (input_cloud);
  this->mImpl->downsampling_grid.filter (*(this->mImpl->downsampled_cloud));

  if(!this->mImpl->segmenter.segmentTableTop(this->mImpl->downsampled_cloud,this->mImpl->table_cloud,this->mImpl->table_plane_normal)){//tabletop segmentation failed
    this->setState(cedar::proc::Triggerable::STATE_EXCEPTION,"Tabletop segmentation failed.");
    cedar::aux::LogSingleton::getInstance()->systemInfo("Tabletop segmentation failed","utilities::TabletopSegmentationMotion::calibrate()");
    return;
  }
  this->mImpl->segmenter.calcTabletopHull(this->mImpl->table_cloud,this->mImpl->table_hull);

  if (this->_mInvertPlaneNormal->getValue())
    this->mImpl->table_plane_normal *= -1.0;

  if (!this->mImpl->calibrator.calibrate(this->mImpl->table_cloud, this->mImpl->table_plane_normal)){
    this->setState(cedar::proc::Triggerable::STATE_EXCEPTION,"Calibration failed.");
    cedar::aux::LogSingleton::getInstance()->systemInfo("Calibration failed","utilities::TabletopSegmentationMotion::calibrate()");
    return;
  }

  this->setState(cedar::proc::Triggerable::STATE_UNKNOWN,"Calibration success.");
  cedar::aux::LogSingleton::getInstance()->systemInfo("Calibration success","utilities::TabletopSegmentationMotion::calibrate()");
  cedar::aux::RGBAPointCloudPtr table_cloud_worldf(new cedar::aux::RGBAPointCloud);
  this->mImpl->calibrator.transformPointCloudToWorldCoordinates(*this->mImpl->table_cloud, *table_cloud_worldf);
  this->mImpl->table_size_worldf = this->mImpl->segmenter.calcTabletopSize(*table_cloud_worldf);
  this->mImpl->calibrated = true;
  this->updateProjectionTransform();
  return;
}

void utilities::TabletopSegmentationMotion::setPlaneThresh() {
  this->mImpl->segmenter.setPlaneThreshold(_mPlaneThresh->getValue());
}

void utilities::TabletopSegmentationMotion::setMarkerHSVValues() {
  std::vector<unsigned char> oMarkerHSVMin = boost::assign::list_of(static_cast<unsigned char>(_mOMarkerHMin->getValue()))(static_cast<unsigned char>(_mOMarkerSMin->getValue()))(static_cast<unsigned char>(_mOMarkerVMin->getValue()));
  std::vector<unsigned char> oMarkerHSVMax = boost::assign::list_of(static_cast<unsigned char>(_mOMarkerHMax->getValue()))(static_cast<unsigned char>(_mOMarkerSMax->getValue()))(static_cast<unsigned char>(_mOMarkerVMax->getValue()));
  std::vector<unsigned char> xMarkerHSVMin = boost::assign::list_of(static_cast<unsigned char>(_mXMarkerHMin->getValue()))(static_cast<unsigned char>(_mXMarkerSMin->getValue()))(static_cast<unsigned char>(_mXMarkerVMin->getValue()));
  std::vector<unsigned char> xMarkerHSVMax = boost::assign::list_of(static_cast<unsigned char>(_mXMarkerHMax->getValue()))(static_cast<unsigned char>(_mXMarkerSMax->getValue()))(static_cast<unsigned char>(_mXMarkerVMax->getValue()));
  this->mImpl->calibrator.setMarkerHSVValues(ActionParsing::hsvFromOpenCV(oMarkerHSVMin),ActionParsing::hsvFromOpenCV(oMarkerHSVMax),
					     ActionParsing::hsvFromOpenCV(xMarkerHSVMin),ActionParsing::hsvFromOpenCV(xMarkerHSVMax));
}

void utilities::TabletopSegmentationMotion::setHandHSVValues() {
  std::vector<unsigned char> handHSVMin = boost::assign::list_of(static_cast<unsigned char>(_mHandHMin->getValue()))(static_cast<unsigned char>(_mHandSMin->getValue()))(static_cast<unsigned char>(_mHandVMin->getValue()));
  std::vector<unsigned char> handHSVMax = boost::assign::list_of(static_cast<unsigned char>(_mHandHMax->getValue()))(static_cast<unsigned char>(_mHandSMax->getValue()))(static_cast<unsigned char>(_mHandVMax->getValue()));
  this->mImpl->handclassifier.setHandHSVValues(ActionParsing::hsvFromOpenCV(handHSVMin),ActionParsing::hsvFromOpenCV(handHSVMax));
}

void utilities::TabletopSegmentationMotion::setHandMinPoints() {
  this->mImpl->handclassifier.setMinPoints(this->_mHandMinPoints->getValue());
}

void utilities::TabletopSegmentationMotion::updateProjectionTransform(){
  const float output_width = (float)this->_mOutputWidth->getValue();
  const float output_height = (float)this->_mOutputHeight->getValue();

  if (!this->mImpl->calibrated)
    return;
  
  this->mImpl->xyprojection_ptr.reset(new ActionParsing::TableTopXYProjection(output_width,output_height,
									      this->mImpl->table_size_worldf.x(),this->mImpl->table_size_worldf.y()));
}

void utilities::TabletopSegmentationMotion::showPCLVisualizer(){

  if (this->mImpl->viewer){
    this->mImpl->viewer->removeAllPointClouds();
    this->mImpl->viewer->removeAllShapes();
  }

  this->mImpl->viewer.reset();
  if (this->_mShowPCLVisualizer->getValue()){
    this->mImpl->viewer.reset(new pcl::visualization::PCLVisualizer("PCL viz"));
    this->mImpl->viewer->setBackgroundColor (0, 0, 0);
    this->mImpl->viewer->createViewPort (0, 0, 0.25, 1, this->mImpl->vp_1);
    this->mImpl->viewer->createViewPort (0.25, 0, 0.5, 1, this->mImpl->vp_2);
    this->mImpl->viewer->createViewPort (0.5, 0, 0.75, 1, this->mImpl->vp_3);
    this->mImpl->viewer->createViewPort (0.75, 0, 1, 1, this->mImpl->vp_4);
    this->mImpl->viewer->addCoordinateSystem (0.5, this->mImpl->vp_2);
  }
}
