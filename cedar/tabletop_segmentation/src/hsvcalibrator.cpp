/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// LOCAL INCLUDES
#include <tabletop_segmentation/hsvcalibrator.h>

// PROJECT INCLUDES
#include <tabletop_segmentation/tabletop_segmentation.h>
#include <tabletop_segmentation/tabletop_handclassifier.h>
#include <tabletop_segmentation/tabletop_calibration.h>
#include <tabletop_segmentation/tabletop_xyprojection.h>
#include <tabletop_segmentation/hsvutil.h>

// SYSTEM INCLUDES
#include <cedar/auxiliaries/EnumParameter.h>
#include <cedar/auxiliaries/assert.h>
#include <cedar/auxiliaries/exceptions.h>
#include <cedar/auxiliaries/Log.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <boost/assign/list_of.hpp> 


//----------------------------------------------------------------------------------------------------------------------
// constructors and destructor
//----------------------------------------------------------------------------------------------------------------------

//default values for parameter initialization
//HSV values are in opencv ranges
const float defaultFocalLenght = 525;//kinect at 640x480 FIXME
const float defaultImageCenterX = 320;
const float defaultImageCenterY = 240;
const unsigned char defaultProjectionWidth=80;
const unsigned char defaultProjectionHeight=60;
const double planeThresholdDefault = 0.02;
const unsigned char oMarkerHMaxDefault = 70;
const unsigned char oMarkerHMinDefault = 40;
const unsigned char oMarkerSMaxDefault = 255;
const unsigned char oMarkerSMinDefault = 70;
const unsigned char oMarkerVMaxDefault = 255;
const unsigned char oMarkerVMinDefault = 150;
const unsigned char xMarkerHMaxDefault = 10;
const unsigned char xMarkerHMinDefault = 0;
const unsigned char xMarkerSMaxDefault = 150;
const unsigned char xMarkerSMinDefault = 70;
const unsigned char xMarkerVMaxDefault = 255;
const unsigned char xMarkerVMinDefault = 150;
const unsigned char handMinPointsDefault = 100;
const unsigned char handHMaxDefault = 15;
const unsigned char handHMinDefault = 2;
const unsigned char handSMaxDefault = 100;
const unsigned char handSMinDefault = 50;
const unsigned char handVMaxDefault = 180;
const unsigned char handVMinDefault = 50;


const std::vector<unsigned char> oMarkerHSVMin = boost::assign::list_of(oMarkerHMinDefault)(oMarkerSMinDefault)(oMarkerVMinDefault);
const std::vector<unsigned char> oMarkerHSVMax = boost::assign::list_of(oMarkerHMaxDefault)(oMarkerSMaxDefault)(oMarkerVMaxDefault);
const std::vector<unsigned char> xMarkerHSVMin = boost::assign::list_of(xMarkerHMinDefault)(xMarkerSMinDefault)(xMarkerVMinDefault);
const std::vector<unsigned char> xMarkerHSVMax = boost::assign::list_of(xMarkerHMaxDefault)(xMarkerSMaxDefault)(xMarkerVMaxDefault);
const std::vector<unsigned char> handHSVMin = boost::assign::list_of(handHMinDefault)(handSMinDefault)(handVMinDefault);
const std::vector<unsigned char> handHSVMax = boost::assign::list_of(handHMaxDefault)(handSMaxDefault)(handVMaxDefault);


struct utilities::HSVCalibrator::PImpl{
  PImpl():
    segmenter(planeThresholdDefault),
    handclassifier(ActionParsing::hsvFromOpenCV(handHSVMin),
		   ActionParsing::hsvFromOpenCV(handHSVMax)),
    calibrator(defaultFocalLenght, defaultImageCenterX, defaultImageCenterY,
	       ActionParsing::hsvFromOpenCV(oMarkerHSVMin),
	       ActionParsing::hsvFromOpenCV(oMarkerHSVMax),
	       ActionParsing::hsvFromOpenCV(xMarkerHSVMin),
	       ActionParsing::hsvFromOpenCV(xMarkerHSVMax)),
    downsampled_cloud(new cedar::aux::RGBAPointCloud),
    table_cloud(new cedar::aux::RGBAPointCloud),
    o_marker_cloud(new cedar::aux::RGBAPointCloud),
    x_marker_cloud(new cedar::aux::RGBAPointCloud),
    objects_cloud(new cedar::aux::RGBAPointCloud),
    hands_cloud(new cedar::aux::RGBAPointCloud) {}
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
  int vp_1,vp_2,vp_3,vp_4;
  ActionParsing::TableTopSegmentation segmenter;
  ActionParsing::TableTopHandClassifier handclassifier;
  ActionParsing::TableTopCalibration calibrator;
  boost::shared_ptr<ActionParsing::TableTopXYProjection> xyprojection_ptr;
  pcl::VoxelGrid<pcl::PointXYZRGBA> downsampling_grid;
  cedar::aux::RGBAPointCloudPtr downsampled_cloud;
  cedar::aux::RGBAPointCloudPtr table_cloud;
  cedar::aux::RGBAPointCloudPtr table_hull;
  cedar::aux::RGBAPointCloudPtr o_marker_cloud;
  cedar::aux::RGBAPointCloudPtr x_marker_cloud;
  cedar::aux::RGBAPointCloudPtr objects_cloud;
  cedar::aux::RGBAPointCloudPtr hands_cloud;
  cv::Mat height;
  Eigen::Vector4f table_plane_normal;
  Eigen::Affine3f to_world_tf;
};

utilities::HSVCalibrator::HSVCalibrator()
  :
  mImage(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_8UC3))),
  mImpl(new PImpl()),
  _mPlaneThresh(new cedar::aux::DoubleParameter(this,"plane threshold", planeThresholdDefault)),
  _mInvertPlaneNormal(new cedar::aux::BoolParameter(this,"invert plane normal", false)),
  _mShowPCLVisualizer(new cedar::aux::BoolParameter(this,"show PCL Viz", false)),
  _mShowOMarker(new cedar::aux::BoolParameter(this,"Show O marker",true)),
  _mShowXMarker(new cedar::aux::BoolParameter(this,"Show X marker",false)),
  _mShowHand(new cedar::aux::BoolParameter(this,"Show hand",false)),
  _mOMarkerHMax(new cedar::aux::UIntParameter(this,"O marker H Max",oMarkerHMaxDefault,0,180)),
  _mOMarkerHMin(new cedar::aux::UIntParameter(this,"O marker H Min",oMarkerHMinDefault,0,180)),
  _mOMarkerSMax(new cedar::aux::UIntParameter(this,"O marker S Max",oMarkerSMaxDefault,0,255)),
  _mOMarkerSMin(new cedar::aux::UIntParameter(this,"O marker S Min",oMarkerSMinDefault,0,255)),
  _mOMarkerVMax(new cedar::aux::UIntParameter(this,"O marker V Max",oMarkerVMaxDefault,0,255)),
  _mOMarkerVMin(new cedar::aux::UIntParameter(this,"O marker V Min",oMarkerVMinDefault,0,255)),
  _mXMarkerHMax(new cedar::aux::UIntParameter(this,"X marker H Max",xMarkerHMaxDefault,0,180)),
  _mXMarkerHMin(new cedar::aux::UIntParameter(this,"X marker H Min",xMarkerHMinDefault,0,180)),
  _mXMarkerSMax(new cedar::aux::UIntParameter(this,"X marker S Max",xMarkerSMaxDefault,0,255)),
  _mXMarkerSMin(new cedar::aux::UIntParameter(this,"X marker S Min",xMarkerSMinDefault,0,255)),
  _mXMarkerVMax(new cedar::aux::UIntParameter(this,"X marker V Max",xMarkerVMaxDefault,0,255)),
  _mXMarkerVMin(new cedar::aux::UIntParameter(this,"X marker V Min",xMarkerVMinDefault,0,255)),
  _mHandMinPoints(new cedar::aux::UIntParameter(this,"Min hand points",handMinPointsDefault,0,1000)),
  _mHandHMax(new cedar::aux::UIntParameter(this,"Hand H Max",handHMaxDefault,0,180)),
  _mHandHMin(new cedar::aux::UIntParameter(this,"Hand H Min",handHMinDefault,0,180)),
  _mHandSMax(new cedar::aux::UIntParameter(this,"Hand S Max",handSMaxDefault,0,255)),
  _mHandSMin(new cedar::aux::UIntParameter(this,"Hand S Min",handSMinDefault,0,255)),
  _mHandVMax(new cedar::aux::UIntParameter(this,"Hand V Max",handVMaxDefault,0,255)),
  _mHandVMin(new cedar::aux::UIntParameter(this,"Hand V Min",handVMinDefault,0,255)),
  _mOutputWidth(new cedar::aux::UIntParameter(this,"projection width",defaultProjectionWidth,0,2000)),
  _mOutputHeight(new cedar::aux::UIntParameter(this,"projection height",defaultProjectionHeight,0,2000))
{

  //declare inputs/outputs
  this->declareInput("input_cloud");
  this->declareOutput("xyprojectionimage", this->mImage);
  
  
  QObject::connect(_mPlaneThresh.get(), SIGNAL(valueChanged()), this, SLOT(setPlaneThresh()));
  QObject::connect(_mShowPCLVisualizer.get(), SIGNAL(valueChanged()), this, SLOT(showPCLVisualizer()));
  QObject::connect(_mOMarkerHMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerHMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerSMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerSMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerVMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerVMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerHMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerHMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerSMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerSMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerVMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerVMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mHandMinPoints.get(), SIGNAL(valueChanged()), this, SLOT(setHandMinPoints()));
  QObject::connect(_mHandHMax.get(), SIGNAL(valueChanged()), this, SLOT(setHandHSVValues()));
  QObject::connect(_mHandHMin.get(), SIGNAL(valueChanged()), this, SLOT(setHandHSVValues()));
  QObject::connect(_mHandSMax.get(), SIGNAL(valueChanged()), this, SLOT(setHandHSVValues()));
  QObject::connect(_mHandSMin.get(), SIGNAL(valueChanged()), this, SLOT(setHandHSVValues()));
  QObject::connect(_mHandVMax.get(), SIGNAL(valueChanged()), this, SLOT(setHandHSVValues()));
  QObject::connect(_mHandVMin.get(), SIGNAL(valueChanged()), this, SLOT(setHandHSVValues()));
  QObject::connect(_mOutputWidth.get(), SIGNAL(valueChanged()), this, SLOT(updateProjectionTransform()));
  QObject::connect(_mOutputHeight.get(), SIGNAL(valueChanged()), this, SLOT(updateProjectionTransform()));

  this->setPlaneThresh();
  this->setMarkerHSVValues();
  this->setHandHSVValues();
  this->setHandMinPoints();
  this->updateProjectionTransform();
  this->showPCLVisualizer();

  this->mImpl->downsampling_grid.setFilterFieldName ("z");//TODO: set as parameters
  this->mImpl->downsampling_grid.setFilterLimits (0.0f, 2.0f);
  this->mImpl->downsampling_grid.setLeafSize (0.005f, 0.005f, 0.005f);
}

utilities::HSVCalibrator::~HSVCalibrator(){}

//----------------------------------------------------------------------------------------------------------------------
// methods
//----------------------------------------------------------------------------------------------------------------------


void utilities::HSVCalibrator::compute(const cedar::proc::Arguments&)
{
  if (!this->mPointCloud || this->mPointCloud->getData().empty())
    return;

  cedar::aux::ConstRGBAPointCloudPtr input_cloud(this->mPointCloud->getData().makeShared());//this make a copy. FIXME: better way to do this??
    
  this->mImpl->downsampling_grid.setInputCloud (input_cloud);
  this->mImpl->downsampling_grid.filter (*this->mImpl->downsampled_cloud);
  
  static int i = 0;
  if (i%100 == 0){
    if(!this->mImpl->segmenter.segmentTableTop(this->mImpl->downsampled_cloud,this->mImpl->table_cloud,this->mImpl->table_plane_normal)){//tabletop segmentation failed
      cedar::aux::LogSingleton::getInstance()->systemInfo("Tabletop segmentation failed","utilities::HSVCalibrator::calibrate()");
      return;
    }
    this->mImpl->segmenter.calcTabletopHull(this->mImpl->table_cloud,this->mImpl->table_hull);
  }
  ++i;

  //fill color & height outputs
  if (!this->mImpl->xyprojection_ptr)
    updateProjectionTransform();
  const float output_width = (float)this->_mOutputWidth->getValue();
  const float output_height = (float)this->_mOutputHeight->getValue();
  cv::Mat &height = this->mImpl->height;//not used but needed by transformPointCloudToXYProjection
  cv::Mat &color = this->mImage->getData();
  height.create(output_height,output_width,CV_32FC1);
  color.create(output_height,output_width,CV_8UC3);
  height = cv::Scalar::all(0);
  color = cv::Scalar::all(0);
  CEDAR_ASSERT(height.isContinuous());
  CEDAR_ASSERT(color.isContinuous());
  

  // this->mImpl->xyprojection_ptr->transformPointCloudToXYProjection(*this->mImpl->table_cloud, height.ptr<float>(), color.ptr<unsigned char>(),
  // 								   this->mImpl->to_world_tf, false);
  

  if (this->_mShowOMarker->getValue() || this->_mShowXMarker->getValue()){
    this->mImpl->calibrator.segmentMarkers(this->mImpl->table_cloud,*this->mImpl->o_marker_cloud,*this->mImpl->x_marker_cloud);
    if (this->_mShowOMarker->getValue())
      this->mImpl->xyprojection_ptr->transformPointCloudToXYProjection(*this->mImpl->o_marker_cloud, height.ptr<float>(), color.ptr<unsigned char>(),
								       this->mImpl->to_world_tf, false);
    if (this->_mShowXMarker->getValue())
      this->mImpl->xyprojection_ptr->transformPointCloudToXYProjection(*this->mImpl->x_marker_cloud, height.ptr<float>(), color.ptr<unsigned char>(),
								       this->mImpl->to_world_tf, false);
  }
    
  if (this->_mShowHand->getValue()){
    std::vector<pcl::PointIndices> object_clusters;
    if (this->mImpl->segmenter.segmentAndClusterObjects(this->mImpl->downsampled_cloud,this->mImpl->table_hull,this->mImpl->objects_cloud, object_clusters)){
      cedar::aux::RGBAPointCloudPtr new_objects_cloud(new cedar::aux::RGBAPointCloud);
      cedar::aux::RGBAPointCloudPtr new_hands_cloud(new cedar::aux::RGBAPointCloud);
      cedar::aux::RGBAPointCloudPtr tmp_cloud(new cedar::aux::RGBAPointCloud);
      for (unsigned int i=0; i < object_clusters.size(); i++){
  	if (this->mImpl->handclassifier.classifyHand(this->mImpl->objects_cloud, object_clusters[i],*tmp_cloud)){
  	  *new_hands_cloud += *tmp_cloud;
  	}
      }
      this->mImpl->hands_cloud.swap(new_hands_cloud);
    }
    this->mImpl->xyprojection_ptr->transformPointCloudToXYProjection(*this->mImpl->hands_cloud, height.ptr<float>(), color.ptr<unsigned char>(),
  								     this->mImpl->to_world_tf, false);
  }
  cv::cvtColor(color,color,CV_RGB2BGR);

  
  //if PCL viewer available
  if (!this->mImpl->viewer || this->mImpl->viewer->wasStopped())//not sure what happens when user close PCL window
    return;

  if (!this->mImpl->viewer->updatePointCloud(input_cloud,"input_cloud"))
    this->mImpl->viewer->addPointCloud(input_cloud,"input_cloud",this->mImpl->vp_1);
  if (!this->mImpl->viewer->updatePointCloud(this->mImpl->table_cloud,"table_cloud"))
    this->mImpl->viewer->addPointCloud(this->mImpl->table_cloud,"table_cloud",this->mImpl->vp_2);
  if (!this->mImpl->viewer->updatePointCloud(this->mImpl->o_marker_cloud,"o_marker_cloud"))
    this->mImpl->viewer->addPointCloud(this->mImpl->o_marker_cloud,"o_marker_cloud",this->mImpl->vp_3);
  if (!this->mImpl->viewer->updatePointCloud(this->mImpl->x_marker_cloud,"x_marker_cloud"))
    this->mImpl->viewer->addPointCloud(this->mImpl->x_marker_cloud,"x_marker_cloud",this->mImpl->vp_3);
  if (!this->mImpl->viewer->updatePointCloud(this->mImpl->hands_cloud,"hands_cloud"))
    this->mImpl->viewer->addPointCloud(this->mImpl->hands_cloud,"hands_cloud",this->mImpl->vp_4);

  this->mImpl->viewer->spinOnce(100);
}

void utilities::HSVCalibrator::inputConnectionChanged(const std::string& inputName)
{
  if (inputName == "input_cloud")
  {
    this->mPointCloud = boost::shared_dynamic_cast<const cedar::aux::RGBAPointCloudData>(this->getInput("input_cloud"));
    updateProjectionTransform();
  }
  this->onTrigger();
}

void utilities::HSVCalibrator::setPlaneThresh() {
  this->mImpl->segmenter.setPlaneThreshold(_mPlaneThresh->getValue());
}

void utilities::HSVCalibrator::setMarkerHSVValues() {
  std::vector<unsigned char> oMarkerHSVMin = boost::assign::list_of(static_cast<unsigned char>(_mOMarkerHMin->getValue()))(static_cast<unsigned char>(_mOMarkerSMin->getValue()))(static_cast<unsigned char>(_mOMarkerVMin->getValue()));
  std::vector<unsigned char> oMarkerHSVMax = boost::assign::list_of(static_cast<unsigned char>(_mOMarkerHMax->getValue()))(static_cast<unsigned char>(_mOMarkerSMax->getValue()))(static_cast<unsigned char>(_mOMarkerVMax->getValue()));
  std::vector<unsigned char> xMarkerHSVMin = boost::assign::list_of(static_cast<unsigned char>(_mXMarkerHMin->getValue()))(static_cast<unsigned char>(_mXMarkerSMin->getValue()))(static_cast<unsigned char>(_mXMarkerVMin->getValue()));
  std::vector<unsigned char> xMarkerHSVMax = boost::assign::list_of(static_cast<unsigned char>(_mXMarkerHMax->getValue()))(static_cast<unsigned char>(_mXMarkerSMax->getValue()))(static_cast<unsigned char>(_mXMarkerVMax->getValue()));
  this->mImpl->calibrator.setMarkerHSVValues(ActionParsing::hsvFromOpenCV(oMarkerHSVMin),ActionParsing::hsvFromOpenCV(oMarkerHSVMax),
					     ActionParsing::hsvFromOpenCV(xMarkerHSVMin),ActionParsing::hsvFromOpenCV(xMarkerHSVMax));
}

void utilities::HSVCalibrator::setHandHSVValues() {
  std::vector<unsigned char> handHSVMin = boost::assign::list_of(static_cast<unsigned char>(_mHandHMin->getValue()))(static_cast<unsigned char>(_mHandSMin->getValue()))(static_cast<unsigned char>(_mHandVMin->getValue()));
  std::vector<unsigned char> handHSVMax = boost::assign::list_of(static_cast<unsigned char>(_mHandHMax->getValue()))(static_cast<unsigned char>(_mHandSMax->getValue()))(static_cast<unsigned char>(_mHandVMax->getValue()));
  this->mImpl->handclassifier.setHandHSVValues(ActionParsing::hsvFromOpenCV(handHSVMin),ActionParsing::hsvFromOpenCV(handHSVMax));
}

void utilities::HSVCalibrator::setHandMinPoints() {
  this->mImpl->handclassifier.setMinPoints(this->_mHandMinPoints->getValue());
}

void utilities::HSVCalibrator::updateProjectionTransform(){
  if (this->mImpl->table_cloud->empty())
    return;

  const float output_width = (float)this->_mOutputWidth->getValue();
  const float output_height = (float)this->_mOutputHeight->getValue();
  cedar::aux::RGBAPointCloud::PointType min3d,max3d;
  Eigen::Vector3f cloud_dimensions;
  pcl::getMinMax3D(*this->mImpl->table_cloud,min3d,max3d);
  cloud_dimensions << std::abs(max3d.x-min3d.x),
    std::abs(max3d.y-min3d.y),
    std::abs(max3d.z-min3d.z);
  //std::cerr << cloud_dimensions << std::endl;
  this->mImpl->to_world_tf = Eigen::Translation3f(-min3d.x,-min3d.y,0.0);
  this->mImpl->xyprojection_ptr.reset(new ActionParsing::TableTopXYProjection(output_width,
									      output_height,
									      cloud_dimensions[0],
									      cloud_dimensions[1]));
}

void utilities::HSVCalibrator::showPCLVisualizer(){

  if (this->mImpl->viewer){
    this->mImpl->viewer->removeAllPointClouds();
    this->mImpl->viewer->removeAllShapes();
  }

  this->mImpl->viewer.reset();
  if (this->_mShowPCLVisualizer->getValue()){
    this->mImpl->viewer.reset(new pcl::visualization::PCLVisualizer("PCL viz"));
    this->mImpl->viewer->setBackgroundColor (0, 0, 0);
    this->mImpl->viewer->createViewPort (0, 0, 0.25, 1, this->mImpl->vp_1);
    this->mImpl->viewer->createViewPort (0.25, 0, 0.5, 1, this->mImpl->vp_2);
    this->mImpl->viewer->createViewPort (0.5, 0, 0.75, 1, this->mImpl->vp_3);
    this->mImpl->viewer->createViewPort (0.75, 0, 1, 1, this->mImpl->vp_4);
    this->mImpl->viewer->addCoordinateSystem (0.5, this->mImpl->vp_2);
  }
}
