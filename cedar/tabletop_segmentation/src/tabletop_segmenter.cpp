/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// LOCAL INCLUDES
#include <tabletop_segmentation/tabletop_segmenter.h>

// PROJECT INCLUDES
#include <tabletop_segmentation/tabletop_segmentation.h>
#include <tabletop_segmentation/tabletop_calibration.h>
#include <tabletop_segmentation/tabletop_xyprojection.h>
#include <tabletop_segmentation/hsvutil.h>

// SYSTEM INCLUDES
#include <cedar/auxiliaries/EnumParameter.h>
#include <cedar/auxiliaries/assert.h>
#include <cedar/auxiliaries/exceptions.h>
#include <cedar/auxiliaries/Log.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/common/centroid.h>
#include <boost/assign/list_of.hpp> 


//----------------------------------------------------------------------------------------------------------------------
// constructors and destructor
//----------------------------------------------------------------------------------------------------------------------

//default values for parameter initialization
//HSV values are in opencv ranges
const float defaultFocalLenght = 525;//kinect at 640x480 FIXME
const float defaultImageCenterX = 320;
const float defaultImageCenterY = 240;
const unsigned char defaultProjectionWidth=80;
const unsigned char defaultProjectionHeight=60;
const double planeThresholdDefault = 0.02;
const unsigned char oMarkerHMaxDefault = 70;
const unsigned char oMarkerHMinDefault = 40;
const unsigned char oMarkerSMaxDefault = 255;
const unsigned char oMarkerSMinDefault = 70;
const unsigned char oMarkerVMaxDefault = 255;
const unsigned char oMarkerVMinDefault = 150;
const unsigned char xMarkerHMaxDefault = 10;
const unsigned char xMarkerHMinDefault = 0;
const unsigned char xMarkerSMaxDefault = 150;
const unsigned char xMarkerSMinDefault = 70;
const unsigned char xMarkerVMaxDefault = 255;
const unsigned char xMarkerVMinDefault = 150;


const std::vector<unsigned char> oMarkerHSVMin = boost::assign::list_of(oMarkerHMinDefault)(oMarkerSMinDefault)(oMarkerVMinDefault);
const std::vector<unsigned char> oMarkerHSVMax = boost::assign::list_of(oMarkerHMaxDefault)(oMarkerSMaxDefault)(oMarkerVMaxDefault);
const std::vector<unsigned char> xMarkerHSVMin = boost::assign::list_of(xMarkerHMinDefault)(xMarkerSMinDefault)(xMarkerVMinDefault);
const std::vector<unsigned char> xMarkerHSVMax = boost::assign::list_of(xMarkerHMaxDefault)(xMarkerSMaxDefault)(xMarkerVMaxDefault);

struct utilities::TabletopSegmenter::PImpl{
  PImpl():
    segmenter(planeThresholdDefault),
    calibrator(defaultFocalLenght, defaultImageCenterX, defaultImageCenterY,
	       ActionParsing::hsvFromOpenCV(oMarkerHSVMin),
	       ActionParsing::hsvFromOpenCV(oMarkerHSVMax),
	       ActionParsing::hsvFromOpenCV(xMarkerHSVMin),
	       ActionParsing::hsvFromOpenCV(xMarkerHSVMax)),
    downsampled_cloud(new cedar::aux::RGBAPointCloud),
    table_cloud(new cedar::aux::RGBAPointCloud),
    objects_cloud(new cedar::aux::RGBAPointCloud),
    calibrated(false) {}
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
  int vp_1,vp_2,vp_3,vp_4;
  ActionParsing::TableTopSegmentation segmenter;
  ActionParsing::TableTopCalibration calibrator;
  boost::shared_ptr<ActionParsing::TableTopXYProjection> xyprojection_ptr;
  pcl::VoxelGrid<pcl::PointXYZRGBA> downsampling_grid;
  cedar::aux::RGBAPointCloudPtr downsampled_cloud;
  cedar::aux::RGBAPointCloudPtr table_cloud;
  cedar::aux::RGBAPointCloudPtr table_hull;
  cedar::aux::RGBAPointCloudPtr objects_cloud;
  Eigen::Vector4f table_plane_normal;
  Eigen::Vector3f table_size_worldf;
  bool calibrated;
};

utilities::TabletopSegmenter::TabletopSegmenter()
  :
  mTabletopColor(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_8UC3))),
  mTabletopHeight(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_32FC1))),
  mObjectsColor(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_8UC3))),
  mObjectsHeight(new cedar::aux::MatData(cv::Mat(defaultProjectionHeight,defaultProjectionWidth,CV_32FC1))),
  mObjectSet(new cedar::aux::ObjectSetData()),
  mImpl(new PImpl()),
  _mPlaneThresh(new cedar::aux::DoubleParameter(this,"plane threshold", planeThresholdDefault)),
  _mInvertPlaneNormal(new cedar::aux::BoolParameter(this,"invert plane normal", false)),
  _mShowPCLVisualizer(new cedar::aux::BoolParameter(this,"show PCL Viz", false)),
  _mOMarkerHMax(new cedar::aux::UIntParameter(this,"O marker H Max",oMarkerHMaxDefault,0,180)),
  _mOMarkerHMin(new cedar::aux::UIntParameter(this,"O marker H Min",oMarkerHMinDefault,0,180)),
  _mOMarkerSMax(new cedar::aux::UIntParameter(this,"O marker S Max",oMarkerSMaxDefault,0,255)),
  _mOMarkerSMin(new cedar::aux::UIntParameter(this,"O marker S Min",oMarkerSMinDefault,0,255)),
  _mOMarkerVMax(new cedar::aux::UIntParameter(this,"O marker V Max",oMarkerVMaxDefault,0,255)),
  _mOMarkerVMin(new cedar::aux::UIntParameter(this,"O marker V Min",oMarkerVMinDefault,0,255)),
  _mXMarkerHMax(new cedar::aux::UIntParameter(this,"X marker H Max",xMarkerHMaxDefault,0,180)),
  _mXMarkerHMin(new cedar::aux::UIntParameter(this,"X marker H Min",xMarkerHMinDefault,0,180)),
  _mXMarkerSMax(new cedar::aux::UIntParameter(this,"X marker S Max",xMarkerSMaxDefault,0,255)),
  _mXMarkerSMin(new cedar::aux::UIntParameter(this,"X marker S Min",xMarkerSMinDefault,0,255)),
  _mXMarkerVMax(new cedar::aux::UIntParameter(this,"X marker V Max",xMarkerVMaxDefault,0,255)),
  _mXMarkerVMin(new cedar::aux::UIntParameter(this,"X marker V Min",xMarkerVMinDefault,0,255)),
  _mOutputWidth(new cedar::aux::UIntParameter(this,"projection width",defaultProjectionWidth,0,2000)),
  _mOutputHeight(new cedar::aux::UIntParameter(this,"projection height",defaultProjectionHeight,0,2000))
{

  //declare inputs/outputs
  this->declareInput("input_cloud");
  this->declareInput("input_image");
  this->declareOutput("tabletop_color", this->mTabletopColor);
  this->declareOutput("tabletop_height", this->mTabletopHeight);
  this->declareOutput("objects_color", this->mObjectsColor);
  this->declareOutput("objects_height", this->mObjectsHeight);
  this->declareOutput("objectset", this->mObjectSet);
  
  
  QObject::connect(_mPlaneThresh.get(), SIGNAL(valueChanged()), this, SLOT(setPlaneThresh()));
  QObject::connect(_mShowPCLVisualizer.get(), SIGNAL(valueChanged()), this, SLOT(showPCLVisualizer()));
  QObject::connect(_mOMarkerHMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerHMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerSMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerSMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerVMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mOMarkerVMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerHMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerHMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerSMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerSMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerVMax.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
  QObject::connect(_mXMarkerVMin.get(), SIGNAL(valueChanged()), this, SLOT(setMarkerHSVValues()));
   QObject::connect(_mOutputWidth.get(), SIGNAL(valueChanged()), this, SLOT(updateProjectionTransform()));
  QObject::connect(_mOutputHeight.get(), SIGNAL(valueChanged()), this, SLOT(updateProjectionTransform()));

  this->registerFunction("calibrate", boost::bind(&utilities::TabletopSegmenter::calibrate, this));
  
  this->setPlaneThresh();
  this->setMarkerHSVValues();
  this->updateProjectionTransform();
  this->showPCLVisualizer();

  this->mImpl->downsampling_grid.setFilterFieldName ("z");//TODO: set as parameters
  this->mImpl->downsampling_grid.setFilterLimits (0.0f, 2.0f);
  this->mImpl->downsampling_grid.setLeafSize (0.005f, 0.005f, 0.005f);
}

utilities::TabletopSegmenter::~TabletopSegmenter(){}

//----------------------------------------------------------------------------------------------------------------------
// methods
//----------------------------------------------------------------------------------------------------------------------


void utilities::TabletopSegmenter::compute(const cedar::proc::Arguments&)
{
  if (!this->mPointCloud || this->mPointCloud->getData().empty() || !this->mImage)
    return;

  if (!this->mImpl->calibrated)
    return;

  cedar::aux::ConstRGBAPointCloudPtr input_cloud(this->mPointCloud->getData().makeShared());//this make a copy. FIXME: better way to do this??
  const cv::Mat& input_image(this->mImage->getData());

  this->mImpl->downsampling_grid.setInputCloud (input_cloud);
  this->mImpl->downsampling_grid.filter (*this->mImpl->downsampled_cloud);

  //fill table color & height outputs
  CEDAR_ASSERT(this->mImpl->xyprojection_ptr);
  const float output_width = (float)this->_mOutputWidth->getValue();
  const float output_height = (float)this->_mOutputHeight->getValue();
  //tabletop: complete cloud without segmentation
  cv::Mat &tabletop_height = this->mTabletopHeight->getData();
  cv::Mat &tabletop_color = this->mTabletopColor->getData();
  tabletop_height.create(output_height,output_width,CV_32FC1);
  tabletop_color.create(output_height,output_width,CV_8UC3);
  CEDAR_ASSERT(tabletop_height.isContinuous());
  CEDAR_ASSERT(tabletop_color.isContinuous());
  this->mImpl->xyprojection_ptr->transformPointCloudToXYProjection(*this->mImpl->downsampled_cloud, tabletop_height.ptr<float>(), tabletop_color.ptr<unsigned char>(),
  								   this->mImpl->calibrator.getTransformationToWorldCoordinates());
  cv::cvtColor(tabletop_color,tabletop_color,CV_RGB2BGR);




  std::vector<pcl::PointIndices> object_clusters;
  if (!this->mImpl->segmenter.segmentAndClusterObjects(this->mImpl->downsampled_cloud,this->mImpl->table_hull,this->mImpl->objects_cloud, object_clusters)->empty()){
    std::vector<ActionParsing::TableTopObjectPtr> &objects = this->mObjectSet->getData();
    objects.resize(object_clusters.size());
    for (unsigned int i=0; i < object_clusters.size(); i++){
      objects[i].reset(new ActionParsing::TableTopObject(*this->mImpl->objects_cloud, object_clusters[i].indices,
    							 ActionParsing::TableTopObject::findObjectRGBImage(*this->mImpl->objects_cloud, object_clusters[i].indices, 
    													   input_image, this->mImpl->calibrator, 5)));
    }
    //objects:
    cv::Mat &objects_height = this->mObjectsHeight->getData();
    cv::Mat &objects_color = this->mObjectsColor->getData();
    objects_height.create(output_height,output_width,CV_32FC1);
    objects_color.create(output_height,output_width,CV_8UC3);
    CEDAR_ASSERT(objects_height.isContinuous());
    CEDAR_ASSERT(objects_color.isContinuous());
    this->mImpl->xyprojection_ptr->transformPointCloudToXYProjection(*this->mImpl->objects_cloud, objects_height.ptr<float>(), objects_color.ptr<unsigned char>(),
								     this->mImpl->calibrator.getTransformationToWorldCoordinates());
    cv::cvtColor(objects_color,objects_color,CV_RGB2BGR);
  }
	

  //if PCL viewer available
  if (!this->mImpl->viewer || this->mImpl->viewer->wasStopped())//not sure what happens when user close PCL window
    return;

  if (!this->mImpl->viewer->updatePointCloud(input_cloud,"input_cloud"))
    this->mImpl->viewer->addPointCloud(input_cloud,"input_cloud",this->mImpl->vp_1);
  if (!this->mImpl->viewer->updatePointCloud(this->mImpl->table_cloud,"table_cloud"))
    this->mImpl->viewer->addPointCloud(this->mImpl->table_cloud,"table_cloud",this->mImpl->vp_2);
  if (!this->mImpl->viewer->updatePointCloud(this->mImpl->objects_cloud,"objects_cloud"))
    this->mImpl->viewer->addPointCloud(this->mImpl->objects_cloud,"objects_cloud",this->mImpl->vp_3);
  
  //draw axis
  Eigen::Vector3f o_position,x_direction,y_direction,z_direction,n_direction;
  o_position = this->mImpl->calibrator.transformPointToDeviceCoordinates(Eigen::Vector3f::Zero(3));
  x_direction = this->mImpl->calibrator.transformPointToDeviceCoordinates(Eigen::Vector3f::UnitX());
  y_direction = this->mImpl->calibrator.transformPointToDeviceCoordinates(Eigen::Vector3f::UnitY());
  z_direction = this->mImpl->calibrator.transformPointToDeviceCoordinates(Eigen::Vector3f::UnitZ());
  this->mImpl->viewer->removeAllShapes(this->mImpl->vp_2);
  this->mImpl->viewer->addLine(pcl::PointXYZ(o_position[0],o_position[1],o_position[2]),
  			      pcl::PointXYZ(x_direction[0],x_direction[1],x_direction[2]),
  			      255.0,0.0,0.0,"tabletop_tf_arrow_x",this->mImpl->vp_2);
  this->mImpl->viewer->addLine(pcl::PointXYZ(o_position[0],o_position[1],o_position[2]),
  			      pcl::PointXYZ(y_direction[0],y_direction[1],y_direction[2]),
  			      0.0,255.0,0.0,"tabletop_tf_arrow_y",this->mImpl->vp_2);
  this->mImpl->viewer->addLine(pcl::PointXYZ(o_position[0],o_position[1],o_position[2]),
  			      pcl::PointXYZ(z_direction[0],z_direction[1],z_direction[2]),
  			      0.0,0.0,255.0,"tabletop_tf_arrow_z",this->mImpl->vp_2);
  this->mImpl->viewer->spinOnce(100);
}

void utilities::TabletopSegmenter::inputConnectionChanged(const std::string& inputName)
{
  if (inputName == "input_cloud"){
    this->mPointCloud = boost::dynamic_pointer_cast<const cedar::aux::RGBAPointCloudData>(this->getInput("input_cloud"));
    this->calibrate();
  }
  else if (inputName == "input_image"){
    this->mImage = boost::dynamic_pointer_cast<const cedar::aux::MatData>(this->getInput("input_image"));
  }
  this->onTrigger();
}

void utilities::TabletopSegmenter::calibrate(){
  if (!this->mPointCloud || this->mPointCloud->getData().empty())
    return;

  this->mImpl->calibrated = false;
  cedar::aux::ConstRGBAPointCloudPtr input_cloud(this->mPointCloud->getData().makeShared());//this make a copy. FIXME: better way to do this??
  
  this->mImpl->downsampling_grid.setInputCloud (input_cloud);
  this->mImpl->downsampling_grid.filter (*(this->mImpl->downsampled_cloud));

  if(!this->mImpl->segmenter.segmentTableTop(this->mImpl->downsampled_cloud,this->mImpl->table_cloud,this->mImpl->table_plane_normal)){//tabletop segmentation failed
    this->setState(cedar::proc::Triggerable::STATE_EXCEPTION,"Tabletop segmentation failed.");
    cedar::aux::LogSingleton::getInstance()->systemInfo("Tabletop segmentation failed","utilities::TabletopSegmentation::calibrate()");
    return;
  }
  this->mImpl->segmenter.calcTabletopHull(this->mImpl->table_cloud,this->mImpl->table_hull);

  if (this->_mInvertPlaneNormal->getValue())
    this->mImpl->table_plane_normal *= -1.0;

  if (!this->mImpl->calibrator.calibrate(this->mImpl->table_cloud, this->mImpl->table_plane_normal)){
    this->setState(cedar::proc::Triggerable::STATE_EXCEPTION,"Calibration failed.");
    cedar::aux::LogSingleton::getInstance()->systemInfo("Calibration failed","utilities::TabletopSegmenter::calibrate()");
    return;
  }

  this->setState(cedar::proc::Triggerable::STATE_UNKNOWN,"Calibration success.");
  cedar::aux::LogSingleton::getInstance()->systemInfo("Calibration success","utilities::TabletopSegmenter::calibrate()");
  cedar::aux::RGBAPointCloudPtr table_cloud_worldf(new cedar::aux::RGBAPointCloud);
  this->mImpl->calibrator.transformPointCloudToWorldCoordinates(*this->mImpl->table_cloud, *table_cloud_worldf);
  this->mImpl->table_size_worldf = this->mImpl->segmenter.calcTabletopSize(*table_cloud_worldf);
  this->mImpl->calibrated = true;
  this->updateProjectionTransform();
  return;
}

void utilities::TabletopSegmenter::setPlaneThresh() {
  this->mImpl->segmenter.setPlaneThreshold(_mPlaneThresh->getValue());
}

void utilities::TabletopSegmenter::setMarkerHSVValues() {
  std::vector<unsigned char> oMarkerHSVMin = boost::assign::list_of(static_cast<unsigned char>(_mOMarkerHMin->getValue()))(static_cast<unsigned char>(_mOMarkerSMin->getValue()))(static_cast<unsigned char>(_mOMarkerVMin->getValue()));
  std::vector<unsigned char> oMarkerHSVMax = boost::assign::list_of(static_cast<unsigned char>(_mOMarkerHMax->getValue()))(static_cast<unsigned char>(_mOMarkerSMax->getValue()))(static_cast<unsigned char>(_mOMarkerVMax->getValue()));
  std::vector<unsigned char> xMarkerHSVMin = boost::assign::list_of(static_cast<unsigned char>(_mXMarkerHMin->getValue()))(static_cast<unsigned char>(_mXMarkerSMin->getValue()))(static_cast<unsigned char>(_mXMarkerVMin->getValue()));
  std::vector<unsigned char> xMarkerHSVMax = boost::assign::list_of(static_cast<unsigned char>(_mXMarkerHMax->getValue()))(static_cast<unsigned char>(_mXMarkerSMax->getValue()))(static_cast<unsigned char>(_mXMarkerVMax->getValue()));
  this->mImpl->calibrator.setMarkerHSVValues(ActionParsing::hsvFromOpenCV(oMarkerHSVMin),ActionParsing::hsvFromOpenCV(oMarkerHSVMax),
					     ActionParsing::hsvFromOpenCV(xMarkerHSVMin),ActionParsing::hsvFromOpenCV(xMarkerHSVMax));
}

void utilities::TabletopSegmenter::updateProjectionTransform(){
  const float output_width = (float)this->_mOutputWidth->getValue();
  const float output_height = (float)this->_mOutputHeight->getValue();

  if (!this->mImpl->calibrated)
    return;
  
  this->mImpl->xyprojection_ptr.reset(new ActionParsing::TableTopXYProjection(output_width,output_height,
									      this->mImpl->table_size_worldf[0],this->mImpl->table_size_worldf[1]));
}

void utilities::TabletopSegmenter::showPCLVisualizer(){

  if (this->mImpl->viewer){
    this->mImpl->viewer->removeAllPointClouds();
    this->mImpl->viewer->removeAllShapes();
  }

  this->mImpl->viewer.reset();
  if (this->_mShowPCLVisualizer->getValue()){
    this->mImpl->viewer.reset(new pcl::visualization::PCLVisualizer("PCL viz"));
    this->mImpl->viewer->setBackgroundColor (0, 0, 0);
    this->mImpl->viewer->createViewPort (0, 0, 0.25, 1, this->mImpl->vp_1);
    this->mImpl->viewer->createViewPort (0.25, 0, 0.5, 1, this->mImpl->vp_2);
    this->mImpl->viewer->createViewPort (0.5, 0, 0.75, 1, this->mImpl->vp_3);
    this->mImpl->viewer->createViewPort (0.75, 0, 1, 1, this->mImpl->vp_4);
    this->mImpl->viewer->addCoordinateSystem (0.5, this->mImpl->vp_2);
  }
}
