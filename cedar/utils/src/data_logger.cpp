/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// LOCAL INCLUDES
#include <utils/data_logger.h>

// CEDAR INCLUDES
#include <cedar/auxiliaries/assert.h>
#include <cedar/auxiliaries/exceptions.h>
#include <cedar/auxiliaries/casts.h>
#include <cedar/auxiliaries/Log.h>


// SYSTEM INCLUDES
#include <sstream>

//----------------------------------------------------------------------------------------------------------------------
// constructors and destructor
//----------------------------------------------------------------------------------------------------------------------
utilities::DataLogger::DataLogger()
  :_mDataFileName(new cedar::aux::FileParameter(this,"PCD filename",cedar::aux::FileParameter::READ,"./data.out")),
   startTime(boost::posix_time::microsec_clock::local_time()),frameN(0)
   
{
  // declare all data
  this->declareInputCollection("terms");

  this->mInputs = this->getInputSlot("terms");

  QObject::connect(_mDataFileName.get(), SIGNAL(valueChanged()), this, SLOT(setDataFileName()));
  this->registerFunction("start logging", boost::bind(&utilities::DataLogger::startLogging, this));
  this->registerFunction("stop logging", boost::bind(&utilities::DataLogger::stopLogging, this));
}
//----------------------------------------------------------------------------------------------------------------------
// methods
//----------------------------------------------------------------------------------------------------------------------

void utilities::DataLogger::compute(const cedar::proc::Arguments&)
{
  if (this->mInputs->getDataCount() == 0)
  {
    // no terms, result is all zeros.
    return;
  }

  boost::posix_time::ptime now(boost::posix_time::microsec_clock::local_time());
  boost::posix_time::time_duration stamp = now - this->startTime;

  ++frameN;

  if (!this->isLogging())
    return;

  std::stringstream ss;

  for (unsigned int data_id = 0; data_id < this->mInputs->getDataCount(); ++data_id){
    cedar::aux::ConstMatDataPtr data = cedar::aux::asserted_pointer_cast<cedar::aux::MatData>(this->mInputs->getData(data_id));
    cedar::aux::StringParameterPtr name_param = boost::dynamic_pointer_cast<cedar::aux::StringParameter>(data->getOwner()->getParameter("name"));
    cv::Mat mat = data->getData();
    ss << name_param->getValue() << std::endl << mat << std::endl;
  }
  
  this->dataFile << "frame_n=" << frameN << std::endl;
  this->dataFile << "timestamp=" << stamp.total_microseconds() << std::endl;
  this->dataFile << "size=" << ss.str().size() << std::endl;
  this->dataFile << ss.str();
  this->dataFile << "#################################";
 
}

cedar::proc::DataSlot::VALIDITY utilities::DataLogger::determineInputValidity
                                (
                                  cedar::proc::ConstDataSlotPtr CEDAR_DEBUG_ONLY(slot),
                                  cedar::aux::ConstDataPtr data
                                ) const
{
  // First, let's make sure that this is really the input in case anyone ever changes our interface.
  CEDAR_DEBUG_ASSERT(slot->getName() == "terms")

  if (cedar::aux::ConstMatDataPtr mat_data = boost::dynamic_pointer_cast<cedar::aux::ConstMatData>(data))
  {
    // if (this->mInputs->getDataCount() > 0)
    // {
    //   const cv::Mat& first_mat
    //     = cedar::aux::asserted_pointer_cast<cedar::aux::MatData>(this->mInputs->getData(0))->getData();
    //   if (!cedar::aux::math::matrixSizesEqual(first_mat, mat_data->getData()))
    //   {
    //     return cedar::proc::DataSlot::VALIDITY_ERROR;
    //   }
    // }
    // Mat data is accepted.
    return cedar::proc::DataSlot::VALIDITY_VALID;
  }
  else
  {
    // Everything else is rejected.
    return cedar::proc::DataSlot::VALIDITY_ERROR;
  }
}

void utilities::DataLogger::startLogging(){
  if (this->isLogging())
    return;
  this->dataFile.open(this->_mDataFileName->getPath().c_str(), std::ios::trunc);
  if (!this->dataFile.is_open())
    cedar::aux::LogSingleton::getInstance()->error("Can't open data file","utilities::DataLogger::startLogging()");
  cedar::aux::LogSingleton::getInstance()->message("Logging to " + this->_mDataFileName->getPath(),"utilities::DataLogger::startLogging()");
}

void utilities::DataLogger::stopLogging(){
  this->dataFile.close();
  cedar::aux::LogSingleton::getInstance()->message("Logging stopped","utilities::DataLogger::stopLogging()");
}

bool utilities::DataLogger::isLogging(){
  return this->dataFile.is_open();
}

void utilities::DataLogger::setDataFileName(){
  if (this->isLogging())
    this->stopLogging();
}
