'''
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

#from numpy  import *

class ParseState:
    FRAME_INI = 1
    TIMESTAMP = 2
    SIZE = 3
    DATA_INI = 4
    DATA = 5
    DATA_CONT = 6
    DATA_END = 7
    FRAME_END = 8

class ParseException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)

def parse_data_file(f):
    data = { 'frame_n': [],
             'timestamp': [] }

    state = ParseState.FRAME_INI

    for line in f:
        #print 'state: ',state
        #print 'line: ', line
        #print 'data: ', data
        #raw_input('press any key')
        try:
            if state == ParseState.FRAME_INI:
                if not line.startswith('frame_n'): 
                    raise ParseException('frame_n not found on read line: ' + line)
                data['frame_n'].append(long(line.rstrip().split('=')[1]))
                state = ParseState.TIMESTAMP
            elif state == ParseState.TIMESTAMP:
                if not line.startswith('timestamp'):
                    raise ParseException('timestamp not found on read line: ' + line)
                data['timestamp'].append(long(line.rstrip().split('=')[1]))
                state = ParseState.SIZE
            elif state == ParseState.SIZE:#ignored by now
                if not line.startswith('size'):
                    raise ParseException('size not found on read line: ' + line)
                size = line.rstrip().split('=')[1]
                state = ParseState.DATA_INI
            elif state == ParseState.DATA_INI:
                if line.startswith('#########'):
                    state = ParseState.FRAME_END
                else:
                    data_name = line.rstrip()
                    state = ParseState.DATA
            elif state == ParseState.DATA:
                if not line.startswith('['):
                    raise ParseException('[ not found on read line: ' + line)
                data_content = line.rstrip()
                if data_content.endswith(']'):
                    state = ParseState.DATA_END
                else:
                    state = ParseState.DATA_CONT
            elif state == ParseState.DATA_CONT:
                data_content += line.rstrip()
                if data_content.endswith(']'):
                    state = ParseState.DATA_END
                else:
                    state = ParseState.DATA_CONT
            
            #this states don't consume a line
            if state == ParseState.DATA_END:
                #guess data dimesions. only 0d implemented
                if data_content.count(';'):#1d and 2d
                    if data_content.count(','):#2d
                        dims = 2
                    else:
                        dims = 1
                else:
                    dims = 0

                if dims == 0:
                    if not data.has_key(data_name):
                        data[data_name] = [float('NaN')]*(len(data['frame_n'])-1) #add missing data as empty str
                    data[data_name].append(float(data_content.strip('[]')))
                else:
                    print 'parse ',dims,' dimesions not implemented yet'
                state = ParseState.DATA_INI
            if state == ParseState.FRAME_END:
                state = ParseState.FRAME_INI
                continue
                
        except Exception as e:
            print 'Error parsing:',e
            state = ParseState.FRAME_INI
    return data

if __name__ == '__main__':
    import sys
    import matplotlib.pyplot as plt
    filename = sys.argv[1]
    print 'parsing file:',filename
    f = open(filename,'r')
    d = parse_data_file(f)
    keys = d.keys()
    keys.remove('timestamp')
    keys.remove('frame_n')
    for k in keys:
        plt.plot(d['timestamp'],d[k],label=k)
    plt.legend()
    plt.show()
