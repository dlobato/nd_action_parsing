'''
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import sys
from read_data import parse_data_file
import numpy as np
#import matplotlib
#matplotlib.use('GTKAgg')
import matplotlib.pyplot as plt

#from pylab import *

filename = sys.argv[1]
print 'parsing file:',filename
f = open(filename,'r')
d = parse_data_file(f)

t = np.array(d['timestamp'])
s_rcoi = np.array(d['reach CoI node'])
s_rcoi_m = np.array(d['reach CoI memory'])
s_rcof = np.array(d['reach CoF node'])
s_rcos = np.array(d['reach CoS node'])

s_gcoi = np.array(d['grasp CoI node'])
s_gcoi_m = np.array(d['grasp CoI memory'])
s_gcof = np.array(d['grasp CoF node'])
s_gcos = np.array(d['grasp CoS node'])
s_gcos_m = np.array(d['grasp CoS memory'])

s_dcoi = np.array(d['drop CoI node'])
s_dcoi_m = np.array(d['drop CoI memory'])
s_dcof = np.array(d['drop CoF node'])
s_dcos = np.array(d['drop CoS node'])

plt.subplots_adjust(hspace=0.001)
plt.figure(1)

ax1 = plt.subplot(311)
ax1.plot(t,s_rcoi,label='R CoI')
ax1.plot(t,s_rcoi_m,label='R CoI M')
ax1.plot(t,s_gcoi,label='G CoI')
ax1.plot(t,s_gcoi_m,label='G CoI M')
ax1.plot(t,s_dcoi,label='D CoI')
ax1.plot(t,s_dcoi_m,label='D CoI M')
plt.setp(ax1.get_xticklabels(),visible=False)
plt.legend()
#yticks(arange(-0.9, 1.0, 0.4))
#ylim(-1,1)

#ax2 = plt.subplot(312)
#ax2.plot(t,s_coi_m,label='CoI M')
#plt.setp(ax2.get_xticklabels(),visible=False)

ax2 = plt.subplot(312)
ax2.plot(t,s_rcof,label='R CoF')
ax2.plot(t,s_gcof,label='G CoF')
ax2.plot(t,s_dcof,label='D CoF')
plt.setp(ax2.get_xticklabels(),visible=False)
plt.legend()

ax3 = plt.subplot(313)
ax3.plot(t,s_rcos,label='R CoS')
ax3.plot(t,s_gcos,label='G CoS')
ax3.plot(t,s_gcos_m,label='G CoS M')
ax3.plot(t,s_dcos,label='D CoS')
plt.legend()

plt.figure(2)
plt.plot(t,s_rcoi,label='R CoI')
plt.plot(t,s_rcoi_m,label='R CoI M')
plt.plot(t,s_rcof,label='R CoF')
plt.plot(t,s_rcos,label='R CoS')
plt.legend()

plt.figure(3)
plt.plot(t,s_gcoi,label='G CoI')
plt.plot(t,s_gcoi_m,label='G CoI M')
plt.plot(t,s_gcof,label='G CoF')
plt.plot(t,s_gcos,label='G CoS')
plt.plot(t,s_gcos_m,label='G CoS M')
plt.legend()

plt.figure(4)
plt.plot(t,s_dcoi,label='D CoI')
plt.plot(t,s_dcoi_m,label='D CoI M')
plt.plot(t,s_dcof,label='D CoF')
plt.plot(t,s_dcos,label='D CoS')
plt.legend()


plt.show()


