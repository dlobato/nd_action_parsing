/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UTILITIES_DUMMYPOINTCLOUDSINK_H
#define UTILITIES_DUMMYPOINTCLOUDSINK_H

// LOCAL INCLUDES
//#include "utilities_namespace.h"
#include "RGBAPointCloudData.h"
// PROJECT INCLUDES


// SYSTEM INCLUDES
#include <pcl/point_types.h>
//#include <pcl/visualization/pcl_visualizer.h>
#include <cedar/processing/Step.h>
#include <cedar/auxiliaries/StringParameter.h>
#include <cedar/auxiliaries/BoolParameter.h>
#include <cedar/auxiliaries/DoubleParameter.h>



namespace utilities
{
  class DummyPointCloudSink;
}

/*!@brief Abstract description of the class.
 *
 * More detailed description of the class.
 */
class utilities::DummyPointCloudSink : public cedar::proc::Step
{
  Q_OBJECT
  //--------------------------------------------------------------------------------------------------------------------
  // macros
  //--------------------------------------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------------------------------------
  // constructors and destructor
  //--------------------------------------------------------------------------------------------------------------------
public:
  //!@brief The standard constructor.
  DummyPointCloudSink();

  //!@brief Destructor
  ~DummyPointCloudSink();

  //--------------------------------------------------------------------------------------------------------------------
  // public methods
  //--------------------------------------------------------------------------------------------------------------------
public:
  void compute(const cedar::proc::Arguments&);

  void inputConnectionChanged(const std::string& inputName);
public slots:
  //--------------------------------------------------------------------------------------------------------------------
  // protected methods
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

  //--------------------------------------------------------------------------------------------------------------------
  // private methods
  //--------------------------------------------------------------------------------------------------------------------
private:
  //--------------------------------------------------------------------------------------------------------------------
  // members
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

private:
  // input
  cedar::aux::ConstRGBAPointCloudDataPtr mPointCloud;

  //pcl::visualization::PCLVisualizer mViewer;
  
  //--------------------------------------------------------------------------------------------------------------------
  // parameters
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

private:

}; // class utilities::DummyPointCloudSink

#endif // UTILITIES_DUMMYPOINTCLOUDSINK_H
