/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UTILITIES_PCDREADER_H
#define UTILITIES_PCDREADER_H

// LOCAL INCLUDES
//#include "utilities_namespace.h"
#include "RGBAPointCloudData.h"
// PROJECT INCLUDES


// SYSTEM INCLUDES
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
//#include <pcl/visualization/pcl_visualizer.h>
#include <cedar/processing/Step.h>
#include <cedar/auxiliaries/FileParameter.h>
#include <cedar/auxiliaries/StringParameter.h>
#include <cedar/auxiliaries/BoolParameter.h>
#include <cedar/auxiliaries/DoubleParameter.h>
#include <cedar/auxiliaries/MatData.h>



namespace utilities
{
  class PCDReader;
}

/*!@brief Abstract description of the class.
 *
 * More detailed description of the class.
 */
class utilities::PCDReader : public cedar::proc::Step
{
  Q_OBJECT
  //--------------------------------------------------------------------------------------------------------------------
  // macros
  //--------------------------------------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------------------------------------
  // constructors and destructor
  //--------------------------------------------------------------------------------------------------------------------
public:
  //!@brief The standard constructor.
  PCDReader();

  //!@brief Destructor
  ~PCDReader();

  //--------------------------------------------------------------------------------------------------------------------
  // public methods
  //--------------------------------------------------------------------------------------------------------------------
public:
  void compute(const cedar::proc::Arguments&);
   
public slots:
  void setPCDFileName();
  void setDownsamplingGrid();
  
  //--------------------------------------------------------------------------------------------------------------------
  // protected methods
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

  //--------------------------------------------------------------------------------------------------------------------
  // private methods
  //--------------------------------------------------------------------------------------------------------------------
private:
  //--------------------------------------------------------------------------------------------------------------------
  void readPCDFile(const std::string filename);

  // members
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

private:
  // outputs
  cedar::aux::RGBAPointCloudDataPtr mPointCloud;
  
  pcl::VoxelGrid<cedar::aux::RGBAPointCloud::PointType> mGrid;
  //pcl::visualization::PCLVisualizer mViewer;
  
  //--------------------------------------------------------------------------------------------------------------------
  // parameters
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

private:
  cedar::aux::FileParameterPtr _mPCDFileName;
  //cedar::aux::StringParameterPtr _mPCDFileName;
  //downsampling parameters
  cedar::aux::BoolParameterPtr _mDownsample;
  cedar::aux::DoubleParameterPtr _mDownsampleZMinLimit;
  cedar::aux::DoubleParameterPtr _mDownsampleZMaxLimit;
  cedar::aux::DoubleParameterPtr _mDownsampleLeafSize;
  
}; // class utilities::PCDReader

#endif // UTILITIES_PCDREADER_H
