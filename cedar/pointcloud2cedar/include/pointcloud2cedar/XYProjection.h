/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef UTILITIES_XYPROJECTION_H
#define UTILITIES_XYPROJECTION_H

// LOCAL INCLUDES
//#include "utilities_namespace.h"
#include "RGBAPointCloudData.h"
// PROJECT INCLUDES


// SYSTEM INCLUDES

//#include <pcl/visualization/pcl_visualizer.h>
#include <cedar/processing/Step.h>
#include <cedar/auxiliaries/MatData.h>
#include <cedar/auxiliaries/StringParameter.h>
#include <cedar/auxiliaries/BoolParameter.h>
#include <cedar/auxiliaries/DoubleParameter.h>
#include <boost/thread.hpp>




namespace utilities
{
  class XYProjection;
}

/*!@brief Abstract description of the class.
 *
 * More detailed description of the class.
 */
class utilities::XYProjection : public cedar::proc::Step
{
  Q_OBJECT
  //--------------------------------------------------------------------------------------------------------------------
  // macros
  //--------------------------------------------------------------------------------------------------------------------

  //--------------------------------------------------------------------------------------------------------------------
  // constructors and destructor
  //--------------------------------------------------------------------------------------------------------------------
public:
  //!@brief The standard constructor.
  XYProjection();

  //!@brief Destructor
  ~XYProjection();

  //--------------------------------------------------------------------------------------------------------------------
  // public methods
  //--------------------------------------------------------------------------------------------------------------------
public:
  void compute(const cedar::proc::Arguments&);

  void inputConnectionChanged(const std::string& inputName);
public slots:
  void updateTransform();
  void updateProjectionSize();
  //--------------------------------------------------------------------------------------------------------------------
  // protected methods
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

  //--------------------------------------------------------------------------------------------------------------------
  // private methods
  //--------------------------------------------------------------------------------------------------------------------
private:
  void updateViewer();
  //--------------------------------------------------------------------------------------------------------------------
  // members
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

private:
  // input
  cedar::aux::ConstRGBAPointCloudDataPtr mPointCloud;

  // outputs
  cedar::aux::MatDataPtr mImage;
  cedar::aux::MatDataPtr mHeightMatrix;

  Eigen::Affine3f transform;

  /* pcl::visualization::PCLVisualizer mViewer; */
  /* boost::thread mViewerThread; */
  
  //--------------------------------------------------------------------------------------------------------------------
  // parameters
  //--------------------------------------------------------------------------------------------------------------------
protected:
  // none yet

private:
  cedar::aux::BoolParameterPtr _mAutoTransform;
  cedar::aux::UIntParameterPtr _mProjectionWidth;
  cedar::aux::UIntParameterPtr _mProjectionHeight;
}; // class utilities::XYProjection

#endif // UTILITIES_XYPROJECTION_H
