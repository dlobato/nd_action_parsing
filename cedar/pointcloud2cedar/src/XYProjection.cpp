/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// LOCAL INCLUDES
#include <pointcloud2cedar/XYProjection.h>

// PROJECT INCLUDES

// SYSTEM INCLUDES
#include <sstream>
#include <cedar/auxiliaries/EnumParameter.h>
#include <cedar/auxiliaries/assert.h>
#include <cedar/auxiliaries/exceptions.h>
#include <cedar/auxiliaries/Log.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/myutils/projections.h>


//thread object


//----------------------------------------------------------------------------------------------------------------------
// constructors and destructor
//----------------------------------------------------------------------------------------------------------------------

utilities::XYProjection::XYProjection():
  mImage(new cedar::aux::MatData(cv::Mat())),
  mHeightMatrix(new cedar::aux::MatData((cv::Mat()))),
  transform(Eigen::Affine3f::Identity()),
  // mViewer("PCL visualizer"),
  // mViewerThread(&utilities::XYProjection::updateViewer,this),
  _mAutoTransform(new cedar::aux::BoolParameter(this,"auto transform",true)),
  _mProjectionWidth(new cedar::aux::UIntParameter(this,"projection width",320,0,2000)),
  _mProjectionHeight(new cedar::aux::UIntParameter(this,"projection height",240,0,2000))
{
  //declare inputs/outputs
  this->declareInput("rgbapointcloud");
  this->declareOutput("xyprojectionimage", this->mImage);
  this->declareOutput("xyprojectionheight", this->mHeightMatrix);

  QObject::connect(_mAutoTransform.get(), SIGNAL(valueChanged()), this, SLOT(updateTransform()));
  QObject::connect(_mProjectionWidth.get(), SIGNAL(valueChanged()), this, SLOT(updateProjectionSize()));
  QObject::connect(_mProjectionHeight.get(), SIGNAL(valueChanged()), this, SLOT(updateProjectionSize()));

  this->updateProjectionSize();
}

utilities::XYProjection::~XYProjection(){}

//----------------------------------------------------------------------------------------------------------------------
// methods
//----------------------------------------------------------------------------------------------------------------------

void utilities::XYProjection::compute(const cedar::proc::Arguments&){
  if (!this->mPointCloud || this->mPointCloud->getData().empty())
    return;

  cedar::aux::RGBAPointCloudPtr cloud_transformed(new cedar::aux::RGBAPointCloud);
  cv::Mat &z_data = this->mHeightMatrix->getData();
  cv::Mat &color_data = this->mImage->getData();
  const float projection_width = (float)this->_mProjectionWidth->getValue();
  const float projection_height = (float)this->_mProjectionHeight->getValue();

  CEDAR_ASSERT(z_data.isContinuous() && z_data.rows*z_data.cols*z_data.channels() == projection_width*projection_height);
  CEDAR_ASSERT(color_data.isContinuous() && color_data.rows*color_data.cols*color_data.channels() == projection_width*projection_height*3);

  z_data = cv::Scalar::all(0.0);//we are assuming z is always positive
  color_data = cv::Scalar::all(0);
  pcl::transformPointCloud(this->mPointCloud->getData(),*cloud_transformed,this->transform);
  pcl::orthographicXYProjection(*cloud_transformed,
				projection_width,projection_height,
				z_data.ptr<float>(0),
				color_data.ptr<unsigned char>(0));
  
  cv::cvtColor(color_data,color_data,CV_RGB2BGR);
  // if (!mViewer.updatePointCloud(cloud_transformed,"cloud"))
  //   mViewer.addPointCloud(cloud_transformed,"cloud");
}

void utilities::XYProjection::inputConnectionChanged(const std::string& inputName){
  if (inputName == "rgbapointcloud"){
    this->mPointCloud = boost::shared_dynamic_cast<const cedar::aux::RGBAPointCloudData>(this->getInput("rgbapointcloud"));
    this->updateTransform();
  }
  this->onTrigger();
}

void utilities::XYProjection::updateTransform(){
  if (!this->mPointCloud || this->mPointCloud->getData().empty())
    return;
  const cedar::aux::RGBAPointCloud &cloud = this->mPointCloud->getData();
  const float projection_width = (float)this->_mProjectionWidth->getValue();
  const float projection_height = (float)this->_mProjectionHeight->getValue();

  this->transform = Eigen::Affine3f::Identity();
  if (_mAutoTransform->getValue()){
    cedar::aux::RGBAPointCloud::PointType min3d,max3d;
    Eigen::Vector3f cloud_dimensions;
    pcl::getMinMax3D(cloud,min3d,max3d);
    cloud_dimensions << std::abs(max3d.x-min3d.x),
      std::abs(max3d.y-min3d.y),
      std::abs(max3d.z-min3d.z);
    float projection_x_scale = projection_width/cloud_dimensions[0];
    float projection_y_scale = projection_height/cloud_dimensions[1];
    //calc transform to put points in projection limits
    //translation to set xy to min boundin box
    //scaling to fill projection size (projection_width*projection_height)
    this->transform = Eigen::Scaling(projection_x_scale,projection_y_scale,(float)1.0) * Eigen::Translation3f(-min3d.x,-min3d.y,0.0);
    std::cout << "cloud_dimensions=\n" << cloud_dimensions.matrix() << std::endl;
    std::cout << "projection_x_scale=" << projection_x_scale << ",projection_y_scale=" << projection_y_scale << std::endl;
    std::cout << "transform=\n" << this->transform.matrix() << std::endl;
  }
}

void utilities::XYProjection::updateProjectionSize(){
  const float projection_width = (float)this->_mProjectionWidth->getValue();
  const float projection_height = (float)this->_mProjectionHeight->getValue();
  cv::Mat new_mImage = cv::Mat(projection_height, projection_width, CV_8UC3, cv::Scalar::all(0));
  cv::Mat new_mHeightMatrix = cv::Mat(projection_height, projection_width, CV_32F, cv::Scalar::all(0));
  this->mImage->setData(new_mImage);
  this->mHeightMatrix->setData(new_mHeightMatrix);
  this->updateTransform();
}

void utilities::XYProjection::updateViewer(){
  // mViewer.setBackgroundColor (0, 0, 0);
  // mViewer.addCoordinateSystem (10.0);
  // while (!mViewer.wasStopped ())
  //   mViewer.spinOnce(100,true);
}
