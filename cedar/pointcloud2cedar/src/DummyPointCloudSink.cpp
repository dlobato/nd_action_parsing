/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// LOCAL INCLUDES
#include <pointcloud2cedar/DummyPointCloudSink.h>

// PROJECT INCLUDES

// SYSTEM INCLUDES
#include <sstream>
#include <cedar/auxiliaries/EnumParameter.h>
#include <cedar/auxiliaries/assert.h>
#include <cedar/auxiliaries/exceptions.h>
#include <cedar/auxiliaries/Log.h>

//----------------------------------------------------------------------------------------------------------------------
// constructors and destructor
//----------------------------------------------------------------------------------------------------------------------

utilities::DummyPointCloudSink::DummyPointCloudSink(){
  //declare inputs
  this->declareInput("rgbapointcloud");
}

utilities::DummyPointCloudSink::~DummyPointCloudSink(){}

//----------------------------------------------------------------------------------------------------------------------
// methods
//----------------------------------------------------------------------------------------------------------------------

void utilities::DummyPointCloudSink::compute(const cedar::proc::Arguments&){
  cedar::aux::ConstRGBAPointCloudPtr input_cloud(this->mPointCloud->getData().makeShared());//this make a copy. FIXME: better way to do this??
  //do some stuff with the pointcloud
}

void utilities::DummyPointCloudSink::inputConnectionChanged(const std::string& inputName){
  if (inputName == "rgbapointcloud"){
    this->mPointCloud = boost::shared_dynamic_cast<const cedar::aux::RGBAPointCloudData>(this->getInput("rgbapointcloud"));
  }
  this->onTrigger();
}

