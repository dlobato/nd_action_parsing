/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// LOCAL INCLUDES
#include <pointcloud2cedar/PCDReader.h>

// PROJECT INCLUDES

// SYSTEM INCLUDES
#include <sstream>
#include <cedar/auxiliaries/EnumParameter.h>
#include <cedar/auxiliaries/assert.h>
#include <cedar/auxiliaries/exceptions.h>
#include <cedar/auxiliaries/Log.h>

//----------------------------------------------------------------------------------------------------------------------
// constructors and destructor
//----------------------------------------------------------------------------------------------------------------------


utilities::PCDReader::PCDReader()
  :
  mPointCloud(new cedar::aux::RGBAPointCloudData(cedar::aux::RGBAPointCloud())),
  //_mPCDFileName(new cedar::aux::StringParameter(this,"PCD filename")),
  _mPCDFileName(new cedar::aux::FileParameter(this,"PCD filename",cedar::aux::FileParameter::READ)),
  _mDownsample(new cedar::aux::BoolParameter(this,"Downsample",false)),
  _mDownsampleZMinLimit(new cedar::aux::DoubleParameter(this,"Downsampling z min limit",0.0,0.0,std::numeric_limits<double>::max())),
  _mDownsampleZMaxLimit(new cedar::aux::DoubleParameter(this,"Downsampling z max limit",2.0,0.0,std::numeric_limits<double>::max())),
  _mDownsampleLeafSize(new cedar::aux::DoubleParameter(this,"Downsampling leaf size",0.005,0.0,0.1))
{
  cedar::aux::LogSingleton::getInstance()->allocating(this);

  this->declareOutput("rgbapointcloud", mPointCloud);

  QObject::connect(_mPCDFileName.get(), SIGNAL(valueChanged()), this, SLOT(setPCDFileName()));
  QObject::connect(_mDownsample.get(), SIGNAL(valueChanged()), this, SLOT(setDownsamplingGrid()));
  QObject::connect(_mDownsampleZMinLimit.get(), SIGNAL(valueChanged()), this, SLOT(setDownsamplingGrid()));
  QObject::connect(_mDownsampleZMaxLimit.get(), SIGNAL(valueChanged()), this, SLOT(setDownsamplingGrid()));
  QObject::connect(_mDownsampleLeafSize.get(), SIGNAL(valueChanged()), this, SLOT(setDownsamplingGrid()));
  
  //downsampling grid
  setDownsamplingGrid();
  //updateImageSize();
  
  //viewer
  //set viewer state
  //mViewer.setBackgroundColor (0, 0, 0);
  //viewer.createViewPort (0, 0, 0.5, 1, vp_1);
  //viewer.createViewPort (0.5, 0, 1, 1, vp_2);
  //viewer.createViewPort (0, 0, 0.3, 1, vp_1);
  //viewer.createViewPort (0.3, 0, 0.6, 1, vp_2);
  //viewer.createViewPort (0.6, 0, 1, 1, vp_3);
  //mViewer.addCoordinateSystem (0.5);
  
}

utilities::PCDReader::~PCDReader(){
  cedar::aux::LogSingleton::getInstance()->freeing(this);
}

//----------------------------------------------------------------------------------------------------------------------
// methods
//----------------------------------------------------------------------------------------------------------------------


void utilities::PCDReader::compute(const cedar::proc::Arguments&) {}

void utilities::PCDReader::readPCDFile(const std::string filename){
  std::stringstream ss;
  cedar::aux::RGBAPointCloudPtr rcloud(new cedar::aux::RGBAPointCloud);
  if (pcl::io::loadPCDFile<cedar::aux::RGBAPointCloud::PointType> (filename, *rcloud) == -1) //* load the file
  {
    ss << "Couldn't read file " << this->_mPCDFileName->getPath();
    cedar::aux::LogSingleton::getInstance()->error(ss.str(),"utilities::PCDReader");
    return;
  }
  ss.str("");
  ss << "File " << filename << " read successfully";
  cedar::aux::LogSingleton::getInstance()->message(ss.str(),"utilities::PCDReader");
  {
    QWriteLocker locker(&this->mPointCloud->getLock()); // use a QWriteLocker to reduce the risk of unfreed locks
    this->mPointCloud->setData(*rcloud);
  }
  this->onTrigger();
}

// void utilities::PCDReader::cloud_cb_(const cedar::aux::ConstRGBAPointCloudPtr& cloud){
//   // std::stringstream ss;
//   // ss << "cloud: " << *cloud << std::endl;
//   // cedar::aux::LogSingleton::getInstance()->debugMessage(ss.str(),"utilities::PCDReader::cloud_cb_");
//   {
//     QWriteLocker locker(&this->mPointCloud->getLock()); // use a QWriteLocker to reduce the risk of unfreed locks
//     cedar::aux::RGBAPointCloudPtr downsampled_cloud (new cedar::aux::RGBAPointCloud);

//     if (_mDownsample->getValue()){//if downsampling active
//       mGrid.setInputCloud (cloud);
//       mGrid.filter (*downsampled_cloud);
//       this->mPointCloud->setData(*downsampled_cloud);
//     }else
//       this->mPointCloud->setData(*cloud);

//     // ss.str("");
//     // ss << "downsampled cloud: " << *downsampled_cloud << std::endl;
//     // cedar::aux::LogSingleton::getInstance()->debugMessage(ss.str(),"utilities::PCDReader::cloud_cb_");
//     // if (!mViewer.updatePointCloud(downsampled_cloud,"cloud"))
//     //   mViewer.addPointCloud(downsampled_cloud,"cloud");
//     // mViewer.spinOnce(100);
//   }
//   //locker.unlock(); // this is necessary to avoid deadlocks, as the onTrigger call will lock the data again
  
//   this->onTrigger();
// }

// void utilities::PCDReader::image_cb_(const boost::shared_ptr<openni_wrapper::Image>& image){
//   {
//     QWriteLocker locker(&this->mImage->getLock());
//     cv::Mat grabber_image(image->getHeight(),image->getWidth(),CV_8UC3);
//     cv::Mat &mimage = this->mImage->getData();

//     image->fillRGB(grabber_image.cols, grabber_image.rows, grabber_image.ptr<unsigned char>(0));
//     cv::cvtColor(grabber_image,mimage,CV_RGB2BGR);
//     //cv::resize(grabber_image,mimage,mimage.size());
//   }
//   this->onTrigger();
// }


void utilities::PCDReader::setPCDFileName(){
  readPCDFile(this->_mPCDFileName->getPath());
}

void utilities::PCDReader::setDownsamplingGrid() {
  if (_mDownsample->getValue()){//downsampling active
    const double& zMinLimit = _mDownsampleZMinLimit->getValue();
    const double& zMaxLimit = _mDownsampleZMaxLimit->getValue();
    const double& leafSize = _mDownsampleLeafSize->getValue();

    //check params:
    if (zMinLimit >= zMaxLimit){
      CEDAR_THROW(cedar::aux::RangeException,"Downsampling z limits is an invalid range");
      return;
    }
    {
      QWriteLocker locker(&this->mPointCloud->getLock());//FIXME: should I use this lock??
      mGrid.setFilterFieldName("z");
      mGrid.setFilterLimits(zMinLimit, zMaxLimit);
      mGrid.setLeafSize(leafSize,leafSize,leafSize);//leaf size equal on each dimension
    }
  }
  return;
}

