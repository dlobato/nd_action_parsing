/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// LOCAL INCLUDES
#include <pointcloud2cedar/PCLVisualizer.h>


// SYSTEM INCLUDES
#include <cedar/auxiliaries/EnumParameter.h>
#include <cedar/auxiliaries/assert.h>
#include <cedar/auxiliaries/exceptions.h>
#include <cedar/auxiliaries/Log.h>
#include <pcl/visualization/pcl_visualizer.h>



//----------------------------------------------------------------------------------------------------------------------
// constructors and destructor
//----------------------------------------------------------------------------------------------------------------------


struct utilities::PCLVisualizer::PImpl{
  PImpl():
    viewer("PCLVisualizer") {}
  pcl::visualization::PCLVisualizer viewer;
  //pcl::visualization::PCLVisualizer viewer_tabletopf;
  int vp_1,vp_2,vp_3,vp_4;
};

utilities::PCLVisualizer::PCLVisualizer()
  : mImpl(new PImpl())
  {

  //declare inputs/outputs
  this->declareInput("rgbapointcloud");
  

  this->mImpl->viewer.setBackgroundColor (0, 0, 0);
  this->mImpl->viewer.createViewPort (0, 0, 0.25, 1, this->mImpl->vp_1);
  this->mImpl->viewer.createViewPort (0.25, 0, 0.5, 1, this->mImpl->vp_2);
  this->mImpl->viewer.createViewPort (0.5, 0, 0.75, 1, this->mImpl->vp_3);
  this->mImpl->viewer.createViewPort (0.75, 0, 1, 1, this->mImpl->vp_4);
}

utilities::PCLVisualizer::~PCLVisualizer(){}

//----------------------------------------------------------------------------------------------------------------------
// methods
//----------------------------------------------------------------------------------------------------------------------


void utilities::PCLVisualizer::compute(const cedar::proc::Arguments&)
{
  if (!this->mPointCloud || this->mPointCloud->getData().empty())
    return;

  cedar::aux::ConstRGBAPointCloudPtr input_cloud(this->mPointCloud->getData().makeShared());//this make a copy. FIXME: better way to do this??
  if (!this->mImpl->viewer.wasStopped()){
    if (!this->mImpl->viewer.updatePointCloud(input_cloud,"input_cloud"))
      this->mImpl->viewer.addPointCloud(input_cloud,"input_cloud",this->mImpl->vp_1);
    
    this->mImpl->viewer.spinOnce(100);
  }
}

void utilities::PCLVisualizer::inputConnectionChanged(const std::string& inputName)
{
  if (inputName == "rgbapointcloud")
  {
    this->mPointCloud = boost::shared_dynamic_cast<const cedar::aux::RGBAPointCloudData>(this->getInput("rgbapointcloud"));
  }
  this->onTrigger();
}
