/*
  This file is part of the action parsing project.

  Copyright (C) 2013 David Lobato <dav.lobato@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "plugin.h"
// include your headers here
#include <pointcloud2cedar/OpenniGrabber.h>
#include <pointcloud2cedar/DummyPointCloudSink.h>
#include <pointcloud2cedar/XYProjection.h>
#include <pointcloud2cedar/PCDReader.h>
#include <pointcloud2cedar/PCLVisualizer.h>
#include <tabletop_segmentation/hsvcalibrator.h>
#include <tabletop_segmentation/tabletop_segmentation_rawsrc.h>
#include <tabletop_segmentation/tabletop_segmentation_motion.h>
//#include <tabletop_segmentation/tabletop_segmenter.h>
#include <utils/data_logger.h>

#include <cedar/processing/ElementDeclaration.h>


void pluginDeclaration(cedar::proc::PluginDeclarationPtr plugin)
{ 
  //openni grabber
  plugin->add
  (
    cedar::proc::ElementDeclarationPtr
    (
      new cedar::proc::ElementDeclarationTemplate
      <
	utilities::PointCloudOpenniGrabber
      >
      (
        "PCL"
      )
    )
  );
  //dummy sink
  plugin->add
  (
    cedar::proc::ElementDeclarationPtr
    (
      new cedar::proc::ElementDeclarationTemplate
      <
	utilities::DummyPointCloudSink
      >
      (
        "PCL"
      )
    )
  );
  //xyprojection
  plugin->add
  (
    cedar::proc::ElementDeclarationPtr
    (
      new cedar::proc::ElementDeclarationTemplate
      <
	utilities::XYProjection
      >
      (
        "PCL"
      )
    )
  );
  //PCDReader
  plugin->add
  (
    cedar::proc::ElementDeclarationPtr
    (
      new cedar::proc::ElementDeclarationTemplate
      <
	utilities::PCDReader
      >
      (
        "PCL"
      )
    )
  );
  //PCDReader
  plugin->add
  (
    cedar::proc::ElementDeclarationPtr
    (
      new cedar::proc::ElementDeclarationTemplate
      <
	utilities::PCLVisualizer
      >
      (
        "PCL"
      )
    )
  );
  //TabletopSegmentationRawSrc
  plugin->add
  (
    cedar::proc::ElementDeclarationPtr
    (
      new cedar::proc::ElementDeclarationTemplate
      <
	utilities::TabletopSegmentationRawSrc
      >
      (
        "TabletopSegmentation"
      )
    )
  );
  //TabletopSegmentationMotion
  plugin->add
  (
    cedar::proc::ElementDeclarationPtr
    (
      new cedar::proc::ElementDeclarationTemplate
      <
	utilities::TabletopSegmentationMotion
      >
      (
        "TabletopSegmentation"
      )
    )
  );
  //TabletopSegmentationMotion
  // plugin->add
  // (
  //   cedar::proc::ElementDeclarationPtr
  //   (
  //     new cedar::proc::ElementDeclarationTemplate
  //     <
  // 	utilities::TabletopSegmenter
  //     >
  //     (
  //       "TabletopSegmentation"
  //     )
  //   )
  // );
  //HSVCalibrator
  plugin->add
  (
    cedar::proc::ElementDeclarationPtr
    (
      new cedar::proc::ElementDeclarationTemplate
      <
	utilities::HSVCalibrator
      >
      (
        "TabletopSegmentation"
      )
    )
  );
  //DataLogger
  plugin->add
  (
    cedar::proc::ElementDeclarationPtr
    (
      new cedar::proc::ElementDeclarationTemplate
      <
	utilities::DataLogger
      >
      (
        "Utilities"
      )
    )
  );
}

